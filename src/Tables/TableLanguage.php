<?php

namespace Mota\AdminOfficer\Tables;

use Mota\TableManager\TableManager;
use Mota\AdminOfficer\Models\Language;

class TableLanguage extends TableManager {

    protected $tableManager;

    public function __construct($connection = null) {

        Parent::__construct(Language::class, $connection);
    }

}