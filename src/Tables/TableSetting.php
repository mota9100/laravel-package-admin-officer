<?php

namespace Mota\AdminOfficer\Tables;

use Mota\TableManager\TableManager;
use Mota\AdminOfficer\Models\Setting;

class TableSetting extends TableManager {

    protected $tableManager;

    public function __construct($connection = null) {

        Parent::__construct(Setting::class, $connection);
    }

    public function UpdateSetting(array $update, int $adminId) {

        $this->Update([
            ['accountid', '=', $adminId]
        ], $update);
    }
}