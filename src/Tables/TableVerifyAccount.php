<?php

namespace Mota\AdminOfficer\Tables;

use Illuminate\Support\Facades\Notification;
use Mota\AdminOfficer\Notifications\NotificationWelcome;
use Mota\AdminOfficer\Traits\ClassHelper;
use Mota\AdminOfficer\Utilities\KeyGenerator;
use Mota\TableManager\TableManager;
use Mota\AdminOfficer\Models\VerifyAccount;

class TableVerifyAccount extends TableManager {

    use ClassHelper;

    protected $tableManager;

    public function __construct($connection = null) {

        Parent::__construct(VerifyAccount::class, $connection);
    }

    public function SendVerifyLink($accountId) {

        $objTableAccounts = new TableAccount();

        $account = $objTableAccounts->SelectFirst([
            ['accountid', '=', $accountId]
        ]);

        if(is_null($account))
            $this->GenerateReturn()
                ->SetStatus(false);

        $token = $this->CreateNewVerifyToken($account->email);

        Notification::send($account, new NotificationWelcome($account->email, $token));

        return $this->GenerateReturn()
            ->SetStatus(true);
    }

    private function CreateNewVerifyToken($email) {

        $objKeyGenerator = new KeyGenerator(true, true, true, false);

        $HaveOldToken = $this->GetCount([
            ['email', '=', $email]
        ]);

        if($HaveOldToken > 0) {

            $this->CustomDelete([
                ['email', '=', $email]
            ]);
        }

        $token = $objKeyGenerator->Generate(64,'ao');

        $this->Insert([
            'email' => $email,
            'token' => $token,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        return $token;
    }
}