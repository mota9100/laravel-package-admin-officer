<?php

namespace Mota\AdminOfficer\Tables;

use Mota\TableManager\TableManager;
use Mota\AdminOfficer\Models\Theme;

class TableTheme extends TableManager {

    protected $tableManager;

    public function __construct($connection = null) {

        Parent::__construct(Theme::class, $connection);
    }

    public function UpdateTheme(array $update, string $themeName) {

        $this->Update([
            ['themename', '=', $themeName]
        ], $update);
    }
}