<?php

namespace Mota\AdminOfficer\Tables;

use Mota\AdminOfficer\Traits\ClassHelper;
use Mota\AdminOfficer\Models\Account;
use Mota\TableManager\TableManager;
use Illuminate\Support\Facades\Auth;

class TableAccount extends TableManager {

    use ClassHelper;

    public function __construct($connection = null) {

        Parent::__construct(Account::class, $connection);
    }

    public function Login(array $inputs, $remember = false) {

        $guard = config('auth.defaults.admin_officer');

        $attempt = Auth::guard($guard)->attempt($inputs, $remember);

        if (!$attempt)
            return $this->GenerateReturn()
                ->SetStatus(false);


        return $this->GenerateReturn()
            ->SetStatus(true);
    }

    public function LogOut() {

        $guard = config('auth.defaults.admin_officer');

        $checkAuth = Auth::guard($guard)->check();

        if($checkAuth) {

            Auth::guard($guard)->logout();
        }

        return $this->GenerateReturn()
            ->SetData([
                'Url' => ao_url('login')
            ])->SetStatus(true);
    }

    public function Register($inputs) {

        $insert = (int) $this->InsertGetID($inputs);

        if ($insert == 0)
            return $this->GenerateReturn()
                ->SetStatus(false);


        return $this->GenerateReturn()
            ->SetStatus(true)
            ->SetData([
                $this->GetTablePrimaryKey() => $insert
            ]);
    }
}