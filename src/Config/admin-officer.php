<?php

return [

    'name' => 'Admin Officer',

    'system_menu' => [
        'dashboard',
        'file_manager'
    ],

    'fa_name' => 'مدیریت سایت',

    'multi_language' => false,

    'route' => [

        'prefix' => 'officer',

        'namespace' => 'App\Http\Controllers\AdminOfficer',

        'middleware' => [
            'admin.officer.auth'
        ],

        'middleware_authed' => [
            'admin.officer.authed'
        ]
    ],

    'directory' => 'AdminOfficer',

    'title' => 'AdminOfficer',

    'auth' => [

        'defaults' => [
            'admin_officer' => 'admin_officer'
        ],

        'guards' => [
            'admin_officer' => [
                'driver'   => 'session',
                'provider' => 'admin_officer',
            ],
        ],

        'providers' => [
            'admin_officer' => [
                'driver' => 'eloquent',
                'model' => Mota\AdminOfficer\Models\Account::class
            ]
        ],

        'activation' => [
            'admin_officer' => [
                'provider' => 'admin_officer',
                'table' => 'tb_ao_verify_accounts',
                'expire' => 30,
            ],
        ],

        'passwords' => [
            'admin_officer' => [
                'provider' => 'admin_officer',
                'table' => 'tb_ao_password_resets',
                'expire' => 15,
            ],
        ]
    ],

    'secure' => false,

    'version' => 'v1.3.3',

    'copyright' => '2018',

    'file_manager' => [

        'filesystems' => [

            'disks' => [
                'admin_officer' => [
                    'driver' => 'local',
                    'root' => storage_path('AOFileManager'),
                    'url' => env('APP_URL', 'http://localhost').'/officerstorage',
                    'visibility' => 'public',
                ],
            ]
        ],

        'filesystem_disk' => 'admin_officer',

        'storage_directory' => 'AOFileManager',

        'public_directory' => 'officerstorage',

        //in min
        'temp_expire_time' => 10,

        'folders' => [
            'pictures',
            'documents',
            'videos',
            'musics'
        ],

        'sort_type' => [
            'time',
            'alphabetic'
        ],

        'view_type' => [
            'grid',
            'list'
        ],

        'selectable_types' => [
            'all',
            'image',
            'video',
            'application'
        ],

        'not_allowed_mimes' => [
            'php',
            'html',
            'htm',
            'css',
            'js',
            'aspx',
            'asp',
            'jsp',
            'hphp',
            'phtml',
            'module',
            'php4',
            'php5',
            'inc',
            '.htaccess',
            '.env',
            'htaccess',
            'env',
            '.git',
            '.gitignore',
            'git',
            'gitignore'
        ],

        'middleware' => [
            'admin.file.manager.multi.user',
            'admin.file.manager.default.folder'
        ],

        'prefix' => 'fm',

        'url_prefix' => 'fm',

        'allow_multi_user' => true,

        'allow_share_folder' => true,

        'user_field' => Mota\AdminOfficer\Utilities\FileManager\ConfigHandler::class,

        'images_folder_name' => 'photos',
        'files_folder_name'  => 'files',

        'shared_folder_name' => 'shares',
        'thumb_folder_name'  => 'thumbs',

        'images_startup_view' => 'grid',
        'files_startup_view' => 'list',

        'rename_file' => false,

        'alphanumeric_filename' => false,

        'alphanumeric_directory' => false,

        'should_validate_size' => false,

        'max_image_size' => 50000,
        'max_file_size' => 50000,

        'should_validate_mime' => false,

        'valid_image_mimetypes' => [
            'image/jpeg',
            'image/pjpeg',
            'image/png',
            'image/gif',
            'image/svg+xml',
        ],

        'should_create_thumbnails' => true,

        'raster_mimetypes' => [
            'image/jpeg',
            'image/pjpeg',
            'image/png',
        ],

        'create_folder_mode' => 0755,

        'create_file_mode' => 0644,

        'valid_file_mimetypes' => [
            'image/jpeg',
            'image/pjpeg',
            'image/png',
            'image/gif',
            'image/svg+xml',
            'application/pdf',
            'text/plain',
        ],

        'thumb_img_width' => 200,
        'thumb_img_height' => 200,


        'file_type_array' => [
            'pdf'  => 'Adobe Acrobat',
            'doc'  => 'Microsoft Word',
            'docx' => 'Microsoft Word',
            'xls'  => 'Microsoft Excel',
            'xlsx' => 'Microsoft Excel',
            'zip'  => 'Archive',
            'gif'  => 'GIF Image',
            'jpg'  => 'JPEG Image',
            'jpeg' => 'JPEG Image',
            'png'  => 'PNG Image',
            'ppt'  => 'Microsoft PowerPoint',
            'pptx' => 'Microsoft PowerPoint',
        ],

        'file_icon_array' => [
            'pdf'  => 'picture_as_pdf',
            'doc'  => 'insert_drive_file',
            'docx' => 'insert_drive_file',
            'xls'  => 'insert_drive_file',
            'xlsx' => 'insert_drive_file',
            'zip'  => 'insert_drive_file',
            'gif'  => 'gif',
            'jpg'  => 'image',
            'jpeg' => 'image',
            'png'  => 'image',
            'ppt'  => 'insert_drive_file',
            'pptx' => 'insert_drive_file',
        ],

        'php_ini_overrides' => [
            'memory_limit'        => '256M',
        ],
    ]
];
