<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminOfficerTables extends Migration {

    private $prefix = 'ao_';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {

        Schema::create($this->prefix . 'accounts', function (Blueprint $table) {
            $table->increments('AccountID');
            $table->string('FirstName', 256);
            $table->string('LastName', 256);
            $table->string('email', 256)->unique();
            $table->string('password');
            $table->integer('IsActive');
            $table->charset = 'utf8';
            $table->collation = 'utf8_bin';
            $table->rememberToken();
            $table->timestamps();
        });

        Schema::create($this->prefix . 'verify_accounts', function (Blueprint $table) {
            $table->increments('VerifyAccountID');
            $table->string('Email')->index();
            $table->string('Token');
            $table->timestamp('created_at')->nullable();
        });

        Schema::create($this->prefix . 'password_resets', function (Blueprint $table) {
            $table->string('email')->index();
            $table->string('token');
            $table->timestamp('created_at')->nullable();
        });

        Schema::create($this->prefix . 'themes', function (Blueprint $table) {
            $table->increments('ThemeID');
            $table->string('ThemeName', 256);
            $table->string('Skin', 256);
            $table->string('Version', 128);
            $table->charset = 'utf8';
            $table->collation = 'utf8_bin';
        });

        Schema::create($this->prefix . 'languages', function (Blueprint $table) {
            $table->increments('LanguageID');
            $table->string('Country', 256);
            $table->string('Language', 256);
            $table->string('Flag', 1024);
            $table->string('Direction', 4);
            $table->string('ISO3166', 4);
            $table->string('ISO639', 4);
            $table->charset = 'utf8';
            $table->collation = 'utf8_bin';
        });

        Schema::create($this->prefix . 'settings', function (Blueprint $table) {
            $table->increments('SettingID');
            $table->integer('AccountID')->unsigned();
            $table->foreign('AccountID')->references('AccountID')->on($this->prefix . 'accounts');
            $table->integer('ThemeID')->unsigned();
            $table->foreign('ThemeID')->references('ThemeID')->on($this->prefix . 'themes');
            $table->integer('LanguageID')->unsigned();
            $table->foreign('LanguageID')->references('LanguageID')->on($this->prefix . 'languages');
            $table->string('FileManagerSort', 256);
            $table->string('FileManagerView', 256);
            $table->text('Avatar');
            $table->charset = 'utf8';
            $table->collation = 'utf8_bin';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {

        Schema::dropIfExists($this->prefix . 'accounts');
        Schema::dropIfExists($this->prefix . 'verify_accounts');
        Schema::dropIfExists($this->prefix . 'password_resets');
        Schema::dropIfExists($this->prefix . 'settings');
        Schema::dropIfExists($this->prefix . 'themes');
        Schema::dropIfExists($this->prefix . 'languages');
    }
}
