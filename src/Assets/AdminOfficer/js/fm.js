var previousFolderType, previousWorkDirectory, currentContextID,
    cmFolderType, cmWorkDirectory, cmItemName, cmAction;

var rangeSliders = [
    'optimizeJpegConvert',
    'optimizeChangeQuality'
];

$(document).ready(function () {

    GetTree();
    GetView();

    for (var indexSlider = 0; indexSlider < rangeSliders.length; indexSlider++) {

        InitRangeSlider(rangeSliders[indexSlider], 0, 100, 0);
    }

    var uploadArea = $(".fmg-upload-area");

    uploadArea.slimScroll({
        position: 'right',
        width: '100%',
        height: '95%',
        color: '#999',
        opacity: .9,
        railColor: '#999',
        railOpacity: .4,
        railVisible: true,
        alwaysVisible: false
    });

    uploadArea.click(function () {
        $('.fmg-upload-form > input[type="file"]').trigger('click');
    });

    $('.fmg-content').slimScroll({
        position: 'right',
        width: '100%',
        height: '375px',
        color: '#999',
        opacity: .9,
        railColor: '#999',
        railOpacity: .4,
        railVisible: true,
        alwaysVisible: false
    });

    $('.fmg-upload-form > input[type="file"]').on('change', UploadFile);

    $('[data-magnify=gallery]').magnify();

    var body = $("body");

    body.on("contextmenu", ".file-tree-list", function (e) {
        return false;
    });

    body.on("contextmenu", ".fmg-upload-box", function (e) {
        return false;
    });

    body.on("contextmenu", ".fmg-overlay", function (e) {
        return false;
    });

    body.on("contextmenu", ".fmg-context-menu", function (e) {
        return false;
    });

    body.on("contextmenu", ".fmg-main-overlay", function (e) {

        ToggleMainOverLay();
        FadeOutContextMenu();
        return false;
    });

    body.on("contextmenu", ".show-context", function (e) {


        var itemType = $(this).data('item-type');
        var itemId = $(this).data('item-number');

        if (typeof itemId === 'string') {

            if (itemId.startsWith('fmg-item-')) {

                currentContextID = itemId;
            } else {

                currentContextID = undefined;
            }

        } else {

            currentContextID = undefined;
        }

        if (itemType) {

            FadeOutContextMenu(itemType);

            var contextMenu = $("#contextMenu");

            switch (itemType) {

                case 'folder':
                    contextMenu = $("#contextMenuFolder");
                    break;
                case 'file':
                    contextMenu = $("#contextMenuFile");
                    break;
                case 'image':
                    contextMenu = $("#contextMenuImage");
                    break;
            }

            ToggleMainOverLay('show');

            var beforeOffsets = $('.fmg-card').offset();

            var offsetTop = (beforeOffsets.top - 40);
            var offsetLeft = beforeOffsets.left;

            var container = $('[data-item-type="container"]');
            var containerHeight = container.height();
            var containerWidth = container.width();
            var containerOffset = container.offset();
            containerOffset.top = containerOffset.top + containerHeight;
            containerOffset.left = containerOffset.left + containerWidth;
            var contextMenuWidth = contextMenu.width();
            var contextMenuHeight = contextMenu.height();

            if ((containerOffset.top - e.pageY) < contextMenuHeight) {

                offsetTop += contextMenuHeight;
            }

            if ((containerOffset.left - e.pageX) < contextMenuWidth) {

                offsetLeft += contextMenuWidth;
            }

            contextMenu.css({
                display: "block",
                top: (parseInt(e.pageY) - offsetTop),
                left: (parseInt(e.pageX) - offsetLeft)
            });
        }

        return false;
    });
});

$(document).on("click", ".webui-popover", function () {

    return false;
});

$(document).on("click", "body", function () {

    FadeOutContextMenu();
});

$(document).on("dblclick", "[data-item-box]", function () {

    var $this = $(this);
    var type = $this.data('item-type');

    if (type === 'folder') {

        ChangeDirectory($this);

    } else {

        ItemActon($this);
    }
});

$(document).on('click', '.copy-clipboard', function () {

    var urlToClipboard = $('[data-item-number="' + currentContextID + '"]').data('item-url');
    var clipboardInput = $('#clipboardInput');

    clipboardInput.show();
    clipboardInput.val(urlToClipboard);
    clipboardInput.select();
    document.execCommand("copy");
    clipboardInput.val('');
    clipboardInput.hide();

    AlertNotification('info', 'آدرس در کلیپ‌برد ذخیره شد.');
});

$(document).on('drop dragover', '.fmg-upload-area', function (event) {
    event.preventDefault();

    var droppedFiles = event.originalEvent.dataTransfer.files;

    if (droppedFiles.length > 0) {

        UploadFile({
            target: {
                files: droppedFiles
            }
        });
    }
});

$(document).on('click', '#optimizePreview', function() {

    if (typeof currentContextID !== 'undefined') {

        var itemUnderOptimize = $('[data-item-number="' + currentContextID + '"]');
        var image = itemUnderOptimize.data('item-name');

        var jpeg = GetRangeSliderValues(rangeSliders[0]);
        var quality = GetRangeSliderValues(rangeSliders[1]);
        var overwrite = $('#optimizeOverWriteCheck').is(':checked') ? 1 : 0;

        ajaxes.fileManager.OptimizePreview({
            image: image,
            quality: quality,
            jpeg: jpeg,
            overwrite: overwrite
        });
    }
});

$(document).on('click', '#optimizeFinal', function() {

    if (typeof currentContextID !== 'undefined') {

        var itemUnderOptimize = $('[data-item-number="' + currentContextID + '"]');
        var image = itemUnderOptimize.data('item-name');

        var jpeg = GetRangeSliderValues(rangeSliders[0]);
        var quality = GetRangeSliderValues(rangeSliders[1]);
        var overwrite = $('#optimizeOverWriteCheck').is(':checked') ? 1 : 0;

        ajaxes.fileManager.Optimize({
            image: image,
            quality: quality,
            jpeg: jpeg,
            overwrite: overwrite
        });
    }
});

function EmptyCurrentContextID() {

    currentContextID = undefined;
}

function EmptyCM() {

    $('.fmg-paste').addClass('hide');
    cmFolderType = undefined;
    cmWorkDirectory = undefined;
    cmItemName = undefined;
    cmAction = undefined;
}

function SetFolderType(folderType) {

    ajaxes.fileManager.constRequest.folderType = folderType;
}

/**
 * @return {string}
 */
function GetFolderType() {

    return ajaxes.fileManager.constRequest.folderType;
}

function SetWorkDirectory(workDirectory) {

    ajaxes.fileManager.constRequest.workDirectory = workDirectory;
}

/**
 * @return {string}
 */
function GetWorkDirectory() {

    return ajaxes.fileManager.constRequest.workDirectory;
}

function GetTree() {

    ajaxes.fileManager.Tree();
}

function PreviousDirectory() {

    if (typeof previousFolderType !== 'undefined' && typeof previousWorkDirectory !== 'undefined') {

        if (previousFolderType !== '' || previousWorkDirectory !== '') {

            SetFolderType(previousFolderType);
            SetWorkDirectory(previousWorkDirectory);

            GetView();
        }
    }
}

function SetPreviousDirectory(folderType, workDirectory) {

    previousFolderType = folderType;
    previousWorkDirectory = workDirectory;

    if (folderType === '' || workDirectory === '') {

        $('.fmg-back-button').addClass('hide');

    } else {

        $('.fmg-back-button').removeClass('hide');
    }
}

function CreateTree(html) {

    $('#tree').html(html);
}

function GetView() {

    ajaxes.fileManager.View({
        view: viewType,
        sort: sortType,
        selectabletype: fmType
    });
}

function CreateView(html) {

    $('#fmContent').html(html);

    var modal = $('#fmModal');

    if(modal.length > 0) {

        var allowOpen = modal.data('open');

        if(allowOpen === '1') {

            modal.modal('show');
        }
    }

    $('.fmg-content img').one("load", function() {

        var $this = $(this);

        $this.parent().find('.fa-spin').fadeOut('fast');

        $(this).fadeIn('slow');

    }).each(function() {

        var $this = $(this);

        if(this.complete) $this.load();

        $this.error(function(){
            $(this).attr('src', '/vendor/mota/AdminOfficer/images/broken-image.png');
        });
    });
}

function ChangeViewType(view) {

    if (viewType !== view) {

        viewType = view;
        ajaxes.admin.setting.FileManagerView({
            fileManagerView: view
        });
        GetView();
    }
}

function ChangeDirectory(directory) {

    var workingDirectory = directory.data('work-directory');
    workingDirectory = (workingDirectory !== 'root') ? workingDirectory + '/' : '';

    SetFolderType(directory.data('folder-type'));
    SetWorkDirectory((workingDirectory + directory.data('item-name')));
    GetView();
}

/**
 * @return {boolean}
 */
function ItemActon(itemType) {

    //console.log(itemType.data('item-name'));
    return false;
}

function ChangeSortType(sort) {

    if (sortType !== sort) {

        sortType = sort;
        ajaxes.admin.setting.FileManagerSort({
            fileManagerSort: sort
        });
        GetView();
    }
}

function FadeOutContextMenu(contextType) {

    var ActiveContextMenu = $('.fmg-context-menu:visible');
    ToggleMainOverLay();

    if (ActiveContextMenu) {

        if (typeof contextType === 'undefined') {

            ActiveContextMenu.fadeOut(100);

        } else {

            var ActiveContextMenuType = ActiveContextMenu.data('type');

            if (contextType !== ActiveContextMenuType) {

                ActiveContextMenu.fadeOut(100);
            }
        }
    }
}

function ToggleUpload() {

    $('.fmg-upload-box').slideToggle();
}

function UploadFile(event) {

    var filesList = CreateUploadList(event);
    ValidatingUploadList(filesList);
}

function CreateUploadList(event) {

    var file, fileName, fileId, fileExtension, fileSize, uploadTemplate,
        progressColor, uploadAreaBottom, clearButton;

    var arrayFiles = [];

    var uploadArea = $('.fmg-upload-area');

    uploadArea.find('.fmg-upload-area-first-message').hide();

    var files = event.target.files;

    $.each(files, function (index, value) {

        file = value;
        fileName = GetFileName(file.name);
        fileId = GetFileID(index);
        fileExtension = GetFileExtension(file.name);
        fileSize = GetFileSize(file.size);
        progressColor = 'bg-' + GetSkin();

        uploadTemplate = '<div id="' + fileId + '" unselectable="on" class="card fmg-upload-area-preview fmg-dz-file-preview">\n' +
            '<div class="fmg-upload-area-details">\n' +
            '<div class="fmg-upload-area-filename">' +
            '<i class="fmg-list-icon" style="width: 35px;">' + fileExtension.substr(0, 4) + '</i>\n' +
            '<span>' + file.name + '</span></div>' +
            '<div class="fmg-upload-area-message">Pending...</div>\n' +
            '<div class="fmg-upload-area-progress bg-lt-grey">\n' +
            '<div class="fmg-upload-area-determinate changble-bg ' + progressColor + '"></div>\n' +
            '</div>' +
            '<div class="fmg-upload-area-size">' + fileSize + '</div> &bull; \n' +
            '<div class="fmg-upload-area-percentage">0%</div>\n' +
            '</div></div>';

        uploadArea.find('.fmg-upload-area-files').append(uploadTemplate);

        clearButton = $('.fmg-upload-clear-button');

        if (!clearButton.is(':visible')) {

            clearButton.removeClass('hide');
        }

        uploadAreaBottom = uploadArea[0].scrollHeight;
        uploadArea.slimScroll({scrollTo: uploadAreaBottom});

        arrayFiles.push({
            file: file,
            templateId: fileId
        });
    });

    return arrayFiles;
}

function ValidatingUploadList(files) {

    var resolveResponse,
        validationRequest, validationMessagesArray, validationMessagesSingle,
        validationMessage, validationMessageClass, validationMessagesText,
        validationMessagesType, validationResult;

    $.each(files, function (index, value) {

        validationRequest = CreateAjaxRequest('00', {
            data: {
                size: value.file.size,
                extension: GetFileExtension(value.file.name),
                folder_type: GetFolderType(),
                work_directory: GetWorkDirectory()
            }
        });

        CallAjax(validationRequest, routes.fileManager.uploadValidate, function (response) {

            validationMessage = '';

            resolveResponse = GetResponse(response);

            validationMessagesArray = resolveResponse.messages;
            validationResult = resolveResponse.result;

            for (validationMessagesSingle in validationMessagesArray) {

                validationMessagesType = validationMessagesArray[validationMessagesSingle].type;
                validationMessagesText = validationMessagesArray[validationMessagesSingle].text;

                validationMessageClass = (validationMessagesType === 'error' || validationMessagesType === 'warning') ? 'class="col-red"' : '';

                validationMessage += ' <span ' + validationMessageClass + '>' + validationMessagesText + '</span> ';
            }

            $('#' + value.templateId).find('.fmg-upload-area-message').html(validationMessage);

            if (validationResult) {

                SendFile(value.file, value.templateId);
            }
        });
    });
}

function ClearUploadList(element) {

    var clearButton = $('.fmg-upload-clear-button');
    var uploadArea = $('.fmg-upload-area');
    uploadArea.find('.fmg-upload-area-files').html('');
    uploadArea.find('.fmg-upload-area-first-message').show();
    clearButton.addClass('hide');
}

function OpenFileBrowser() {

    $('.fmg-upload-form > input[type="file"]').trigger('click');
}

function SendFile(file, templateId) {

    var data = new FormData();
    var xhr = new XMLHttpRequest();
    xhr.global = false;
    var progressColor, requestUrl, templateDiv, progressBar, percentage,
        inPercent, percentText, resolveResponse, responseMessages, messages,
        responseMessage, responseMessageType, responseMessageText,
        responseMessageClass;

    requestUrl = AdminOfficerUrl(routes.fileManager.upload);
    templateDiv = $('#' + templateId);

    data.append('file', file, file.name);
    data.append('folder_type', GetFolderType());
    data.append('work_directory', GetWorkDirectory());

    xhr.upload.addEventListener("progress", function (event) {


        progressBar = templateDiv.find('.fmg-upload-area-determinate');
        percentage = templateDiv.find('.fmg-upload-area-percentage');
        if (event.lengthComputable) {

            inPercent = Math.round(event.loaded / event.total * 100);
            percentText = inPercent + '%';
            progressBar.css('width', percentText);
            percentage.text(percentText);
        }
    }, false);

    xhr.onreadystatechange = function () {

        if (xhr.readyState === xhr.DONE) {

            if (xhr.status === 200) {

                resolveResponse = GetResponse({
                    responseText: xhr.response,
                    status: xhr.status
                });

                responseMessages = resolveResponse.messages;
                messages = '';

                for (responseMessage in responseMessages) {

                    responseMessageType = responseMessages[responseMessage].type;
                    responseMessageText = responseMessages[responseMessage].text;

                    responseMessageClass = (responseMessageType === 'error' || responseMessageType === 'warning') ? 'class="col-red"' : 'class="col-green"';

                    messages += ' <span ' + responseMessageClass + '>' + responseMessageText + '</span> ';
                }

                templateDiv.find('.fmg-upload-area-message').html(messages);

                if (resolveResponse.result) {
                    progressColor = 'bg-green';
                    GetView();

                } else {

                    progressColor = 'bg-red';
                }

                templateDiv.find('.fmg-upload-area-determinate').removeClass(function (index, className) {
                    return (className.match(/(^|\s)bg-\S+/g) || []).join(' ');
                });
                templateDiv.find('.fmg-upload-area-determinate').addClass(progressColor);

            } else {

                progressColor = 'bg-red';
                templateDiv.find('.fmg-upload-area-determinate').removeClass(function (index, className) {
                    return (className.match(/(^|\s)bg-\S+/g) || []).join(' ');
                });
                templateDiv.find('.fmg-upload-area-determinate').addClass(progressColor);
                templateDiv.find('.fmg-upload-area-message').html('<span class="col-red">Internal error</span>');
            }

        }
    };

    xhr.open('POST', requestUrl, true);
    xhr.setRequestHeader('X-CSRF-Token', GetToken());
    xhr.send(data);
}

function GetFileName(fileName) {

    return fileName.replace(('.' + fileName.split('.').pop()), '');
}

function GetFileExtension(fileName) {

    return fileName.split('.').pop();
}

function GetFileID(count) {

    if (typeof count === 'undefined')
        count = Math.random();

    var timeStamp = new Date().getTime();

    return (timeStamp.toString() + count.toString());
}

function GetFileSize(bytes) {

    var size = ['B', 'kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];

    var factor = Math.floor((bytes.toString().length - 1) / 3);

    return sprintf("%.2f", (bytes / Math.pow(1024, factor))) + ' ' + size[factor];
}

function ToggleOverLay(toggleType, preLoader) {

    if (toggleType === undefined)
        toggleType = 'hide';

    if (preLoader === undefined)
        preLoader = true;

    var overLay = $('.fmg-overlay');
    var preLoaderDiv = overLay.find('.preloader');

    if (toggleType === 'hide') {

        if (!overLay.hasClass('hide')) {

            overLay.addClass('hide');
        }

        if (!preLoaderDiv.hasClass('hide')) {

            preLoaderDiv.addClass('hide');
        }

    } else {

        overLay.removeClass('hide');

        if (preLoader) {

            overLay.find('.preloader').removeClass('hide');
        }
    }
}

function ToggleMainOverLay(toggleType) {

    if (toggleType === undefined)
        toggleType = 'hide';

    var overLay = $('.fmg-main-overlay');

    if (toggleType === 'hide') {

        if (!overLay.hasClass('hide')) {

            overLay.addClass('hide');
        }

    } else {

        overLay.removeClass('hide');
    }
}

function ToggleNewFolderOverLay(toggleType) {

    if (toggleType === undefined)
        toggleType = 'hide';

    var overLay = $('.fmg-new-folder-overlay');

    if (toggleType === 'hide') {

        if (!overLay.hasClass('hide')) {

            overLay.addClass('hide');
        }

    } else {

        overLay.removeClass('hide');
    }
}

function ToggleDeleteOverLay(toggleType) {

    if (toggleType === undefined)
        toggleType = 'hide';

    var overLay = $('.fmg-delete-overlay');

    if (toggleType === 'hide') {

        if (!overLay.hasClass('hide')) {

            overLay.addClass('hide');
        }

    } else {

        overLay.removeClass('hide');
    }
}

function EmptyNewFolderInput() {

    $('.fmg-create-folder-name').val('');
}

function CreateFolder() {

    var folderName = $('.fmg-create-folder-name').val();

    if (folderName !== undefined && folderName !== '') {

        ajaxes.fileManager.NewFolder({
            name: folderName
        });

    } else {

        AlertNotification('warning', lang.error_folder_name);
    }
}

function ToggleNewFolder() {

    var newFolderBox = $('.fmg-new-folder-box');
    var type;

    if (newFolderBox.is(':visible')) {

        type = 'hide'
    } else {

        type = 'show';
    }

    ToggleOverLay(type, false);
    newFolderBox.slideToggle();
    $('.fmg-create-folder-name').focus();

}

function ToggleDelete() {

    if (typeof currentContextID !== 'undefined') {

        var DeleteBox = $('.fmg-delete-box');
        var type;

        if (DeleteBox.is(':visible')) {

            type = 'hide'
        } else {

            type = 'show';
            var itemUnderAction = $('[data-item-number="' + currentContextID + '"]');

            var text = 'آیا از حذف ' + itemUnderAction.data('item-name') + ' مطمئنید؟';

            $('.fmg-delete-text').text(text);
        }

        ToggleOverLay(type, false);

        $('.fmg-delete-no-btn').focus();
        DeleteBox.slideToggle();
    }
}

function Delete() {

    if (typeof currentContextID !== 'undefined') {

        var itemUnderDelete = $('[data-item-number="' + currentContextID + '"]');
        var itemName = itemUnderDelete.data('item-name');

        var request = {
            items: [
                itemName
            ]
        };

        ajaxes.fileManager.Delete(request);
    }
}

function RenamePopOver() {

    if (typeof currentContextID !== 'undefined') {

        var popOverPosition;
        var itemUnderRename = $('[data-item-number="' + currentContextID + '"]');
        var container = itemUnderRename.parents('.fmg-content');
        var containerHeight = container.height();
        var containerWidth = container.width();
        var position = itemUnderRename.position();
        var rightOfItem = position.left + itemUnderRename.width();
        var bottomOfItem = position.top + itemUnderRename.height();

        if (itemUnderRename.hasClass('fmg-box-grid')) {

            if ((containerWidth - rightOfItem) < (containerWidth / 2)) {

                popOverPosition = 'left';

            } else {

                popOverPosition = 'right';
            }
        } else {

            if ((containerHeight - bottomOfItem) < (containerHeight / 2)) {

                popOverPosition = 'top';

            } else {

                popOverPosition = 'bottom';
            }
        }

        var currentName = itemUnderRename.find('.fmg-item-title').attr('data-name');

        var reverseName = ReverseString(currentName);
        var extensionLength = (reverseName.substr(0, reverseName.indexOf('.'))).length;

        var selectingLength = currentName.length - (extensionLength + 1);

        ToggleMainOverLay('show');

        var Content = '<div class="row m-r-0 m-l-0 form-submit">' +
            '<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">' +
            '<div class="form-group">' +
            '<div class="form-line">' +
            '<input class="form-control detect-direction fmg-rename-input" placeholder="' + lang.title_rename + '" type="text">' +
            '</div></div></div>' +
            '<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">' +
            '<button type="button" onclick="Rename(this)" class="btn btn-block changble-bg bg-' + GetSkin() + ' waves-effect form-submit-button">' + lang.btn_change + '</button>' +
            '</div></div>';

        itemUnderRename.webuiPopover({
            title: lang.title_rename,
            content: Content,
            placement: popOverPosition,
            closeable: false,
            cache: false,
            width: 400,
            animation: 'pop',
            direction: 'rtl'
        });

        itemUnderRename.webuiPopover('show');

        $('.fmg-rename-input').val($.trim(currentName)).trigger('change').focus(function () {
            this.setSelectionRange(currentName.length, currentName.length, 'none');
            this.setSelectionRange(0, selectingLength, 'forward');

        }).focus();

    }
}

function Rename(button) {

    if (typeof currentContextID !== 'undefined') {

        var itemUnderRename = $('[data-item-number="' + currentContextID + '"]');

        button = $(button);
        var newName = button.parents('.form-submit').find('.fmg-rename-input').val();
        var oldName = itemUnderRename.data('item-name');

        if (oldName !== newName) {

            var folderType = itemUnderRename.data('folder-type');
            var workDirectory = itemUnderRename.data('work-directory');
            ajaxes.fileManager.Rename({
                items: {
                    old: [
                        {
                            folder_type: folderType,
                            work_directory: workDirectory,
                            item: oldName
                        }
                    ],
                    new: [
                        newName
                    ]
                }
            });
        }

        ToggleMainOverLay('hide');
        itemUnderRename.webuiPopover('destroy');
    }
}

function Copy() {

    if (typeof currentContextID !== 'undefined') {

        var itemUnderAction = $('[data-item-number="' + currentContextID + '"]');

        cmFolderType = itemUnderAction.data('folder-type');
        cmWorkDirectory = itemUnderAction.data('work-directory');
        cmItemName = itemUnderAction.data('item-name');
        cmAction = 'cp';

        $('.fmg-paste').removeClass('hide');
        AlertNotification('success', 'در کلیپ برد ذخیره شد.');
    }

}

function Cut() {

    if (typeof currentContextID !== 'undefined') {

        var itemUnderAction = $('[data-item-number="' + currentContextID + '"]');

        cmFolderType = itemUnderAction.data('folder-type');
        cmWorkDirectory = itemUnderAction.data('work-directory');
        cmItemName = itemUnderAction.data('item-name');
        cmAction = 'mv';

        $('.fmg-paste').removeClass('hide');
        AlertNotification('success', 'در کلیپ برد ذخیره شد.');
    }
}

function Paste() {

    if (typeof cmFolderType !== 'undefined' &&
        typeof cmWorkDirectory !== 'undefined' &&
        typeof cmItemName !== 'undefined' &&
        typeof cmAction !== 'undefined') {

        var request = {
            items: {
                old: [
                    {
                        folder_type: cmFolderType,
                        work_directory: cmWorkDirectory,
                        item: cmItemName
                    }
                ],
                new: [
                    cmItemName
                ]
            }
        };

        if (cmAction === 'cp') {

            ajaxes.fileManager.Copy(request);

        } else if (cmAction === 'mv') {

            ajaxes.fileManager.Cut(request);
        }
    }

}

function GetUrl() {

    if (typeof currentContextID !== 'undefined') {

        var itemUnderDownload = $('[data-item-number="' + currentContextID + '"]');
        var itemName = itemUnderDownload.data('item-name');

        ajaxes.fileManager.Download({
            file: itemName
        });
    }
}

function Download() {

    if (typeof currentContextID !== 'undefined') {

        var itemUnderDownload = $('[data-item-number="' + currentContextID + '"]');
        var folderType = itemUnderDownload.data('folder-type');
        var workDirectory = itemUnderDownload.data('work-directory');
        var itemName = itemUnderDownload.data('item-name');

        location.href = AdminOfficerUrl(FileManagerRoute('download?' + $.param({
            folder_type: folderType,
            work_directory: workDirectory,
            file_name: itemName
        })));
    }
}

function PreviewImage() {

    if (typeof currentContextID !== 'undefined') {

        var itemUnderDownload = $('[data-item-number="' + currentContextID + '"]');

        itemUnderDownload.trigger('dblclick');
    }
}

/**
 * @return {string}
 */
function ReverseString(s) {
    return s.split("").reverse().join("");
}

function InitRangeSlider(id, start, end, defaultValue) {

    var node = document.getElementById(id);
    var nodeValueId = '#' + id + 'Value';

    noUiSlider.create(node, {
        start: [defaultValue],
        connect: 'lower',
        step: 1,
        range: {
            'min': [start],
            'max': [end]
        }
    });

    node.noUiSlider.on('update', function () {

        var val = node.noUiSlider.get();
        val = parseInt(val);
        val += '%';

        $(nodeValueId).text(val);
    });
}

/**
 * @return {number}
 */
function GetRangeSliderValues(id) {

    var node = document.getElementById(id);
    return parseInt(node.noUiSlider.get());
}

function ShowOptimizeModal() {

    if (typeof currentContextID !== 'undefined') {

        var itemUnderOptimize = $('[data-item-number="' + currentContextID + '"]');
        var url = itemUnderOptimize.data('item-url');

        SetOptimizeImage(url);
    }

    $('#optimizeModal').modal('show');
}

function CloseOptimizeModal() {

    $('#optimizeModal').modal('hide');
}

function SetOptimizeImage(imageAddress) {

    var previewContainer = $('.fmg-optimize-preview');
    previewContainer.find('img').attr('src', imageAddress);
    previewContainer.find('a').attr('href', imageAddress);
}