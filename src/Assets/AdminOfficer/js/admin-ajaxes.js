
var routes = {
    admin: {
        terminal: 'admin/terminal',
        registerTerminal: 'admin/register/terminal'
    },
    fileManager: {
        terminal: FileManagerRoute('terminal'),
        upload: FileManagerRoute('upload'),
        uploadValidate:FileManagerRoute('upload/validate')
    }
};

var defines = {
    admin: {
        actions: {
            init: 'adm-00-00',
            logout: 'adm-00-01',
            register: 'adm-03-00',
            sendActivationEmail: 'adm-03-01'
        },
        setting: {
            skin: 'adm-00-02',
            fileManagerSort: 'adm-01-00',
            fileManagerView: 'adm-01-01',
            updateProfile: 'adm-02-00'
        }
    },
    fileManager: {
        get: 'fmg-00',
        tree: 'fmg-01',
        list: 'fmg-02',
        newFolder: 'fmg-03',
        delete: 'fmg-04',
        download: 'fmg-05',
        cut: 'fmg-06',
        copy: 'fmg-07',
        rename: 'fmg-08',
        optimize: 'fmg-14'
    }
};

var ajaxes = {

    admin: {

        setting: {

            Skin: function(requestObj) {

                var request = CreateAjaxRequest(defines.admin.setting.skin, {
                    data: requestObj
                });

                CallAjax(request, routes.admin.terminal, function (response) {

                    var getResponse = GetResponse(response);

                    if(!getResponse.result) {

                        ShowResponseMessages(getResponse);
                    }
                });
            },

            FileManagerSort: function(requestObj) {

                var request = CreateAjaxRequest(defines.admin.setting.fileManagerSort, {
                    data: requestObj
                });

                CallAjax(request, routes.admin.terminal, function (response) {

                    var getResponse = GetResponse(response);

                    if(!getResponse.result) {

                        ShowResponseMessages(getResponse);
                    }
                });
            },

            FileManagerView: function(requestObj) {

                var request = CreateAjaxRequest(defines.admin.setting.fileManagerView, {
                    data: requestObj
                });

                CallAjax(request, routes.admin.terminal, function (response) {

                    var getResponse = GetResponse(response);

                    if(!getResponse.result) {

                        ShowResponseMessages(getResponse);
                    }
                });
            },

            UpdateProfile: function (requestObj) {

                var request = CreateAjaxRequest(defines.admin.setting.updateProfile, {
                    data: requestObj
                });

                CallAjax(request, routes.admin.terminal, function (response) {

                    var getResponse = GetResponse(response);

                    ShowResponseMessages(getResponse);

                    if(getResponse.result) {

                        setTimeout(function () {

                            Refresh();

                        }, 1500);
                    }
                });
            }
        },

        actions: {

            Init: function() {

                var request = CreateAjaxRequest(defines.admin.actions.init);

                CallAjax(request, routes.admin.terminal, function (response) {});
            },

            LogOut: function() {

                var request = CreateAjaxRequest(defines.admin.actions.logout);

                CallAjax(request, routes.admin.terminal, function (response) {

                    var getResponse = GetResponse(response);

                    if(getResponse.result) {

                        Redirect(getResponse.data.url);
                    }
                });
            },

            Register: function (requestObj) {

                var request = CreateAjaxRequest(defines.admin.actions.register, {
                    data: requestObj
                });

                CallAjax(request, routes.admin.registerTerminal, function (response) {

                    var getResponse = GetResponse(response);

                    if(getResponse.result) {

                        if(getResponse['data'].hasOwnProperty('redirect')) {

                            Redirect(getResponse['data'].redirect);

                        } else if(getResponse['data'].hasOwnProperty('refresh')) {

                            Refresh();
                        }

                    }

                    ShowResponseMessages(getResponse);
                });
            },

            SendActivationEmail: function (requestObj) {

                var request = CreateAjaxRequest(defines.admin.actions.sendActivationEmail, {
                    data: requestObj
                });

                CallAjax(request, routes.admin.registerTerminal, function (response) {

                    var getResponse = GetResponse(response);

                    if(getResponse.result) {

                        if(getResponse['data'].hasOwnProperty('redirect')) {

                            Redirect(getResponse['data'].redirect);

                        } else if(getResponse['data'].hasOwnProperty('refresh')) {

                            Refresh();
                        }

                    }

                    ShowResponseMessages(getResponse);
                });
            }
        }
    },

    fileManager: {

        constRequest: {
            workDirectory: 'root',
            folderType: 'user'
        },

        Get: function (attributes) {

            var request = CreateAjaxRequest(defines.fileManager.get, {
                data: Object.assign(attributes, this.constRequest)
            });

            CallAjax(request, routes.fileManager.terminal, function (response) {

                var getResponse = GetResponse(response);

                if(!getResponse.result) {

                    ShowResponseMessages(getResponse);

                } else {

                    ShowFileManager(getResponse.data.html);
                }

            });
        },

        Tree: function () {

            var request = CreateAjaxRequest(defines.fileManager.tree, {
                data: this.constRequest
            });

            CallAjax(request, routes.fileManager.terminal, function (response) {

                var getResponse = GetResponse(response);

                if(!getResponse.result) {

                    ShowResponseMessages(getResponse);

                } else {

                    CreateTree(getResponse.data.html);
                }
            });
        },

        View: function (attributes) {

            ToggleOverLay('show');

            var request = CreateAjaxRequest(defines.fileManager.list, {
                data: Object.assign(attributes, this.constRequest)
            });

            CallAjax(request, routes.fileManager.terminal, function (response) {

                var getResponse = GetResponse(response);

                if(!getResponse.result) {

                    ShowResponseMessages(getResponse);

                } else {

                    CreateView(getResponse.data.html);
                }

                ToggleOverLay('hide');
            });
        },

        Upload: function (attributes) {

            var request = CreateAjaxRequest(defines.fileManager.upload, {
                data: Object.assign(attributes, this.constRequest)
            });

            CallAjax(request, routes.fileManager.terminal, function (response) {

                console.log(response)
            }, 'POST', 'multipart/form-data');
        },

        NewFolder: function (attributes) {

            ToggleNewFolderOverLay('show');

            var request = CreateAjaxRequest(defines.fileManager.newFolder, {
                data: Object.assign(attributes, this.constRequest)
            });

            CallAjax(request, routes.fileManager.terminal, function (response) {

                var getResponse = GetResponse(response);

                ShowResponseMessages(getResponse);

                if(getResponse.result) {

                    EmptyNewFolderInput();

                    ToggleNewFolder();

                    GetView();
                }

                ToggleNewFolderOverLay();
            });
        },

        Rename: function (attributes) {

            ToggleOverLay('show');

            var request = CreateAjaxRequest(defines.fileManager.rename, {
                data: Object.assign(attributes, this.constRequest)
            });

            CallAjax(request, routes.fileManager.terminal, function (response) {

                var getResponse = GetResponse(response);

                ShowResponseMessages(getResponse);

                if(getResponse.result) {

                    EmptyCurrentContextID();
                    GetView();

                } else {

                    ToggleOverLay('hide');
                }
            });
        },

        Copy: function (attributes) {

            ToggleOverLay('show');

            var request = CreateAjaxRequest(defines.fileManager.copy, {
                data: Object.assign(attributes, this.constRequest)
            });

            CallAjax(request, routes.fileManager.terminal, function (response) {

                var getResponse = GetResponse(response);

                ShowResponseMessages(getResponse);

                if(getResponse.result) {

                    EmptyCurrentContextID();
                    EmptyCM();
                    GetView();

                } else {

                    ToggleOverLay('hide');
                }
            });
        },

        Cut: function (attributes) {

            ToggleOverLay('show');

            var request = CreateAjaxRequest(defines.fileManager.cut, {
                data: Object.assign(attributes, this.constRequest)
            });

            CallAjax(request, routes.fileManager.terminal, function (response) {

                var getResponse = GetResponse(response);

                ShowResponseMessages(getResponse);

                if(getResponse.result) {

                    EmptyCurrentContextID();
                    EmptyCM();
                    GetView();

                } else {

                    ToggleOverLay('hide');
                }
            });
        },

        Delete: function (attributes) {

            ToggleOverLay('show');

            var request = CreateAjaxRequest(defines.fileManager.delete, {
                data: Object.assign(attributes, this.constRequest)
            });

            CallAjax(request, routes.fileManager.terminal, function (response) {

                var getResponse = GetResponse(response);

                ShowResponseMessages(getResponse);

                ToggleDelete();

                if(getResponse.result) {

                    EmptyCurrentContextID();
                    GetView();

                } else {

                    ToggleOverLay('hide');
                }
            });
        },

        GetUrl: function (attributes) {

            var request = CreateAjaxRequest(defines.fileManager.download, {
                data: Object.assign(attributes, this.constRequest)
            });

            CallAjax(request, routes.fileManager.terminal, function (response) {

                var getResponse = GetResponse(response);

                ShowResponseMessages(getResponse);

                EmptyCurrentContextID();

                if(getResponse.result) {

                    console.log(getResponse)
                }
            });
        },

        Optimize: function (attributes) {

            attributes = Object.assign(attributes, {
                temporary: 0
            });

            var request = CreateAjaxRequest(defines.fileManager.optimize, {
                data: Object.assign(attributes, this.constRequest)
            });

            CallAjax(request, routes.fileManager.terminal, function (response) {

                var getResponse = GetResponse(response);

                ShowResponseMessages(getResponse);

                if(getResponse.result) {

                    EmptyCurrentContextID();
                    GetView();
                    CloseOptimizeModal();
                }
            });
        },

        OptimizePreview: function (attributes) {

            attributes = Object.assign(attributes, {
                temporary: 1
            });

            var request = CreateAjaxRequest(defines.fileManager.optimize, {
                data: Object.assign(attributes, this.constRequest)
            });

            CallAjax(request, routes.fileManager.terminal, function (response) {

                var getResponse = GetResponse(response);

                if(getResponse.result) {

                    SetOptimizeImage(getResponse['data'].url);
                }
            });
        }
    }
};