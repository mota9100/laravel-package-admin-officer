﻿
$(function () {
    NProgress.configure({
        minimum: 0.2,
        easing: 'ease',
        speed: 500
    });
});

$(document).ajaxSend(function (x) {
    NProgress.start();
});

$(document).ajaxStop(function () {

    NProgress.inc(0.9);
    setTimeout(function () {
        NProgress.done();
        NProgress.remove();
    }, 500);
});

function GetResponse(ajaxResponse) {

    var response;
    var httpStatus = 500;
    var responseResult = false;
    var responseMessages = [];
    var responseData = {};

    if(ajaxResponse.hasOwnProperty('status')) {

        httpStatus = ajaxResponse.status;
    }

    if(ajaxResponse.hasOwnProperty('responseText') && parseInt(httpStatus) === 200) {

        response = JSON.parse(ajaxResponse.responseText);

        if(response.hasOwnProperty('header')) {

            var responseHeader = response.header;

            if(responseHeader.hasOwnProperty('result')) {

                responseResult = responseHeader.result;
            }
        }

        if(response.hasOwnProperty('body')) {

            var responseBody = response.body;

            if(responseBody.hasOwnProperty('message')) {

                responseMessages = responseBody.message;
            }

            if(responseBody.hasOwnProperty('data')) {

                responseData = responseBody.data;
            }
        }

    } else {

        responseMessages = [
            {
                type: 'error',
                text: 'Something went wrong, please try again.'
            }
        ];
    }

    return {
        result: responseResult,
        status: httpStatus,
        messages: responseMessages,
        data: responseData
    }
}

function GetResponseMessages(response) {

    if(response.hasOwnProperty('messages')) {

        return response.messages;
    }

    return [];
}

function ShowResponseMessages(response) {

    var messages = GetResponseMessages(response);

    for (var message in messages) {

        var type = messages[message].type;
        var text = messages[message].text;

        AlertNotification(type, text);
    }
}

function CallAjax(sendData, page, func, method, type, async) {

    if(method === undefined) {
        method = 'POST';
    }

    if(type === undefined) {
        type = 'json';
    }

    if(async === undefined) {
        async = true;
    }


    var requestUrl = AdminOfficerUrl(page);

    var ajaxOptions = {
        url: requestUrl,
        method: method,
        cache: false,
        async: async,
        global: true,
        headers: {},
        xhrFields: {
            withCredentials: true
        },
        contentType: 'application/json; charset=utf-8',
        dataType: type,
        success: function (response, textStatus, xhr) {

            var returnObject = {
                responseText: ((xhr.status != 200) || (type == 'json')) ? JSON.stringify(response) : response,
                status: xhr.status
            };

            func(returnObject);
        },
        error: function (data) {

            func(data);
        }
    };

    if(method === 'POST') {

        ajaxOptions.headers = Object.assign({
            'X-CSRF-Token': GetToken()
        }, ajaxOptions.headers);
    }

    if(typeof sendData === 'object') {

        ajaxOptions.headers = Object.assign({
            Accept: "application/json"
        }, ajaxOptions.headers);

        ajaxOptions.data = JSON.stringify(sendData);

    } else {

        ajaxOptions.data = sendData;
    }

    $.ajax(ajaxOptions);
}

function CreateAjaxRequest(ajaxDefine, request) {

    if(typeof request === 'undefined')
        request = {};

    var ajaxRequest = {
        header: {
            define: ajaxDefine
        },
        body: {}
    };

    if(request.hasOwnProperty('data')) {

        ajaxRequest.body['data'] = request.data;
    }

    return ajaxRequest;
}

function GetToken() {

    var token = $(document).has('meta[name="csrf-token"]') ?
        $('meta[name="csrf-token"]').attr('content') : '';

    return token;
}

function GetSkin() {

    var skin = $(document).has('meta[name="current-skin"]') ?
        $('meta[name="current-skin"]').attr('content') : '';

    return skin;
}

function RemoveSlash(string) {

    return string.replace(/^\/|\/$/g, '');
}

function RemoveDuplicateSlash(string) {

    return string.replace(/([^:]\/)\/+/g, "$1");
}

function BaseUrl() {

    var windowUrl = window.location;
    var baseUrl = windowUrl.protocol + "//" + windowUrl.host;

    return baseUrl;
}

function AdminOfficerRoute(route) {

    var adminOfficerPrefix = $(document).has('meta[name="admin-officer"]') ?
        $('meta[name="admin-officer"]').attr('content') : '';

    return RemoveSlash(
        RemoveDuplicateSlash(
            (adminOfficerPrefix + ((route !== undefined) ? '/' + route : ''))
        )
    );
}

function AdminOfficerUrl(url) {

    var baseUrl = BaseUrl();

    return baseUrl + '/' + AdminOfficerRoute(url);
}

function FileManagerRoute(route) {

    var fileManagerPrefix = $(document).has('meta[name="file-manager"]') ?
        '/' + $('meta[name="file-manager"]').attr('content') : '/';

    return RemoveSlash(
        RemoveDuplicateSlash(
            (fileManagerPrefix + ((route !== undefined) ? '/' + route : ''))
        )
    );
}

function Base64(targetString) {

    return window.btoa(unescape(encodeURIComponent(targetString)));
}

function ReloadCaptcha(param) {

    $('input[type="text"][data-title="' + param.title + '"]').val('');
    $('img[data-title="' + param.title + '"]').attr('src',  param.url + '?' + new Date());
}

$(document).on('keypress', '.form-submit', function (event) {

    if(event.keyCode === 13) {

        $(this).find('.form-submit-button').trigger('click');
    }
});

function SetDirection(element) {

    var valueOfInput = $(element).val();
    var firstChar = valueOfInput.substr(0, 1);

    if(firstChar) {

        var detectPersian = IsPersian(firstChar);
        var direction = (detectPersian) ? 'rtl' : 'ltr';
        var currentDirection = $(element).attr('dir');

        if(currentDirection !== direction) {

            $(element).attr('dir', direction);
        }
    }
}

$(function(){

    $(document).on('keyup change', '.detect-direction', function (event) {

        event.preventDefault();

        SetDirection(this);
    });

    $('.detect-direction').each(function () {

        SetDirection(this);
    });

});

function IsPersian(word) {

    // var isArabic = /^([\u0600-\u06ff]|[\u0750-\u077f]|[\ufb50-\ufbc1]|[\ufbd3-\ufd3f]|[\ufd50-\ufd8f]|[\ufd92-\ufdc7]|[\ufe70-\ufefc]|[\ufdf0-\ufdfd]|[ ])*$/g;
    var isPersian = /^[\u0600-\u06FF\s]+$/;

    if(isPersian.test($.trim(word))){

        return true;

    }else{

        return false;
    }
}

function Redirect(url) {

    window.location.href = url;
}

function Refresh() {

    window.location.reload();
}

function IsMyScriptLoaded(url) {

    var scripts = document.getElementsByTagName('script');
    for (var i = scripts.length; i--;) {
        if (scripts[i].src == url) {

            return true;
        }
    }
    return false;
}

/**
 * @return {string}
 */
function GetCurrentRoute() {

    var currentPathName = location.pathname;

    return currentPathName;
}