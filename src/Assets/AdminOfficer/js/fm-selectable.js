var selectedUrl;

$(function () {

    var body = $("body");

    body.on("contextmenu", ".fmg-modal-footer", function (e) {
        return false;
    });

    $(document).on("click", "[data-item-box]", function () {

        if(fmButton === true) {

            var $this = $(this);
            var type = $this.data('item-type');

            $('[data-item-box]').each(function () {
                $(this).removeClass('fmg-active-box');
            });

            if (type !== 'folder') {

                SetSelected($this);
            }
        }
    });

});

function SetSelected(element) {

    element.addClass('fmg-active-box');
    selectedUrl = element.data('item-url');
    $('.fmg-choose-button').prop('disabled', false);
}

function UnsetSelected() {

    selectedUrl = undefined;
    $('.fmg-choose-button').prop('disabled', true);
}

function OnSelectFile() {

    if(typeof fmFunction === 'function' && typeof selectedUrl === 'string') {

        fmFunction(ParseLocation(selectedUrl));

        var modal = $('#fmModal');

        if(modal.length > 0) {

            modal.modal('toggle');
        }

    } else {

        if(typeof fmFunction !== 'function') {

            console.error('Define <data-function="..."> attribute on file manager button...');
        }
    }
}

var ParseLocation = function(href) {

    var link = document.createElement("a");
    link.href = href;
    return link;
};