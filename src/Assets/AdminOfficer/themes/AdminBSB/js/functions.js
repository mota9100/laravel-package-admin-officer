$(function () {

    SkinChanger();
    ActivateNotificationAndTasksScroll();

    SetSkinListHeightAndScroll(true);
    SetSettingListHeightAndScroll(true);
    $(window).resize(function () {
        SetSkinListHeightAndScroll(false);
        SetSettingListHeightAndScroll(false);
    });
});

function ToggleLoader() {

    var pageLoader = $('.page-loader-wrapper');

    if(pageLoader.is(':visible')) {

        pageLoader.fadeOut();

    } else {

        pageLoader.fadeIn();
    }
}

function SkinChanger() {

    $('.right-sidebar .demo-choose-skin li').on('click', function () {
        var $body = $('body');
        var $this = $(this);

        var existTheme = $('.right-sidebar .demo-choose-skin li.active').data('theme');
        $('.right-sidebar .demo-choose-skin li').removeClass('active');

        $body.removeClass('theme-' + existTheme);
        $this.addClass('active');
        var newTheme = $this.data('theme');

        $body.addClass('theme-' + newTheme);

        var meta = $('meta[name="current-skin"]');

        if(meta.length > 0) {

            meta.attr('content', newTheme);
        }

        $('.changble-bg').each(function () {

            var $thisBtn = $(this);
            $thisBtn.removeClass('bg-' + existTheme);
            $thisBtn.addClass('bg-' + newTheme);
        });

        $('.changble-pl').each(function () {

            var $thisBtn = $(this);
            $thisBtn.removeClass('pl-' + existTheme);
            $thisBtn.addClass('pl-' + newTheme);
        });

        $('.changble-bg-lt').each(function () {

            var $thisBtn = $(this);
            $thisBtn.removeClass('bg-lt-' + existTheme);
            $thisBtn.addClass('bg-lt-' + newTheme);
        });

        $('.changble-col').each(function () {

            var $thisBtn = $(this);
            $thisBtn.removeClass('col-' + existTheme);
            $thisBtn.addClass('col-' + newTheme);
        });

        ajaxes.admin.setting.Skin({
            skin: newTheme
        });
    });
}

function SetSkinListHeightAndScroll(isFirstTime) {
    var height = $(window).height() - ($('.navbar').innerHeight() + $('.right-sidebar .nav-tabs').outerHeight());
    var $el = $('.demo-choose-skin');

    if (!isFirstTime){
      $el.slimScroll({ destroy: true }).height('auto');
      $el.parent().find('.slimScrollBar, .slimScrollRail').remove();
    }

    $el.slimscroll({
        height: height + 'px',
        color: 'rgba(0,0,0,0.5)',
        size: '6px',
        alwaysVisible: false,
        borderRadius: '0',
        railBorderRadius: '0'
    });
}

function SetSettingListHeightAndScroll(isFirstTime) {
    var height = $(window).height() - ($('.navbar').innerHeight() + $('.right-sidebar .nav-tabs').outerHeight());
    var $el = $('.right-sidebar .demo-settings');

    if (!isFirstTime){
      $el.slimScroll({ destroy: true }).height('auto');
      $el.parent().find('.slimScrollBar, .slimScrollRail').remove();
    }

    $el.slimscroll({
        height: height + 'px',
        color: 'rgba(0,0,0,0.5)',
        size: '6px',
        alwaysVisible: false,
        borderRadius: '0',
        railBorderRadius: '0'
    });
}

function ActivateNotificationAndTasksScroll() {
    $('.navbar-right .dropdown-menu .body .menu').slimscroll({
        height: '254px',
        color: 'rgba(0,0,0,0.5)',
        size: '4px',
        alwaysVisible: false,
        borderRadius: '0',
        railBorderRadius: '0'
    });
}

function HexToRgb(hexCode) {
    var patt = /^#([\da-fA-F]{2})([\da-fA-F]{2})([\da-fA-F]{2})$/;
    var matches = patt.exec(hexCode);
    var rgb = "rgb(" + parseInt(matches[1], 16) + "," + parseInt(matches[2], 16) + "," + parseInt(matches[3], 16) + ")";
    return rgb;
}

function HexToRgba(hexCode, opacity) {
    var patt = /^#([\da-fA-F]{2})([\da-fA-F]{2})([\da-fA-F]{2})$/;
    var matches = patt.exec(hexCode);
    var rgb = "rgba(" + parseInt(matches[1], 16) + "," + parseInt(matches[2], 16) + "," + parseInt(matches[3], 16) + "," + opacity + ")";
    return rgb;
}