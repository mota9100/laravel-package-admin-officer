$(document).on('click', 'label[for="rememberme"]', function (event) {

    event.preventDefault();

    var input = $('#rememberme');

    if(input.is(':checked')) {

        input.prop('checked', false);

    } else {

        input.prop('checked', true);
    }
});

$(document).on('click', '#login', function () {

    var remember = $('#rememberme').is(':checked') ? 1 : 0;

    console.log(remember);

    var request = CreateAjaxRequest('glb-00-00', {
        data: {
            email: $('input[name="email"]').val(),
            password: $('input[name="password"]').val(),
            captcha: $('input[name="captcha"]').val(),
            remember: remember
        }
    });

    CallAjax(request, 'global/terminal', function (response) {

        var getResponse = GetResponse(response);

        if(getResponse.result) {

            window.location.replace(AdminOfficerUrl());

        } else {

            ShowResponseMessages(getResponse);
            ReloadLoginCaptcha();
        }
    });
});

function ReloadLoginCaptcha() {

    ReloadCaptcha({
        title: 'captcha',
        url: AdminOfficerUrl('captcha')
    });
}