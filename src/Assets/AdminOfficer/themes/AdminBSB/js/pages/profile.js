$(function () {

    $("input[name='passwordnew']").password({
        shortPass: 'کوتاه',
        badPass: 'ضعیف',
        goodPass: 'متوسط',
        strongPass: 'قوی',
        containsUsername: 'شامل ایمیل',
        enterPass: 'گذرواژه را تایپ کنید',
        showPercent: false,
        showText: true, // shows the text tips
        animate: true, // whether or not to animate the progress bar on input blur/focus
        animateSpeed: 'fast', // the above animation speed
        username: $('input[name="email"]'), // select the username field (selector or jQuery instance) for better password checks
        usernamePartialMatch: true, // whether to check for username partials
        minimumLength: 2 // minimum password length (below this threshold, the score is 0)
    });
});

$(document).on('click', '.password-collapse', function () {

    EmptyPasswordInputs();
});

$(document).on('click', '.update-profile', function () {

    var request = {
        avatar: $("input[name='avatar']").val(),
        email: $("input[name='email']").val(),
        first_name: $("input[name='firstname']").val(),
        last_name: $("input[name='lastname']").val(),
        password_old: $("input[name='passwordold']").val(),
        password: $("input[name='passwordnew']").val(),
        password_confirmation: $("input[name='passwordconfirm']").val()
    };

    if(multiLanguage) {

        request.language =  $("select[name='language']").val();
    }

    ajaxes.admin.setting.UpdateProfile(request);
});

function EmptyPasswordInputs() {
    $("input[name^='password']").val('');
}

function SetAvatar(file) {

    $('img[class="avatar-image"]').attr('src', file.pathname);
    $('input[class="avatar-image"]').attr('value', file.pathname);
}