$(document).on('click', 'button[name="register-button"]', function () {

    ajaxes.admin.actions.Register({
        email: $("input[name='email']").val(),
        first_name: $("input[name='firstname']").val(),
        last_name: $("input[name='lastname']").val(),
        password: $("input[name='passwordnew']").val(),
        password_confirmation: $("input[name='passwordconfirm']").val()
    })
});

$(document).on('click', 'button[name="send-activation-button"]', function () {

    ajaxes.admin.actions.SendActivationEmail({
        email: $("input[name='email']").val()
    })
});


