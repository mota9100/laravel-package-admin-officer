<?php

namespace Mota\AdminOfficer\Models;

use Illuminate\Database\Eloquent\Model;

class VerifyAccount extends Model {

    public function __construct(array $attributes = [], string $connection = 'mysql') {

        $this->connection = $connection;
        parent::__construct($attributes);
    }

    protected $table = 'ao_verify_accounts';
    protected $primaryKey = 'VerifyAccountID';

    protected $fillable = [
        'VerifyAccountID', 'Email', 'Token',
    ];


    protected $hidden = [];

    public $timestamps = false;
}
