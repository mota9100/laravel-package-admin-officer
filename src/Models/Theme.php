<?php

namespace Mota\AdminOfficer\Models;

use Illuminate\Database\Eloquent\Model;

class Theme extends Model {

    public function __construct(array $attributes = [], $connection = null) {

        if(!is_null($connection)) {

            $this->connection = $connection;
        }

        parent::__construct($attributes);
    }

    protected $table = 'ao_themes';

    protected $primaryKey = 'ThemeID';

    protected $fillable = [
        'ThemeID', 'ThemeName', 'Skin', 'Version'
    ];

    protected $hidden = [];

    public $timestamps = false;
}