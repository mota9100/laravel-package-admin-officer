<?php

namespace Mota\AdminOfficer\Models;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model {

    public function __construct(array $attributes = [], $connection = null) {

        if(!is_null($connection)) {

            $this->connection = $connection;
        }

        parent::__construct($attributes);
    }

    protected $table = 'ao_settings';

    protected $primaryKey = 'SettingID';

    protected $fillable = [
        'SettingID', 'AccountID', 'Avatar', 'FileManagerSort', 'FileManagerView'
    ];

    protected $hidden = [];

    public $timestamps = false;

    public function Admin() {

        return $this->hasOne('Mota\AdminOfficer\Models\Account', 'AccountID', 'AccountID');
    }

    public function Theme() {

        return $this->hasOne('Mota\AdminOfficer\Models\Theme', 'ThemeID', 'ThemeID');
    }

    public function Language() {

        return $this->hasOne('Mota\AdminOfficer\Models\Language', 'LanguageID', 'LanguageID');
    }
}