<?php

namespace Mota\AdminOfficer\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Mota\AdminOfficer\Notifications\NotificationResetPassword;

class Account extends Authenticatable {

    use Notifiable;

    public function __construct(array $attributes = [], string $connection = 'mysql') {

        $this->connection = $connection;
        parent::__construct($attributes);
    }

    protected $table = 'ao_accounts';
    protected $primaryKey = 'AccountID';

    protected $fillable = [
        'FirstName', 'LastName', 'email', 'password',
    ];


    protected $hidden = [
        'password', 'remember_token',
    ];

    public $timestamps = true;

    public function Setting() {

        return $this->hasOne('Mota\AdminOfficer\Models\Setting', 'AccountID', 'AccountID');
    }

    public function sendPasswordResetNotification($token) {

        $this->notify(new NotificationResetPassword($token));
    }
}
