<?php

namespace Mota\AdminOfficer\Models;

use Illuminate\Database\Eloquent\Model;

class Language extends Model {

    public function __construct(array $attributes = [], $connection = null) {

        if(!is_null($connection)) {

            $this->connection = $connection;
        }

        parent::__construct($attributes);
    }

    protected $table = 'ao_languages';

    protected $primaryKey = 'LanguageID';

    protected $fillable = [
        'LanguageID', 'Country', 'Language', 'ISO-3166', 'ISO-639'
    ];

    protected $hidden = [];

    public $timestamps = false;
}