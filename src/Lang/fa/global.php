<?php

return [

    'colors' => [
        'red' => 'قرمز',
        'pink' => 'صورتی',
        'purple' => 'بنفش',
        'purple_deep' => 'بنفش تیره',
        'blue_teal' => 'آبی تیلی',
        'blue' => 'آبی',
        'blue_light' => 'آبی روشن',
        'blue_grey' => 'آبی خاکستری',
        'cyan' => 'فیروزه‌ای',
        'teal' => 'تیلی',
        'green' => 'سبز',
        'green_light' => 'سبز روشن',
        'lime' => 'لیمویی',
        'yellow' => 'زرد',
        'amber' => 'کهربایی',
        'orange' => 'نارنجی',
        'orange_deep' => 'نارنجی تیره',
        'brown' => 'قهوه‌ای',
        'grey' => 'خاکستری',
        'black' => 'مشکی'
    ],
    'version' => 'ورژن',
    'loading' => 'در حال بارگذاری ...',
    'forget_password' => 'فراموشی رمز عبور'
];