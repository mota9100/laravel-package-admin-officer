<?php

return [
    'success' => [
        'update' => 'ویرایش :attribute :name باموفقیت انجام شد.',
        'create' => 'ایجاد :attribute :name باموفقیت انجام شد.',
        'delete' => 'حذف :attribute :name باموفقیت انجام شد.',
        'move' => 'انتقال :attribute :name باموفقیت انجام شد.',
        'copy' => 'کپی :attribute :name باموفقیت انجام شد.',
        'crop' => 'برش :attribute با موفقیت انجام شد.',
        'optimize' => 'بهینه سازی :attribute با موفقیت انجام شد.',
        'resize' => 'تغییر اندازه :attribute با موفقیت انجام شد.',
        'rename' => 'تغییر نام :attribute :name :newName باموفقیت انجام شد.',
        'upload' => 'آپلود شد.',
        'upload_processing' => 'در حال بارگذاری...',
        'email' => 'ایمیل :attribute به ایمیل :email ارسال شد.',
        'account_activate' => 'حساب شما با موفقیت فعال شد.',
        'account_sended' => 'ایمیل فعال سازی حساب [admin officer] به آدرس :attribute ارسال شد. لطفا ایمیل خود را چک کنید.'
    ],

    'error' => [
        'update' => 'متاسفانه ویرایش :attribute :name انجام نشد.',
        'create' => 'متاسفانه ایجاد :attribute :name انجام نشد.',
        'delete' => 'متاسفانه حذف :attribute :name انجام نشد.',
        'move' => 'متاسفانه انتقال :attribute :name انجام نشد.',
        'copy' => 'متاسفانه کپی :attribute :name انجام نشد.',
        'crop' => 'متاسفانه برش :attribute انجام نشد.',
        'optimize' => 'متاسفانه بهینه سازی :attribute انجام نشد.',
        'resize' => 'متاسفانه تغییر اندازه :attribute انجام نشد.',
        'rename' => 'متاسفانه تغییر نام :attribute :name انجام نشد.',
        'upload' => 'خطا!',
        'upload_in_valid_size' => 'سایز فایل باید کمتر از :attribute باشذ.',
        'upload_in_valid_extension' => 'پسوند :attribute اجاره آپلود ندارد.',
        'directory_have_child' => 'پوشه :attribute دارای محتوای دیگری است.',
        'not_found' => ':attribute مورد نظر یافت نشد.',
        'is_exist' => ':attribute مورد نظر از قبل وجود دارد.',
        'in_valid_file_type' => 'فایل درخواستی باید از نوع :attribute باشد.',
        'un_valid_name' => 'نام :attribute نمی‌تواند شامل (\/?%*:|"<>) باشد.',
        'permission_denied' => 'شما دسترسی :attribute در این پوشه را ندارید.',
        'admin_login' => 'اطلاعات کاربری وارد شده اشتباه است.',
        'un_know' => 'پیغام نامشخص',
        'wrong_request' => 'درخواست اشتباه است.',
        'internal_error' => 'متاسفانه خطایی رخ داده است. :message',
        'information' => 'اطلاعات وارد شده اشتباه است.',
        'activate_before' => 'این حساب از قبل فعال شده است.',
        'nothing_to_active' => 'هیچ حساب غیر فعالی جهت فعال سازی وجود ندارد.',
        'email_exist' => 'این ایمیل وجود دارد.'
    ]
];