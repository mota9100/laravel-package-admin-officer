<?php

$adminHeadTitle = 'Admin Officer | ';

return [

    'admin_officer' => 'مدیریت سایت',

    'title' => [
        'login' => [
            'title' => 'ورود',
            'head_title' => "$adminHeadTitle ورود مدیر سایت"
        ],
        'register' => [
            'title' => 'ایجاد کاربر جدید',
            'head_title' => "$adminHeadTitle ایجاد کاربر جدید"
        ],
        'dashboard' => [
            'title' => 'داشبورد',
            'head_title' => "$adminHeadTitle داشبورد مدیریت سایت"
        ],
        'file_manager' => [
            'title' => 'مدیریت فایل‌ها',
            'head_title' => "$adminHeadTitle مدیریت فایل‌ها"
        ],
        'profile' => [
            'title' => 'پروفایل',
            'head_title' => "$adminHeadTitle پروفایل"
        ],
        'password_email' => [
            'title' => 'ارسال لینک تغییر گذرواژه',
            'head_title' => "$adminHeadTitle ارسال لینک تغییر گذرواژه"
        ],
        'password_reset' => [
            'title' => 'تغییر گذرواژه',
            'head_title' => "$adminHeadTitle تغییر گذرواژه"
        ]
    ],

    'input' => [
        'optional' => 'اختیاری',
        'text' => [
            'first_name' => 'نام',
            'last_name' => 'نام خانوادگی',
            'avatar' => 'آواتار',
            'search' => 'جستجو',
            'username' => 'نام‌کاربری',
            'email' => 'ایمیل',
            'email_new' => 'ایمیل جدید',
            'password' => 'گذرواژه',
            'password_new' => 'گذرواژه جدید',
            'password_old' => 'گذرواژه قبلی',
            'password_confirm' => 'تایید گذرواژه',
            'captcha' => 'کد امنیتی',
            'folder_name' => 'نام پوشه'
        ],
        'select' => [
            'language' => 'زبان',
        ],
        'check_box' => [
            'remember_me' => 'مرا به خاطر بسپار'
        ]
    ],

    'button' => [
        'create' => [
            'default' => 'ایجاد'
        ],
        'login' => [
            'default' => 'ورود'
        ],
        'register' => [
            'default' => 'ثبت نام'
        ],
        'logout' => [
            'default' => 'خروج'
        ],
        'profile' => [
            'default' => 'پروفایل'
        ],
        'cancel' => [
            'default' => 'لغو'
        ],
        'delete' => [
            'default' => 'حذف'
        ],
        'update' => [
            'default' => 'ویرایش',
            'avatar' => 'ویرایش آواتار',
            'password' => 'ویرایش گذرواژه',
            'reset_password' => 'تغییر گذرواژه'
        ],
        'save' => [
            'default' => 'ذخیره',
            'profile' => 'ذخیره پروفایل'
        ],
        'close' => [
            'default' => 'بستن',
        ],
        'choose' => [
            'default' => 'انتخاب'
        ],
        'send' => [
            'default' => 'ارسال',
            'reset_password_email' => 'ارسال لینک تغییر گذرواژه',
            'activation_email' => 'ارسال مجدد کد فعال‌سازی'
        ]
    ],

    'sub_titles' => [
        'quality' => 'کیفیت'
    ]

];