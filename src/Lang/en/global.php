<?php

return [

    'colors' => [
        'red' => 'Red',
        'pink' => 'Pink',
        'purple' => 'Purple',
        'purple_deep' => 'Deep Purple',
        'blue_teal' => 'Blue Teal',
        'blue' => 'Blue',
        'blue_light' => 'Light Blue',
        'blue_grey' => 'Blue Grey',
        'cyan' => 'Cyan',
        'teal' => 'Teal',
        'green' => 'Green',
        'green_light' => 'Light Green',
        'lime' => 'Lime',
        'yellow' => 'Yellow',
        'amber' => 'Amber',
        'orange' => 'Orange',
        'orange_deep' => 'Deep Orange',
        'brown' => 'Brown',
        'grey' => 'Grey',
        'black' => 'Black'
    ],
    'version' => 'Version',
    'loading' => 'Loading ...',
    'forget_password' => 'Forgot Password'
];