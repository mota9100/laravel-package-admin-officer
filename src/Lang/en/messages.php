<?php

return [
    'success' => [
        'update' => ':attribute :name  edited successfully.',
        'create' => ':attribute :name  created successfully.',
        'delete' => ':attribute :name  deleted successfully.',
        'move' => ':attribute :name moved successfully',
        'copy' => ':attribute :name copied successfully',
        'crop' => 'Image :attribute cropped successfully.',
        'optimize' => 'Image :attribute optimized successfully.',
        'resize' => 'Image :attribute resized successfully.',
        'rename' => ':attribute :name renamed :newName successfully',
        'upload' => 'Success',
        'upload_processing' => 'Processing...',
        'email' => ':attribute Email Send to :email',
        'account_activate' => 'Your account activate successfully.',
        'account_sended' => 'Admin officer activation account email send to :attribute. please check your inbox or spams.'
    ],

    'error' => [
        'update' => 'Unfortunately editing :attribute :name failed.',
        'create' => 'Unfortunately creating :attribute :name  failed.',
        'delete' => 'Unfortunately deleting :attribute :name  failed.',
        'move' => 'Unfortunately moving :attribute :name  failed.',
        'copy' => 'Unfortunately coping :attribute :name  failed.',
        'crop' => 'Unfortunately cropping :attribute  failed.',
        'optimize' => 'Unfortunately optimizing :attribute  failed.',
        'resize' => 'Unfortunately resizing :attribute  failed.',
        'rename' => 'Unfortunately renaming :attribute :name  failed.',
        'upload' => 'Failed',
        'upload_in_valid_size' => 'Size must be lower than :attribute',
        'upload_in_valid_extension' => 'Can not upload :attribute files',
        'directory_have_child' => ':attribute directory have child.',
        'not_found' => 'The requested :attribute :path was not found',
        'is_exist' => 'The requested :attribute is exist.',
        'in_valid_file_type' => 'The requested file must be :attribute',
        'un_valid_name' => 'The requested :attribute can not be contain (\/?%*:|"<>).',
        'permission_denied' => 'You do not have permission to :attribute in this directory.',
        'admin_login' => 'Username or password is incorrect.',
        'unknow' => 'UnKnow Message.',
        'wrong_request' => 'Wrong Request.',
        'internal_error' => 'Somethings Went Wrong. :message',
        'information' => 'The information entered is incorrect.',
        'activate_before' => 'This account activate before.',
        'nothing_to_active' => 'There are no active accounts available for activation.',
        'email_exist' => 'This email exist.'
    ]
];