<?php

$adminHeadTitle = 'Admin Officer | ';

return [

    'admin_officer' => 'Admin Officer',

    'title' => [
        'login' => [
            'title' => 'Login',
            'head_title' => "$adminHeadTitle Admin Login"
        ],
        'register' => [
            'title' => 'Register New User',
            'head_title' => "$adminHeadTitle Register New User"
        ],
        'dashboard' => [
            'title' => 'Dashboard',
            'head_title' => "$adminHeadTitle Dashboard"
        ],
        'file_manager' => [
            'title' => 'File Manager',
            'head_title' => "$adminHeadTitle File Manager"
        ],
        'profile' => [
            'title' => 'Profile',
            'head_title' => "$adminHeadTitle Profile"
        ],
        'password_email' => [
            'title' => 'Send Reset Password Email',
            'head_title' => "$adminHeadTitle Send Reset Password Email"
        ],
        'password_reset' => [
            'title' => 'Reset Password',
            'head_title' => "$adminHeadTitle Reset Password"
        ]
    ],

    'input' => [
        'optional' => 'Optional',
        'text' => [
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'avatar' => 'Avatar',
            'search' => 'Search',
            'username' => 'Username',
            'email' => 'Email',
            'email_new' => 'New Email',
            'password' => 'Password',
            'password_new' => 'New Password',
            'password_confirm' => 'Confirm Password',
            'password_old' => 'Old Password',
            'captcha' => 'Captcha',
            'folder_name' => 'Folder name'
        ],
        'select' => [
            'language' => 'Language',
        ],
        'check_box' => [
            'remember_me' => 'Remember Me'
        ]
    ],

    'button' => [
        'create' => [
            'default' => 'Create'
        ],
        'login' => [
            'default' => 'Login'
        ],
        'register' => [
            'default' => 'Register'
        ],
        'logout' => [
            'default' => 'Logout'
        ],
        'profile' => [
            'default' => 'Profile'
        ],
        'update' => [
            'default' => 'Update',
            'avatar' => 'Update Avatar',
            'password' => 'Update Password',
            'reset_password' => 'Reset Password'
        ],
        'save' => [
            'default' => 'Save',
            'profile' => 'Save Profile'
        ],
        'close' => [
            'default' => 'Close',
        ],
        'choose' => [
            'default' => 'Choose'
        ],
        'send' => [
            'default' => 'Send',
            'reset_password_email' => 'Send Password Reset Link',
            'activation_email' => 'Send Activation Email Again'
        ]
    ],

    'sub_titles' => [
        'quality' => 'Quality'
    ]

];