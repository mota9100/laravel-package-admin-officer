<?php

return [
    'u' => 'upload',
    'r' => 'read',
    'w' => 'write',
    'd' => 'download',
    'verify_account_email' => 'Activation Account'
];