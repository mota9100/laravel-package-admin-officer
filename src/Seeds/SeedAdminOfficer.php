<?php

namespace Mota\AdminOfficer\Seeds;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Mota\AdminOfficer\Tables\TableAccount;
use Mota\AdminOfficer\Tables\TableLanguage;
use Mota\AdminOfficer\Tables\TableSetting;
use Mota\AdminOfficer\Tables\TableTheme;

class SeedAdminOfficer extends Seeder
{

    protected $objTableAccount;
    protected $objTableSetting;
    protected $objTableTheme;
    protected $objTableLanguage;

    public function __construct() {

        $this->objTableAccount = new TableAccount();
        $this->objTableSetting = new TableSetting();
        $this->objTableTheme = new TableTheme();
        $this->objTableLanguage = new TableLanguage();

    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {

        $appName = config('app.name', 'NotFound');

        $this->objTableAccount->Insert([
            'AccountID' => 1,
            'FirstName' => 'Admin',
            'LastName' => 'Officer',
            'email' => "$appName@adminofficer.co",
            'password' => Hash::make($appName),
            'IsActive' => 1
        ]);

        $this->objTableTheme->Insert([
            'ThemeID' => 1,
            'ThemeName' => 'AdminBSB',
            'Skin' => 'teal',
            'Version' => '1.0.0'
        ]);

        $this->objTableLanguage->Insert([
            'LanguageID' => 1,
            'Country' => 'United Kingdom',
            'Language' => 'English',
            'Flag' => 'vendor/mota/AdminOfficer/images/flags/uk.png',
            'Direction' => 'LTR',
            'ISO3166' => 'UK',
            'ISO639' => 'EN'
        ]);

        $this->objTableLanguage->Insert([
            'LanguageID' => 2,
            'Country' => 'Iran',
            'Language' => 'Persian',
            'Flag' => 'vendor/mota/AdminOfficer/images/flags/ir.png',
            'Direction' => 'RTL',
            'ISO3166' => 'IR',
            'ISO639' => 'FA'
        ]);

        $this->objTableSetting->Insert([
            'SettingID' => 1,
            'AccountID' => 1,
            'ThemeID' => 1,
            'FileManagerSort' => 'alphabetic',
            'FileManagerView' => 'grid',
            'LanguageID' => 2,
            'Avatar' => 'vendor/mota/AdminOfficer/images/avatar.png'
        ]);
    }
}
