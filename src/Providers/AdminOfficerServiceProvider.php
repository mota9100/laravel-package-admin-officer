<?php

namespace Mota\AdminOfficer\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Arr;

class AdminOfficerServiceProvider extends ServiceProvider {

    protected $commands = [
        'Mota\AdminOfficer\Console\CommandInstall',
        'Mota\AdminOfficer\Console\CommandUpdate',
        'Mota\AdminOfficer\Console\CommandMakeController',
        'Mota\AdminOfficer\Console\CommandMakeView'
    ];

    protected $routeMiddleware = [
        'admin.officer.auth'     => \Mota\AdminOfficer\Middlewares\MiddlewareAdmin::class,
        'admin.officer.authed'    => \Mota\AdminOfficer\Middlewares\MiddlewareRedirectAdmin::class,
        'admin.officer.first.login'    => \Mota\AdminOfficer\Middlewares\MiddlewareFirstLogin::class,
        'admin.file.manager.multi.user' => \Mota\AdminOfficer\Middlewares\MiddlewareMultiUser::class,
        'admin.file.manager.default.folder' => \Mota\AdminOfficer\Middlewares\MiddlewareCreateDefaultFolder::class
    ];

    public function boot() {

        $this->loadTranslationsFrom(__DIR__.'/../Lang', 'admin_officer');

        $this->loadViewsFrom(__DIR__ . '/../Views','admin_officer');

        $baseViews = resource_path('views/admin');

        if(is_dir($baseViews)) {

            $this->loadViewsFrom($baseViews,'admin_officer_base');
        }

        $this->publishes([__DIR__ . '/../Assets' => public_path('vendor/mota/')], 'admin-officer-assets');
        $this->publishes([__DIR__ . '/../Config/admin-officer.php' => config_path('admin-officer.php')], 'admin-officer-config');
        $this->publishes([__DIR__ . '/../Lang/en/adminofficer.php' => resource_path('lang/en/adminofficer.php')], 'admin-officer-lang');
        $this->publishes([__DIR__ . '/../Lang/fa/adminofficer.php' => resource_path('lang/fa/adminofficer.php')], 'admin-officer-lang');
    }


    public function register() {

        $this->commands($this->commands);

        $this->loadAdminAuthConfig();

        $this->loadAdminStorageConfig();

        $this->registerRouteMiddleware();

        $this->app->singleton('admin_officer', function () {
            return true;
        });
    }

    protected function loadAdminAuthConfig() {

        config(Arr::dot(ao_config('auth', []), 'auth.'));
    }

    protected function loadAdminStorageConfig() {

        config(Arr::dot(ao_config('file_manager.filesystems', []), 'filesystems.'));
    }

    protected function registerRouteMiddleware() {

        foreach ($this->routeMiddleware as $key => $middleware) {

            app('router')->aliasMiddleware($key, $middleware);
        }
    }
}
