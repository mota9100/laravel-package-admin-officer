<?php

namespace Mota\AdminOfficer\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class AdminOfficerRouteServiceProvider extends ServiceProvider {

    protected $namespace = 'Mota\AdminOfficer\Controllers';

    public function boot() {

        parent::boot();
    }

    public function map() {

        $this->mapAdminAuthRoutes();

        $this->mapAdminAuthedRoutes();

        $this->mapAdminFileManagerRoutes();

        $this->mapGlobalRoutes();

        $this->mapBaseRoutes();
    }

    protected function mapAdminAuthRoutes() {

        $adminMiddleware = ao_config('route.middleware', []);
        $adminPrefix = ao_config('route.prefix', 'officer');

        $adminHardMiddleware = [
            'web'
        ];

        $middleware = array_merge($adminHardMiddleware, $adminMiddleware);

        Route::middleware($middleware)
            ->prefix($adminPrefix)
            ->namespace($this->namespace)
            ->group(ao_package_path('Routes/admin.php'));
    }

    protected function mapAdminAuthedRoutes() {

        $adminMiddleware = ao_config('route.middleware_authed', []);
        $adminPrefix = ao_config('route.prefix', 'officer');

        $adminHardMiddleware = [
            'web'
        ];

        $middleware = array_merge($adminHardMiddleware, $adminMiddleware);

        Route::middleware($middleware)
            ->prefix($adminPrefix)
            ->namespace($this->namespace)
            ->group(ao_package_path('Routes/guest.php'));
    }

    protected function mapAdminFileManagerRoutes() {

        $adminMiddleware = ao_config('route.middleware', []);
        $adminPrefix = ao_config('route.prefix', 'officer');

        $adminHardMiddleware = [
            'web'
        ];

        $middleware = array_merge($adminHardMiddleware, $adminMiddleware);

        $fileManagerPrefix = $adminPrefix .
            '/' .
            ao_config('file_manager.url_prefix', ao_config('file_manager.prefix', 'filemanager'));

        Route::middleware($middleware)
            ->prefix($fileManagerPrefix)
            ->namespace($this->namespace)
            ->group(ao_package_path('Routes/file-manager.php'));
    }

    protected function mapGlobalRoutes() {

        $adminPrefix = ao_config('route.prefix', 'officer');

        $adminHardMiddleware = [
            'web'
        ];

        Route::middleware($adminHardMiddleware)
            ->prefix($adminPrefix)
            ->namespace($this->namespace)
            ->group(ao_package_path('Routes/global.php'));
    }

    protected function mapBaseRoutes() {

        $routeFile = base_path('routes/admin.php');

        if (file_exists($routeFile)) {

            $adminMiddleware = ao_config('route.middleware', []);
            $adminPrefix = ao_config('route.prefix', 'officer');
            $adminNamespace = ao_config('route.namespace', 'App\Http\Controllers\AdminOfficer');

            $adminHardMiddleware = [
                'web'
            ];

            $middleware = array_merge($adminHardMiddleware, $adminMiddleware);

            Route::middleware($middleware)
                ->prefix($adminPrefix)
                ->namespace($adminNamespace)
                ->group($routeFile);
        }
    }

}