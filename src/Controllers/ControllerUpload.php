<?php

namespace Mota\AdminOfficer\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Mota\AdminOfficer\Traits\FileManagerHelper;

class ControllerUpload extends Controller {

    use FileManagerHelper;

    public function Upload(Request $request) {

        $uploadInputs = [
            'file' => $request->file('file'),
            'folder_type' => $request->get('folder_type'),
            'work_directory' => $request->get('work_directory'),
        ];

        $uploadRules = [
            'file' => 'required|file',
            'folder_type' => 'required|string',
            'work_directory' => 'required|string',
        ];

        $uploadValidator = $this->AdminValidator($uploadInputs, $uploadRules);

        if($uploadValidator->fails()) {

            return $this->AdminValidatorMessages($uploadValidator);
        }

        $folderType = $uploadInputs['folder_type'];
        $workDirectory =$uploadInputs['work_directory'];
        $workDirectory = ($workDirectory == 'root') ? 'root' : '*';

        $check = $this->CheckPermission($folderType, $workDirectory, 'u');

        if(!$check)
            return $this->TerminalResponse(false)
                ->SetResponseMessage(ao_message('error', 'permission_denied', 'u', [] ,'en'), 'error')
                ->CreateResponse();

        $file = $uploadInputs['file'];
        $folderType = $uploadInputs['folder_type'];
        $workDirectory = $uploadInputs['work_directory'];

        if($file instanceof UploadedFile) {

            if($file->isValid()) {

                $fileName = $file->getClientOriginalName();
                $extension = $file->getClientOriginalExtension();

                $inValidMimes = ao_config('file_manager.not_allowed_mimes', [
                    'php', 'html', 'htm', 'css', 'js', 'aspx', 'asp', 'jsp',
                    'hphp', 'phtml', 'module', 'php4', 'php5', 'inc',
                    '.htaccess', '.env', 'htaccess', 'env', '.git', '.gitignore',
                    'git', 'gitignore'
                ]);

                $uploadedMime = [
                    'mime' => $extension
                ];

                $uploadedMimeRules = [
                    'mime' => 'not_in:' . implode(',', $inValidMimes)
                ];

                $uploadedMimeValidator = $this->AdminValidator($uploadedMime, $uploadedMimeRules, 'en');

                if($uploadedMimeValidator->fails()) {

                    return $this->AdminValidatorMessages($uploadedMimeValidator);
                }

                $currentPath = $this->GetCurrentPath($fileName, $folderType, $workDirectory);

                if($this->CheckExist($currentPath)) {

                    $fileInfo = $this->GetFileNameInfo($currentPath, false);
                    $fileName = $fileInfo['base_name'] . time() . ".{$fileInfo['extension']}";
                    $currentPath = $this->GetCurrentPath($fileName, $folderType, $workDirectory);
                }

                $uploadProcess = $this->UploadFile($this->RemoveName($currentPath), $file, $this->GetName($currentPath));

                if($uploadProcess) {

                    $this->CreateThumbImage($currentPath);
                }

                $uploaded = (is_null($uploadProcess)) ? false : true;
                $uploadStatus = ($uploaded) ? 'success' : 'error';

                return $this->TerminalResponse($uploaded)->SetResponseMessage(
                    ao_message($uploadStatus, 'upload', $this->GetName($currentPath), [], 'en'), $uploadStatus
                )->CreateResponse();
            }

            return $this->GenerateWrongRequest(__LINE__);
        }

        return $this->GenerateWrongRequest(__LINE__);
    }

    public function UploadValidate(Request $request) {

        $this->TerminalRequest($request);

        $uploadValidateInputs =$this->GetRequestData([
            'size',
            'extension',
            'folder_type',
            'work_directory'
        ]);

        $uploadValidateRules = [
            'size' => 'required|numeric',
            'extension' => 'required|string',
            'folder_type' => 'required|string',
            'work_directory' => 'required|string',
        ];

        $uploadValidateValidator = $this->AdminValidator($uploadValidateInputs, $uploadValidateRules, 'en');

        if($uploadValidateValidator->fails()) {

            return $this->AdminValidatorMessages($uploadValidateValidator);
        }

        $folderType = $uploadValidateInputs['folder_type'];
        $workDirectory =$uploadValidateInputs['work_directory'];
        $workDirectory = ($workDirectory == 'root') ? 'root' : '*';

        $check = $this->CheckPermission($folderType, $workDirectory, 'u');

        if(!$check)
            return $this->TerminalResponse(false)
                ->SetResponseMessage(ao_message('error', 'permission_denied', 'u', [] ,'en'), 'error')
                ->CreateResponse();

        $postSize = (int) ini_get('post_max_size');
        $uploadSize = (int) ini_get('upload_max_filesize');

        $postSize = $postSize * 1048576;
        $uploadSize = $uploadSize * 1048576;

        $validSize = ($postSize < $uploadSize) ? $postSize : $uploadSize;
        $inValidMimes = ao_config('file_manager.not_allowed_mimes', [
            'php', 'html', 'htm', 'css', 'js', 'aspx', 'asp', 'jsp',
            'hphp', 'phtml', 'module', 'php4', 'php5', 'inc',
            '.htaccess', '.env', 'htaccess', 'env', '.git', '.gitignore',
            'git', 'gitignore'
        ]);
        $size = (int) $uploadValidateInputs['size'];
        $extension = $uploadValidateInputs['extension'];

        if($size >= $validSize) {

            return $this->TerminalResponse(false)->SetResponseMessage(
                ao_message('error', 'upload_in_valid_size',
                    $this->ResolveSize($validSize), [], 'en'),
                'error'
            )->CreateResponse();
        }

        if(in_array($extension, $inValidMimes)) {

            return $this->TerminalResponse(false)->SetResponseMessage(
                ao_message('error', 'upload_in_valid_extension',
                    $extension, [], 'en'),
                'error'
            )->CreateResponse();
        }

        return $this->TerminalResponse(true)->SetResponseMessage(
            ao_message('success', 'upload_processing', null, [], 'en'),
            'success'
        )->CreateResponse();
    }
}