<?php

namespace Mota\AdminOfficer\Controllers;


use Illuminate\Support\Facades\Auth;
use Mota\AdminOfficer\Tables\TableAccount;
use Mota\AdminOfficer\Tables\TableLanguage;
use Mota\AdminOfficer\Tables\TableSetting;
use Mota\AdminOfficer\Tables\TableVerifyAccount;

class ControllerAdminViews extends Controller
{

    public function Dashboard()
    {

        $baseDashboardUpper = ao_base_view(ao_db_config('theme.current'), 'Dashboard');
        $baseDashboardLower = ao_base_view(ao_db_config('theme.current'), 'dashboard');

        if (view()->exists($baseDashboardUpper)) {

            $view = $baseDashboardUpper;

        } elseif (view()->exists($baseDashboardLower)) {

            $view = $baseDashboardLower;

        } else {

            $view = ao_view(ao_db_config('theme.current'), 'Dashboard');
        }


        return view($view);
    }

    public function FileManager()
    {

        $view = ao_view(ao_db_config('theme.current'), 'FileManager');

        return view($view);
    }

    public function Register($email = null) {

        $objTableAccount = new TableAccount();

        if(!is_null($email)) {

            $checkExistAccount = $objTableAccount->GetCount([
                ['email', '=', $email],
                ['isactive', '=', 0]
            ]);

            if($checkExistAccount <= 0) {

                return redirect()->route('admin.officer.register')->with('error', [
                    ao_message('error', 'nothing_to_active')
                ]);
            }
        }

        $view = ao_view(ao_db_config('theme.current'), 'Register');

        return view($view)->with([
            'email' => $email
        ]);
    }

    public function RegisterActivation($email, $token) {

        $objTableVerifyAccount = new TableVerifyAccount();
        $objTableAccount = new TableAccount();
        $objTableSetting = new TableSetting();

        $verify = $objTableVerifyAccount->SelectFirst([
            ['email', '=', $email],
            ['token', '=', $token]
        ]);

        $account = $objTableAccount->SelectFirst([
            ['email', '=', $email],
            ['isactive', '=', 0]
        ]);

        if (is_null($verify) || is_null($account))
            return view(ao_view('Errors', 'ExpireToken'));

        $expireTokenTime = (int) config('auth.activation.admin_officer.expire', 30);
        $time = time();
        $tokenIsExpire = ($time - strtotime($verify->created_at)) > (60 * $expireTokenTime);

        if ($tokenIsExpire)
            return view(ao_view('Errors', 'ExpireToken'));

        $objTableAccount->CustomUpdate([
            ['email', '=', $email]
        ], [
            'isactive' => 1
        ]);

        $appName = config('app.name');
        $initEmail = "$appName@adminofficer.co";

        $checkExistInitAccount = $objTableAccount->SelectFirst([
            ['email', '=', $initEmail]
        ]);

        if(!is_null($checkExistInitAccount)) {

            $guard = config('auth.defaults.admin_officer');
            $authAccount = Auth::guard($guard)->user();

            if($authAccount->email == $initEmail) {

                Auth::guard($guard)->logout();
                Auth::guard($guard)->loginUsingId($account->AccountID);
            }

            $objTableSetting->CustomDelete([
                ['accountid', '=', $checkExistInitAccount->AccountID]
            ]);

            $objTableAccount->CustomDelete([
                ['accountid', '=', $checkExistInitAccount->AccountID]
            ]);
        }

        return redirect()->route('admin.officer.dashboard')->with('success', [
            ao_message('success', 'account_activate')
        ]);
    }

    public function Profile()
    {

        $objTableAccount = new TableAccount();
        $objTableLanguage = new  TableLanguage();

        $guard = config('auth.defaults.admin_officer', null);

        $adminId = Auth::guard($guard)->id();

        $admin = $objTableAccount->SelectFirst([
            ['accountid', '=', $adminId]
        ]);

        $adminExist = !is_null($admin);
        $settingExist = ($adminExist) ? !is_null($admin->Setting) : false;

        $profile = (object)[
            'FirstName' => ($adminExist) ? $admin->FirstName : null,
            'LastName' => ($adminExist) ? $admin->LastName : null,
            'Email' => ($adminExist) ? $admin->email : null,
            'LanguageID' => ($settingExist) ? $admin->Setting->LanguageID : null,
            'Avatar' => ($settingExist) ? $admin->Setting->Avatar : null
        ];

        $multiLang = (ao_config('multi_language', false)) ? 1 : 0;

        $languages = ao_make_aoo($objTableLanguage->SelectGet()->toArray());

        $view = ao_view(ao_db_config('theme.current'), 'Profile');

        return view($view)->with([
            'profile' => $profile,
            'languages' => $languages,
            'multiLanguageStatus' => $multiLang
        ]);
    }
}