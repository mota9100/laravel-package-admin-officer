<?php

namespace Mota\AdminOfficer\Controllers;


use Illuminate\Http\Request;
use Mota\AdminOfficer\Traits\FileManagerHelper;

class ControllerDownload extends Controller {

    use FileManagerHelper;

    public function __invoke(Request $request) {

        $downloadInputs = [
            'folder_type' => $request->get('folder_type'),
            'work_directory' => $request->get('work_directory'),
            'file_name' => $request->get('file_name')
        ];

        $downloadRules = [
            'folder_type' => 'required|string',
            'work_directory' => 'required|string',
            'file_name' => 'required|string'
        ];

        $validator = $this->AdminValidator($downloadInputs, $downloadRules);

        if($validator->fails())
            abort(404);

        $folderType = $request->get('folder_type');
        $workDirectory = $request->get('work_directory');
        $fileName = $request->get('file_name');

        $getPath = $this->GetCurrentPath($fileName, $folderType, $workDirectory);

        if(!$this->CheckExist($getPath))
            abort(404);

        return response()->download($this->SystemPath($getPath));
    }
}