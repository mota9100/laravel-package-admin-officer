<?php
namespace Mota\AdminOfficer\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Mota\AdminOfficer\Traits\AdminOfficerHelper;
use Mota\AdminOfficer\Traits\ControllerHelper;

abstract class Controller extends BaseController {

    use AuthorizesRequests, DispatchesJobs, ValidatesRequests, ControllerHelper, AdminOfficerHelper;
}