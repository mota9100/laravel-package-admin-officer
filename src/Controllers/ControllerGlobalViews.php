<?php

namespace Mota\AdminOfficer\Controllers;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Password;
use Mota\AdminOfficer\Utilities\Captcha\Captcha;

class ControllerGlobalViews extends Controller {

    //use SendsPasswordResetEmails;
    use ResetsPasswords;

    protected $redirectTo = '/officer';

    public function LoginView() {

        $view = ao_view('AdminBSB', 'auth.Login');

        return view($view);
    }

    public function RequestResetPasswordLinkView() {

        $view = ao_view('AdminBSB','auth.passwords.Email');

        return view($view);
    }

    public function ResetPasswordView(Request $request, $token = null) {

        $view = ao_view('AdminBSB','auth.passwords.Reset');

        return view($view)->with([
            'token' => $token,
            'email' => $request->email
        ]);
    }

    public function Captcha(Request $request) {

        $objCaptcha = new Captcha();
        $captcha = $objCaptcha->CreateImage();
        $request->session()->put('admin_officer_captcha', $captcha);
        return;
    }

    protected function guard() {

        return Auth::guard('admin_officer');
    }

    protected function broker() {

        return Password::broker('admin_officer');
    }
}