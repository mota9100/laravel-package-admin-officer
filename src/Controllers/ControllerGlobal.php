<?php

namespace Mota\AdminOfficer\Controllers;

use Illuminate\Http\Request;
use Mota\AdminOfficer\Tables\TableAccount;

class ControllerGlobal extends Controller {

    public function Terminal(Request $request) {

        $this->TerminalRequest($request);

        switch ($this->GetRequestDefine()) {

            case 'glb-00-00':

                $objTableAccount = new TableAccount();

                $loginInputs = $this->GetRequestData([
                    'email',
                    'password',
                    'remember',
                    'captcha'
                ]);

                $loginRules = [
                    'email' => 'required|email',
                    'password' => 'required|string',
                    'remember' => 'required|numeric|in:0,1',
                    'captcha' => 'required'
                ];

                $sessionCaptcha = session('admin_officer_captcha');
                $captcha = trim(strtoupper($sessionCaptcha)) === trim(strtoupper($loginInputs['captcha']));

                $validator = $this->AdminValidator($loginInputs, $loginRules);

                if($validator->fails() || !$captcha) {

                    $customMessages = [];

                    if(!$captcha) {
                        $customMessages[] = 'کد امنیتی اشتباه است.';
                    }

                    return $this->AdminValidatorMessages($validator, __LINE__, $customMessages);
                }

                $remember = ($loginInputs['remember'] == 1) ? true : false;

                $login = $objTableAccount->Login([
                    'email' => $loginInputs['email'],
                    'password' => $loginInputs['password'],
                    'isactive' => 1
                ], $remember);

                if($login->GetStatus()) {

                    return $this->TerminalResponse(true)
                        ->CreateResponse();

                }

                return $this->TerminalResponse(false)
                    ->SetResponseMessage(ao_message('error', 'admin_login'), 'error', true)
                    ->CreateResponse();

                break;
        }
    }

}