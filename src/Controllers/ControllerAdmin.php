<?php

namespace Mota\AdminOfficer\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Mota\AdminOfficer\Tables\TableAccount;
use Mota\AdminOfficer\Tables\TableSetting;
use Mota\AdminOfficer\Tables\TableTheme;
use Mota\AdminOfficer\Tables\TableVerifyAccount;

class ControllerAdmin extends Controller {

    public function Terminal(Request $request) {

        $this->TerminalRequest($request);

        if(strpos($request->url(), 'register')) {

            $allowedDefines = [
                'adm-03-00',
                'adm-03-01'
            ];

            if(!in_array($this->GetRequestDefine(), $allowedDefines)) {

                return $this->GenerateWrongRequest(__LINE__);
            }
        }

        switch ($this->GetRequestDefine()) {

            case 'adm-00-00':

                return $this->Init();

                break;

            case 'adm-00-01':

                return $this->LogOut();

                break;

            case 'adm-00-02':

                return $this->ChangeSettingSkin();

                break;

            case 'adm-01-00':

                return $this->ChangeSettingFileManagerSort();

                break;

            case 'adm-01-01':

                return $this->ChangeSettingFileManagerView();

                break;

            case 'adm-02-00':

                return $this->UpdateProfile();

                break;

            case 'adm-03-00':

                return $this->Register();

                break;

            case 'adm-03-01':

                return $this->SendActivationEmail();

                break;

            default:

                return $this->GenerateWrongRequest(__LINE__);

                break;
        }
    }

    private function Init() {

        return $this->TerminalResponse(true)
            ->CreateResponse();
    }

    private function LogOut() {

        $objTableAccount = new TableAccount();

        $logout = $objTableAccount->LogOut();
        $logoutStatus = $logout->GetStatus();
        $logoutData = $logout->GetData();

        return $this->TerminalResponse($logoutStatus)
            ->SetResponseData([
                'url' => $logoutData->Url
            ])->CreateResponse();
    }

    private function ChangeSettingSkin() {

        $objTableTheme = new TableTheme();

        $ChangeInputs = $this->GetRequestData([
            'skin'
        ]);

        $ChangeRules = [
            'skin' => 'required'
        ];

        $validator = $this->AdminValidator($ChangeInputs, $ChangeRules);

        if($validator->fails()) {

            return $this->AdminValidatorMessages($validator, __LINE__);
        }

        $objTableTheme->UpdateTheme([
            'skin' => $ChangeInputs['skin']
        ], ao_db_config('theme.current'));

        return $this->TerminalResponse(true)
            ->SetResponseMessage(ao_message('success', 'update', 'skin'), 'success', true)
            ->CreateResponse();
    }

    private function ChangeSettingFileManagerSort() {

        $objTableSetting = new TableSetting();

        $guard = config('auth.defaults.admin_officer', null);

        $adminId = Auth::guard($guard)->id();

        $ChangeInputs = $this->GetRequestData([
            'fileManagerSort'
        ]);

        $validSortTypes = ao_config('file_manager.sort_type', [
            'time',
            'alphabetic'
        ]);

        $ChangeRules = [
            'fileManagerSort' => 'string|required|in:'  . implode(',', $validSortTypes),
        ];

        $validator = $this->AdminValidator($ChangeInputs, $ChangeRules);

        if($validator->fails()) {

            return $this->AdminValidatorMessages($validator, __LINE__);
        }

        $objTableSetting->UpdateSetting([
            'filemanagersort' => $ChangeInputs['fileManagerSort']
        ], $adminId);

        return $this->TerminalResponse(true)
            ->SetResponseMessage(ao_message('success', 'update', 'sort'), 'success', true)
            ->CreateResponse();
    }

    private function ChangeSettingFileManagerView() {

        $objTableSetting = new TableSetting();

        $guard = config('auth.defaults.admin_officer', null);

        $adminId = Auth::guard($guard)->id();

        $ChangeInputs = $this->GetRequestData([
            'fileManagerView'
        ]);

        $validViewTypes = ao_config('file_manager.view_type', [
            'grid',
            'list'
        ]);

        $ChangeRules = [
            'fileManagerView' => 'required|string|in:'  . implode(',', $validViewTypes),
        ];

        $validator = $this->AdminValidator($ChangeInputs, $ChangeRules);

        if($validator->fails()) {

            return $this->AdminValidatorMessages($validator, __LINE__);
        }

        $objTableSetting->UpdateSetting([
            'filemanagerview' => $ChangeInputs['fileManagerView']
        ], $adminId);

        return $this->TerminalResponse(true)
            ->SetResponseMessage(ao_message('success', 'update', 'view'), 'success', true)
            ->CreateResponse();
    }

    private function UpdateProfile() {

        $objTableAccount = new TableAccount();
        $objTableSetting = new TableSetting();

        $guard = config('auth.defaults.admin_officer', null);
        $admin = Auth::guard($guard)->user();

        $multiLang = ao_config('multi_language', false);

        $ChangeInputs = $this->GetRequestData([
            'avatar',
            'email',
            'first_name',
            'last_name',
            'language',
            'password_old',
            'password',
            'password_confirmation'
        ]);

        $ChangeRules = [
            'avatar' => 'required|string',
            'email' => 'required|email',
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'language' => ($multiLang) ? 'required|numeric|exists:tb_ao_languages,languageid' : 'nullable',
            'password_old' => 'required_with:password_new',
            'password' => 'required_with:password_old|confirmed|min:6',
        ];

        $validator = $this->AdminValidator($ChangeInputs, $ChangeRules);

        if($validator->fails()) {

            return $this->AdminValidatorMessages($validator, __LINE__);
        }

        $avatar = urldecode(trim($ChangeInputs['avatar'], '/'));
        $email = $ChangeInputs['email'];
        $firstName = $ChangeInputs['first_name'];
        $lastName = $ChangeInputs['last_name'];
        $language = $ChangeInputs['language'];
        $passwordOld = $ChangeInputs['password_old'];
        $passwordNew = $ChangeInputs['password'];

        $validateAvatar = file_exists(public_path($avatar)) === true;

        $shouldChangePassword = (is_null($passwordOld) || $passwordOld == '') ? false : true;
        $login = true;

        if($shouldChangePassword) {

            $login = Hash::check($passwordOld, $admin->password);
        }

        if($validateAvatar !== true || ($shouldChangePassword === true && $login !== true))
            return $this->TerminalResponse(false)
                ->SetResponseMessage(ao_message('error', 'information'), 'warning')
                ->CreateResponse();

        $adminOfficerUpdates = [
            'firstname' => $firstName,
            'lastname' => $lastName,
            'email' => $email,
        ];

        if($shouldChangePassword)
            $adminOfficerUpdates['password'] = Hash::make($passwordNew);

        $settingUpdates = [
            'avatar' => $avatar
        ];

        if(!is_null($language)) {

            $settingUpdates['languageid'] = $language;
        }

        $objTableAccount->UpdateWithID($admin->AccountID, $adminOfficerUpdates);
        $objTableSetting->UpdateSetting($settingUpdates, $admin->AccountID);

        if($shouldChangePassword)
            $objTableAccount->LogOut();

        return $this->TerminalResponse(true)
            ->SetResponseMessage(ao_message('success', 'update', 'profile'), 'success', true)
            ->CreateResponse();
    }

    private function Register() {

        $objTableAccount = new TableAccount();
        $objTableSetting = new TableSetting();
        $objTableVerifyAccount = new TableVerifyAccount();

        $ChangeInputs = $this->GetRequestData([
            'email',
            'first_name',
            'last_name',
            'password',
            'password_confirmation'
        ]);

        $ChangeRules = [
            'email' => 'required|email',
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'password' => 'required|confirmed|min:6',
        ];

        $validator = $this->AdminValidator($ChangeInputs, $ChangeRules);

        if($validator->fails()) {

            return $this->AdminValidatorMessages($validator, __LINE__);
        }

        $email = $ChangeInputs['email'];
        $firstName = $ChangeInputs['first_name'];
        $lastName = $ChangeInputs['last_name'];
        $passwordNew = $ChangeInputs['password'];

        $checkExistAccount = $objTableAccount->SelectFirst([
            'email' => $email
        ]);

        if(!is_null($checkExistAccount)) {

            if($checkExistAccount->IsActive = 1) {

                return $this->TerminalResponse(false)
                    ->SetResponseMessage(ao_message('error', 'email_exist'), 'error')
                    ->CreateResponse();

            } else {

                session()->flash('success', [
                    ao_message('success', 'email', 'verify_account_email', [
                        'email' => $email
                    ])
                ]);

                $objTableVerifyAccount->SendVerifyLink($checkExistAccount->AccountID);

                return $this->TerminalResponse(true)
                    ->SetResponseData([
                        'redirect' => route('admin.officer.register', [
                            'email' => $email
                        ])
                    ])->CreateResponse();
            }
        }

        $accountId = (int) $objTableAccount->InsertGetID([
            'firstname' => $firstName,
            'lastname' => $lastName,
            'email' => $email,
            'password' => Hash::make($passwordNew),
            'isactive' => 0
        ]);

        if($accountId == 0)
            return $this->TerminalResponse(false)
                ->SetResponseMessage(ao_message('error', 'internal_error'), 'warning')
                ->CreateResponse();

        $objTableSetting->Insert([
            'accountid' => $accountId,
            'themeid' => 1,
            'filemanagersort' => 'alphabetic',
            'filemanagerview' => 'grid',
            'languageid' => 2,
            'avatar' => 'vendor/mota/AdminOfficer/images/avatar.png'
        ]);

        $objTableVerifyAccount->SendVerifyLink($accountId);

        session()->flash('success', [
            ao_message('success', 'create', 'account'),
            ao_message('success', 'email', 'verify_account_email', [
                'email' => $email
            ])
        ]);

        return $this->TerminalResponse(true)
            ->SetResponseData([
                'redirect' => route('admin.officer.register', [
                    'email' => $email
                ])
            ])->CreateResponse();
    }

    private function SendActivationEmail() {

        $objTableVerifyAccount = new TableVerifyAccount();
        $objTableAccount = new TableAccount();

        $inputs = $this->GetRequestData([
            'email'
        ]);

        $rules = [
            'email' => 'required|email|exists:tb_ao_accounts,email'
        ];

        $validator = $this->AdminValidator($inputs, $rules);

        if($validator->fails()) {

            return $this->AdminValidatorMessages($validator, __LINE__);
        }

        $email = $inputs['email'];

        $account = $objTableAccount->SelectFirst([
            ['email', '=', $email],
            ['isactive', '=', 0]
        ]);

        if(is_null($account)) {

            session()->flash('error', [
                ao_message('error', 'nothing_to_active')
            ]);

            return $this->TerminalResponse(true)
                ->SetResponseData([
                    'redirect' => route('admin.officer.register')
                ])->CreateResponse();
        }

        $objTableVerifyAccount->SendVerifyLink($account->AccountID);

        return $this->TerminalResponse(true)
            ->SetResponseMessage(ao_message('success', 'email', 'verify_account_email', [
                'email' => $email
            ]), 'success')
            ->CreateResponse();
    }
}