<?php
/*
 * TODO: Fix icon Key In FinalGenerator()
 * TODO: Fix type Key In FinalGenerator()
 * TODO: Add permission to private functions
 * TODO: Add try catch for storage engine
 */
namespace Mota\AdminOfficer\Controllers;

use Illuminate\Http\Request;
use Mota\AdminOfficer\Traits\FileManagerHelper;

class ControllerFileManager extends Controller {

    use FileManagerHelper;

    private $terminalPermissions = [
        'fmg-00' => ['r'],
        'fmg-01' => ['r'],
        'fmg-02' => ['r'],
        'fmg-03' => ['w'],
        'fmg-04' => ['w'],
        'fmg-05' => ['d'],
        'fmg-06' => ['w'],
        'fmg-07' => ['w'],
        'fmg-08' => ['w'],
        'fmg-09' => ['r', 'w'],
        'fmg-10' => ['w'],
        'fmg-11' => ['w'],
        'fmg-12' => ['r', 'w'],
        'fmg-13' => ['w'],
        'fmg-14' => ['r', 'w'],
        'fmg-15' => ['r', 'w'],
    ];

    public function Terminal(Request $request) {

        $this->TerminalRequest($request);

        $initInputs =  $this->GetRequestData([
            'folderType',
            'workDirectory'
        ]);

        $availableFolders = [
            'user',
            'share'
        ];

        $initRules = [
            'folderType' => 'required|in:' . implode(',', $availableFolders),
            'workDirectory' => "required"
        ];

        $validator = $this->AdminValidator($initInputs, $initRules);

        if($validator->fails())
            return $this->GenerateWrongRequest(__LINE__);

        $this->InitDirectories();
        $this->SetRequest($initInputs);

        if(!$this->CheckExist($this->GetCurrentPath()))
            return $this->GenerateWrongRequest(__LINE__);

        $checkPermission = $this->CheckControllerPermissions();

        if(!$checkPermission->status)
            return $this->TerminalResponse(false)
                ->SetResponseMessage(ao_message('error', 'permission_denied', $checkPermission->permission), 'error')
                ->CreateResponse();

        switch ($this->GetRequestDefine()) {

            case 'fmg-00':

                $finalReturn = $this->GetMainView();

                break;

            case 'fmg-01':

                $finalReturn = $this->GetRootFolders();

                break;

            case 'fmg-02':

                $finalReturn = $this->GetPathItems();

                break;

            case 'fmg-03':

                $finalReturn = $this->NewFolder();

                break;

            case 'fmg-04':

                $finalReturn = $this->Delete();

                break;

            case 'fmg-05':

                $finalReturn = $this->GetFileUrl();

                break;

            case 'fmg-06':

                $finalReturn = $this->DoMove('move');

                break;

            case 'fmg-07':

                $finalReturn = $this->DoCopy();

                break;

            case 'fmg-08':

                $finalReturn = $this->DoMove('rename');

                break;

            case 'fmg-09':

                $finalReturn = $this->GetCrop();

                break;

            case 'fmg-10':

                $finalReturn = $this->Crop(false);

                break;

            case 'fmg-11':

                $finalReturn = $this->Crop(true);

                break;

            case 'fmg-12':

                $finalReturn =$this->GetResize();

                break;

            case 'fmg-13':

                $finalReturn =$this->Resize();

                break;

            case 'fmg-14':

                $finalReturn =$this->Optimize();

                break;

            default:

                $finalReturn = $this->GenerateWrongRequest(__LINE__);

                break;
        }

        return $finalReturn;
    }

    private function CheckControllerPermissions() {

        $terminalDefine = $this->GetRequestDefine();

        if(key_exists($terminalDefine, $this->terminalPermissions)) {

            $essentialPermissions = $this->terminalPermissions[$terminalDefine];
            $workDirectory = $this->CurrentWorkDir();
            $folderType = $this->CurrentFolderType();
            $workDirectory = ($workDirectory == 'root') ? 'root' : '*';

            foreach ($essentialPermissions as $essentialPermission) {

                $check = $this->CheckPermission($folderType, $workDirectory, $essentialPermission);

                if(!$check)
                    return (object) [
                        'status' => false,
                        'permission' => $essentialPermission
                    ];
            }

            return (object) [
                'status' => true,
                'permission' => 'r'
            ];
        }

        return (object) [
            'status' => false,
            'permission' => 'r'
        ];
    }

    private function GetMainView() {

        $getMainViewInputs = $this->GetRequestData([
            'selectable'
        ]);

        $getMainViewRules = [
            'selectable' => 'required|string|in:true,false',
        ];

        $getMainViewValidator = $this->AdminValidator($getMainViewInputs, $getMainViewRules);

        if($getMainViewValidator->fails()){

            //return $this->AdminValidatorMessages($getMainViewValidator);
            return $this->GenerateWrongRequest(__LINE__);
        }

        $selectable = $getMainViewInputs['selectable'];
        $selectableBool = ($selectable === 'true') ? true : false;

        $view = ao_view(ao_db_config('theme.current'), 'file-manager.Index');

        try {
            $html = view($view)
                ->with([
                    'selectable' => $selectableBool
                ])->render();

        } catch (\Throwable $exception) {

            return $this->TerminalResponse(false)->SetResponseMessage(
                ao_message('error', 'internal_error'),
                'error'
            )->CreateResponse();
        }

        return $this->TerminalResponse(true)
            ->SetResponseData([
                'html' => $html
            ])->CreateResponse();
    }

    private function GetRootFolders() {

        $rootFolders = [];

        $folders = [
            'user' => 'root',
            'share' => 'root'
        ];

        foreach ($folders as $folderType => $folder) {

            $directories = $this->GetCurrentDirectories(null, $folderType, $folder);

            $children = array_map(function($directory) {

                return (object) $directory;

            }, $directories->get('directories')->toArray());

            usort($children, function ($a, $b) {

                return strcmp($a->name, $b->name);
            });

            array_push($rootFolders, (object) [
                'name' => ucfirst($folderType),
                'path' => $directories['path'],
                'children' => $children,
                'has_next' => ! ($folder == end($folders)),
            ]);

        }

        $view = ao_view(ao_db_config('theme.current'), 'file-manager.Tree');

        try {

            $html = view($view)->with([
                'root_folders' => $rootFolders
            ])->render();

        } catch (\Throwable $exception) {

            return $this->TerminalResponse(false)->SetResponseMessage(
                ao_message('error', 'internal_error'),
                'error'
            )->CreateResponse();
        }

        return $this->TerminalResponse(true)->SetResponseData([
                'html' => $html
            ])->CreateResponse();
    }

    private function NewFolder() {

        $newFolderInputs = $this->GetRequestData([
            'name'
        ]);

        $newFolderRules = [
            'name' => 'required|string|not_in:'  . implode(',', $this->systemPath)
        ];

        $newFolderValidator = $this->AdminValidator($newFolderInputs, $newFolderRules);

        if($newFolderValidator->fails()){

            return $this->AdminValidatorMessages($newFolderValidator, __LINE__);
        }

        $newFolderName = $newFolderInputs['name'];

        if(!$this->FileNameIsValid($newFolderName)) {

            return $this->TerminalResponse(false)
                ->SetResponseMessage(
                    ao_message('error', 'un_valid_name', 'folder'), 'warning'
                )->CreateResponse();
        }

        $newFolderPath = $this->GetCurrentPath($newFolderName);

        if ($this->CheckExist($newFolderPath)) {

            return $this->TerminalResponse(false)
                ->SetResponseMessage(
                    ao_message('error', 'is_exist', 'folder'), 'warning'
                )->CreateResponse();
        }

        $createFolder = $this->CreateDirectory($newFolderPath);

        $messageStatus = ($createFolder) ? 'success' : 'error';

        return $this->TerminalResponse($createFolder)
            ->SetResponseMessage(
                ao_message($messageStatus, 'create', 'folder'), $messageStatus
            )->CreateResponse();
    }

    private function GetPathItems() {

        $validSortTypes = ao_config('file_manager.sort_type', [
            'time',
            'alphabetic'
        ]);

        $validViewTypes = ao_config('file_manager.view_type', [
            'grid',
            'list'
        ]);

        $allowedSelectable = ao_config('file_manager.selectable_types', [
            'all',
            'image',
            'video',
            'application'
        ]);

        $getItemInputs = $this->GetRequestData([
            'sort',
            'view',
            'selectabletype'
        ]);

        $getItemRules = [
            'sort' => 'required|string|in:'  . implode(',', $validSortTypes),
            'view' => 'required|string|in:'  . implode(',', $validViewTypes),
            'selectabletype' => 'required|in:' . implode(',', $allowedSelectable)
        ];

        $getItemValidator = $this->AdminValidator($getItemInputs, $getItemRules);

        if($getItemValidator->fails()){

            return $this->AdminValidatorMessages($getItemValidator, __LINE__);
        }

        $sortType = $getItemInputs['sort'];
        $viewType = $getItemInputs['view'];
        $selectableType = $getItemInputs['selectabletype'];

        $typeAllow = ($selectableType === 'all') ? null : $selectableType;

        $getCurrentDirectories = $this->GetCurrentDirectories();
        $getCurrentFiles = $this->GetCurrentFiles(null, null, null, $typeAllow);
        $getPreviousPath = $this->GetPreviousPath();

        $directories = $this->SortFilesAndDirectories(array_map(function($directory) {

            return (object) $directory;

        }, $getCurrentDirectories->get('directories')->toArray()), $sortType);

        $files = $this->SortFilesAndDirectories(array_map(function($file) {

            return (object) $file;

        }, $getCurrentFiles->get('files')->toArray()), $sortType);

        $view = ao_view(ao_db_config('theme.current'), "file-manager.$viewType-view");

        try {

            $html = view($view)->with([
                'filesCount' => count($files),
                'directoriesCount' => count($directories),
                'items' => array_merge($directories, $files),
                'previousFolderType' => $getPreviousPath['folder_type'],
                'previousWorkDirectory' => $getPreviousPath['work_directory'],
            ])->render();

        } catch (\Throwable $exception) {

            return $this->TerminalResponse(false)->SetResponseMessage(
                ao_message('error', 'internal_error'),
                'error'
            )->CreateResponse();
        }

        return $this->TerminalResponse(true)->SetResponseData([
            'html' => $html
        ])->CreateResponse();
    }

    private function Delete() {

        $deleteInputs = $this->GetRequestData([
            'items'
        ]);

        $deleteRules = [
            'items' => 'required|array|min:1'
        ];

        $deleteValidator = $this->AdminValidator($deleteInputs, $deleteRules);

        if($deleteValidator->fails())
            return $this->AdminValidatorMessages($deleteValidator);


        $items = $deleteInputs['items'];

        $itemPaths = array_map(function ($item) {

            $itemPath = $this->GetCurrentPath($item);

            return ($this->CheckExist($itemPath)) ? $itemPath : null;

        }, $items);

        if(in_array(null, $itemPaths))
            return $this->GenerateWrongRequest(__LINE__);


        $return = $this->TerminalResponse(true);

        foreach ($itemPaths as $itemPath) {

            if($this->IsFile($itemPath)) {

                $thumbPath = null;

                if($this->FileIsImage($itemPath)) {

                    $thumbPath = $this->GetThumbPath($itemPath);
                }

                $delete = $this->DeleteFile($itemPath);

                $deleteStatus = ($delete) ? 'success' : 'error';

                $return->SetResponseMessage(
                    ao_message($deleteStatus, 'delete', 'file', [
                        'name' => $this->GetName($itemPath)
                    ]),
                    $deleteStatus
                );

                if($thumbPath)
                    $this->DeleteFile($thumbPath);

            } else {

                $directoryDirectories = $this->GetCurrentDirectories(null,null, $itemPath);
                $directoryFiles = $this->GetCurrentFiles(null,null, $itemPath);

                if($directoryDirectories->get('directories')->count() > 0 ||
                    $directoryFiles->get('files')->count() > 0) {

                    $return->SetResponseMessage(
                        ao_message('error', 'directory_have_child', $this->GetName($itemPath)),
                        'warning'
                    );

                    continue;
                }

                $delete = $this->DeleteDirectory($itemPath);

                $deleteStatus = ($delete) ? 'success' : 'error';

                $return->SetResponseMessage(
                    ao_message($deleteStatus, 'delete', 'directory', [
                        'name' => $this->GetName($itemPath)
                    ]),
                    $deleteStatus
                );
            }
        }

        return $return->CreateResponse();
    }

    private function GetFileUrl() {

        $downloadInputs = $this->GetRequestData([
            'file'
        ]);

        $downloadRules = [
            'file' => 'required|string'
        ];

        $downloadValidator = $this->AdminValidator($downloadInputs, $downloadRules);

        if($downloadValidator->fails()) {

            return $this->AdminValidatorMessages($downloadValidator);
        }

        $file = $downloadInputs['file'];

        $filePath = $this->GetCurrentPath($file);

        $return = $this->TerminalResponse(true);

        $responseData = [
            'url' => null
        ];

        if (!$this->CheckExist($filePath)) {

            $return->SetResponseMessage(
                ao_message('error', 'not_found', 'file', [
                    'path' => $this->GetName($filePath)
                ]), 'error'
            );
        } else {

            $responseData['url'] = $this->GetUrl($filePath);
        }

        return  $return->SetResponseData($responseData)->CreateResponse();
    }

    private function DoMove(string $action = 'move') {

        $moveInputs = $this->GetRequestData([
            'items'
        ]);

        $moveRules = [
            'items' => 'required|array|min:1',
            'items.old' => 'required|array|min:1',
            'items.old.*.folder_type' => 'required|string',
            'items.old.*.work_directory' => 'required|string',
            'items.old.*.item' => 'required|string',
            'items.new' => 'required|array|min:1',
            'items.new.*' => 'string'
        ];

        $moveValidator = $this->AdminValidator($moveInputs, $moveRules);

        if($moveValidator->fails()) {

            return $this->AdminValidatorMessages($moveValidator);
            //return $this->GenerateWrongRequest(__LINE__);
        }

        $oldInput = $moveInputs['items']['old'];
        $newInput = $moveInputs['items']['new'];

        if(count($oldInput) != count($newInput)) {

            return $this->GenerateWrongRequest(__LINE__);
        }

        $wrongResponse = $this->TerminalResponse(false);

        $oldInput = array_map(function ($oldInputItems) use ($wrongResponse) {

            $oldFolderType = $this->RemoveSlash($oldInputItems['folder_type']);
            $oldWorkDirectory = $this->RemoveSlash($oldInputItems['work_directory']);
            $oldItem = $this->RemoveSlash($oldInputItems['item']);

            $oldPath = $this->GetCurrentPath($oldItem, $oldFolderType, $oldWorkDirectory);
            $oldPathType = $this->IsFile($oldPath) ? 'file' : 'directory';

            if(!$this->CheckExist($oldPath)) {

                $wrongResponse->SetResponseMessage(
                        ao_message('error', 'not_found', $oldPathType, [
                            'path' => $this->GetName($oldPath)
                        ]), 'error'
                    );

                return null;
            }

            return [
                'path' => $oldPath,
                'type' => $oldPathType
            ];

        }, $oldInput);

        if(in_array(null, $oldInput)) {

            return $wrongResponse->CreateResponse();
        }


        $newInput = array_map(function ($newInputItem, $newInputKey) use ($wrongResponse, $oldInput) {

            $newPath = $this->GetCurrentPath($newInputItem);

            if($this->CheckExist($newPath)) {

                $wrongResponse->SetResponseMessage(
                        ao_message('error', 'is_exist', $oldInput[$newInputKey]['type']),
                        'error'
                    );

                return null;
            }

            if(!$this->FileNameIsValid($this->GetName($newPath))) {

                $wrongResponse->SetResponseMessage(
                        ao_message('error', 'un_valid_name', $oldInput[$newInputKey]['type']),
                        'error'
                    );

                return null;
            }

            return $newPath;

        }, $newInput, array_keys($newInput));

        if(in_array(null, $newInput)) {

            return $wrongResponse->CreateResponse();
        }

        $movingItems = array_combine($newInput, $oldInput);

        $finalResponse = $this->TerminalResponse(true);

        foreach ($movingItems as $new => $old) {

            $move = $this->Move($old['path'], $new);
            $moveStatus = ($move) ? 'success' : 'error';

            $replaces = [
                'name' => $this->GetName($old['path']),
            ];

            if($action == 'rename' && $move)
                $replaces['newName'] = $this->GetName($new);

            // deleted $replaces for say old name and new name
            $finalResponse->SetResponseMessage(
                ao_message($moveStatus, $action, $old['type']),
                $moveStatus
            );
        }

        return $finalResponse->CreateResponse();
    }

    private function DoCopy() {

        $copyInputs = $this->GetRequestData([
            'items'
        ]);

        $copyRules = [
            'items' => 'required|array|min:1',
            'items.old' => 'required|array|min:1',
            'items.old.*.folder_type' => 'required|string',
            'items.old.*.work_directory' => 'required|string',
            'items.old.*.item' => 'required|string',
            'items.new' => 'required|array|min:1',
            'items.new.*' => 'string'
        ];

        $copyValidator = $this->AdminValidator($copyInputs, $copyRules);

        if($copyValidator->fails()) {

            return $this->AdminValidatorMessages($copyValidator);
            //return $this->GenerateWrongRequest(__LINE__);
        }

        $oldInput = $copyInputs['items']['old'];
        $newInput = $copyInputs['items']['new'];

        if(count($oldInput) != count($newInput)) {

            return $this->GenerateWrongRequest(__LINE__);
        }

        $wrongResponse = $this->TerminalResponse(false);


        $oldInput = array_map(function ($oldInputItems) use ($wrongResponse) {

            $oldFolderType = $this->RemoveSlash($oldInputItems['folder_type']);
            $oldWorkDirectory = $this->RemoveSlash($oldInputItems['work_directory']);
            $oldItem = $this->RemoveSlash($oldInputItems['item']);

            $oldPath = $this->GetCurrentPath($oldItem, $oldFolderType, $oldWorkDirectory);
            $oldPathType = $this->IsFile($oldPath) ? 'file' : 'directory';

            if(!$this->CheckExist($oldPath)) {

                $wrongResponse->SetResponseMessage(
                    ao_message('error', 'not_found', $oldPathType, [
                        'path' => $this->GetName($oldPath)
                    ]), 'error'
                );

                return null;
            }

            return [
                'path' => $oldPath,
                'type' => $oldPathType
            ];

        }, $oldInput);


        if(in_array(null, $oldInput)) {

            return $wrongResponse->CreateResponse();
        }

        $newInput = array_map(function ($newInputItem, $newInputKey) use ($wrongResponse, $oldInput) {

            $newPath = $this->GetCurrentPath($newInputItem);

            if($this->CheckExist($newPath)) {

                $newNameTmp = time() . '_' .$this->GetName($newPath);
                $newPath = $this->ReplaceName($newPath, $newNameTmp);
            }

            if(!$this->FileNameIsValid($this->GetName($newPath))) {

                $wrongResponse->SetResponseMessage(
                    ao_message('error', 'un_valid_name', $oldInput[$newInputKey]['type']),
                    'error'
                );

                return null;
            }

            return $newPath;

        }, $newInput, array_keys($newInput));

        if(in_array(null, $newInput)) {

            return $wrongResponse->CreateResponse();
        }

        $copingItems = array_combine($newInput, $oldInput);

        $finalResponse = $this->TerminalResponse(true);

        foreach ($copingItems as $new => $old) {


            $copy = $this->Copy($old['path'], $new);
            $copyStatus = ($copy) ? 'success' : 'error';

            $finalResponse->SetResponseMessage(
                ao_message($copyStatus, 'copy', $old['type'], [
                    'name' => $this->GetName($old['path'])
                ]),
                $copyStatus
            );
        }

        return $finalResponse->CreateResponse();
    }

    private function GetCrop() {

        $getCropInputs = $this->GetRequestData([
            'image'
        ]);

        $getCropRules = [
            'image' => 'required|string'
        ];

        $getCropValidator = $this->AdminValidator($getCropInputs, $getCropRules);

        if($getCropValidator->fails()) {

            return $this->AdminValidatorMessages($getCropValidator);
        }

        $image = $getCropInputs['image'];

        $imageDetails = $this->GetCurrentFiles($image)->get('file');

        if(is_null($imageDetails)) {

            return $this->TerminalResponse(false)->SetResponseMessage(
                ao_message('error', 'not_found', 'file', [
                    'path' => $image
                ]), 'error'
            )->CreateResponse();
        }

        if(!$this->FileIsImage($imageDetails['path'])) {

            return $this->TerminalResponse(false)->SetResponseMessage(
                ao_message('error', 'in_valid_file_type', 'image'),
                'error'
            )->CreateResponse();
        }

        $view = ao_view(ao_db_config('theme.current'), 'file-manager.Crop');

        try {

            $html = view($view)->with([
                'image' => (object) $imageDetails,
                'workDirectory' => $this->CurrentWorkDir()
            ])->render();

        } catch (\Throwable $exception) {

            return $this->TerminalResponse(false)->SetResponseMessage(
                ao_message('error', 'internal_error'),
                'error'
            )->CreateResponse();
        }

        return $this->TerminalResponse(true)
            ->SetResponseData([
                'html' => $html
            ])->CreateResponse();
    }

    private function Crop($overWrite) {

        $cropInputs = $this->GetRequestData([
            'x',
            'y',
            'height',
            'width',
            'image'
        ]);

        $cropRules = [
            'x' => 'required|numeric',
            'y' => 'required|numeric',
            'height' => 'required|numeric',
            'width' => 'required|numeric',
            'image' => 'required|string'
        ];

        $cropValidator = $this->AdminValidator($cropInputs, $cropRules);

        if($cropValidator->fails()) {

            return $this->AdminValidatorMessages($cropValidator);
        }

        $x = $cropInputs['x'];
        $y = $cropInputs['y'];
        $height = $cropInputs['height'];
        $width = $cropInputs['width'];
        $image = $cropInputs['image'];

        $imagePath = $this->GetCurrentPath($image);

        if(!$this->FileIsImage($imagePath)) {

            return $this->TerminalResponse(false)->SetResponseMessage(
                ao_message('error', 'in_valid_file_type', 'image'),
                'error'
            )->CreateResponse();
        }

        $crop = $this->ImageCrop($imagePath, [
            'width' => $width,
            'height' => $height,
            'x' => $x,
            'y' => $y
        ], $overWrite);

        $cropStatus = ($crop) ? 'success' : 'error';

        return $this->TerminalResponse($crop)->SetResponseMessage(
            ao_message($cropStatus, 'crop', $this->GetName($imagePath)),
            $cropStatus
        )->CreateResponse();
    }

    private function GetResize() {

        $getResizeInputs = $this->GetRequestData([
            'image'
        ]);

        $getResizeRules = [
            'image' => 'required|string'
        ];

        $getResizeValidator = $this->AdminValidator($getResizeInputs, $getResizeRules);

        if($getResizeValidator->fails()) {

            return $this->AdminValidatorMessages($getResizeValidator);
        }


        $image = $getResizeInputs['image'];

        $ratio = 1.0;
        $scaled = false;
        $imagePath = $this->GetCurrentPath($image);


        if(!$this->FileIsImage($imagePath)) {

            return $this->TerminalResponse(false)->SetResponseMessage(
                ao_message('error', 'in_valid_file_type', 'image'),
                'error'
            )->CreateResponse();
        }

        $imageInfo = $this->ImageInfo($imagePath);

        if(is_null($imageInfo)) {

            return $this->TerminalResponse(false)->SetResponseMessage(
                ao_message('error', 'internal_error', null, [
                    'message' => 'can not recognize image info'
                ]),
                'error'
            )->CreateResponse();
        }

        $originalWidth = $imageInfo['width'];
        $originalHeight = $imageInfo['height'];

        if ($originalWidth > 600) {

            $ratio = 600 / $originalWidth;
            $width = $originalWidth * $ratio;
            $height = $originalHeight * $ratio;
            $scaled = true;

        } else {

            $width = $originalWidth;
            $height = $originalHeight;
        }

        if ($height > 400) {

            $ratio = 400 / $originalHeight;
            $width = $originalWidth * $ratio;
            $height = $originalHeight * $ratio;
            $scaled = true;
        }

        $view = ao_view(ao_db_config('theme.current'), 'file-manager.Resize');

        try {
            $html = view($view)->with([
                'image' => $this->GetCurrentFiles($image),
                'height' => number_format($height, 0),
                'width' => $width,
                'original_height' => $originalHeight,
                'original_width' => $originalWidth,
                'scaled' => $scaled,
                'ratio' => $ratio
            ])->render();

        } catch (\Throwable $exception) {

            return $this->TerminalResponse(false)->SetResponseMessage(
                ao_message('error', 'internal_error'),
                'error'
            )->CreateResponse();
        }

        return $this->TerminalResponse(true)
            ->SetResponseData([
                'html' => $html
            ])->CreateResponse();
    }

    private function Resize() {

        $resizeInputs = $this->GetRequestData([
            'image',
            'width',
            'height'
        ]);

        $resizeRules = [
            'image' => 'required|string',
            'width' => 'required|numeric',
            'height' => 'required|numeric'
        ];

        $resizeValidator = $this->AdminValidator($resizeInputs, $resizeRules);

        if($resizeValidator->fails()) {

            return $this->AdminValidatorMessages($resizeValidator);
        }

        $image = $resizeInputs['image'];

        $width = $resizeInputs['width'];
        $height = $resizeInputs['height'];

        $imagePath = $this->GetCurrentPath($image);

        if(!$this->FileIsImage($imagePath)) {

            return $this->TerminalResponse(false)->SetResponseMessage(
                ao_message('error', 'in_valid_file_type', 'image'),
                'error'
            )->CreateResponse();
        }

        $resize = $this->ImageResize($imagePath, [
            'width' => $width,
            'height' => $height,
        ]);

        $resizeStatus = ($resize) ? 'success' : 'error';

        return $this->TerminalResponse($resize)->SetResponseMessage(
            ao_message($resizeStatus, 'resize', $this->GetName($imagePath)),
            $resizeStatus
        )->CreateResponse();
    }

    private function Optimize() {

        $inouts = $this->GetRequestData([
            'image',
            'quality',
            'jpeg',
            'overwrite',
            'temporary'
        ]);

        $rules = [
            'image' => 'required|string',
            'quality' => 'required|numeric|between:0,100',
            'jpeg' => 'required|numeric|between:0,100',
            'overwrite' => 'required|numeric|in:0,1',
            'temporary' => 'required|numeric|in:0,1'
        ];

        $Validator = $this->AdminValidator($inouts, $rules);

        if($Validator->fails()) {

            return $this->AdminValidatorMessages($Validator);
        }

        $image = $inouts['image'];
        $quality = (int) $inouts['quality'];
        $jpeg = (int) $inouts['jpeg'];
        $overwrite = (bool) ((int) $inouts['overwrite']);
        $temporary = (bool) ((int) $inouts['temporary']);

        $imagePath = $this->GetCurrentPath($image);

        if(!$this->FileIsImage($imagePath)) {

            return $this->TerminalResponse(false)
                ->SetResponseMessage(
                ao_message('error', 'in_valid_file_type', 'image'),
                'error'
            )->CreateResponse();
        }

        if($temporary) {

            $imagePath = $this->ImageMakeTemp($imagePath, 'optimize');

        } elseif(!$overwrite) {

            $imageInfp = $this->ImageInfo($imagePath);

            $optimizePath =
                $imageInfp['dir'] .
                $this->slash .
                'optimized_' .
                date('Y-m-d_H-i-s_') .
                $imageInfp['basename'];

            $this->Copy($imagePath, $optimizePath);
            $imagePath = $optimizePath;
        }

        if($jpeg > 0) {

            $this->ImageConvertToJpeg($imagePath, $jpeg);
        }

        if($quality > 0) {

            $this->ImageOptimizer($imagePath, $quality);
        }

        return $this->TerminalResponse(true)
            ->SetResponseMessage(
            ao_message('success', 'optimize', $this->GetName($imagePath)),
            'success'
        )->SetResponseData([
            'url' => $this->GetUrl($imagePath)
            ])->CreateResponse();
    }
}