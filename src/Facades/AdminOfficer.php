<?php

namespace Mota\AdminOfficer\Facades;


use Illuminate\Support\Facades\Facade;

class AdminOfficer extends Facade {

    protected static function getFacadeAccessor() {

        return 'admin_officer';
    }
}