<?php

namespace Mota\AdminOfficer\Middlewares;

use Closure;
use Illuminate\Support\Facades\Auth;

class MiddlewareRedirectAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::guard(ao_config('auth.defaults.admin_officer'))->check()) {

            $redirectUrl = '/' . ao_config('route.prefix');
            return redirect($redirectUrl);
        }

        return $next($request);
    }
}
