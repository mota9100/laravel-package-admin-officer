<?php

namespace Mota\AdminOfficer\Middlewares;

use Closure;
use Illuminate\Support\Facades\Auth;

class MiddlewareAdmin
{

    public function handle($request, Closure $next) {

        $guard = config('auth.defaults.admin_officer');

        if(Auth::guard($guard)->check()) {

            return $next($request);
        }

        if($request->wantsJson()) {

            return response()->json([
                "header" => [
                    "result" => false,
                ],
                "body" => [
                    'command' => 'logout'
                ],
            ]);
        }

        $redirectUrl = '/' . ao_config('route.prefix') . '/login';

        return redirect($redirectUrl);
    }
}
