<?php

namespace Mota\AdminOfficer\Middlewares;

use Closure;
use Illuminate\Support\Facades\Auth;

class MiddlewareFirstLogin {

    public function handle($request, Closure $next) {

        $account = Auth::guard('admin_officer')->user();

        $appName = config('app.name', 'NotFound');
        $defaultEmail = "$appName@adminofficer.co";

        if($defaultEmail == $account->email) {

            return redirect()->route('admin.officer.register');
        }

        return $next($request);
    }
}