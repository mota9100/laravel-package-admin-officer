<?php

namespace Mota\AdminOfficer\Middlewares;

use Closure;
use Mota\AdminOfficer\Traits\FileManagerHelper;

class MiddlewareCreateDefaultFolder {

    use FileManagerHelper;

    public function handle($request, Closure $next) {

        $this->CheckDefaultFolderExists('user');
        $this->CheckDefaultFolderExists('share');

        return $next($request);
    }

    private function CheckDefaultFolderExists($type = 'share') {

        if ($type === 'user' && ! $this->AllowMultiUser()) {

            return;
        }

        if ($type === 'share' && ! $this->AllowShareFolder()) {

            return;
        }

        $path = $this->GetRootFolderPath($type);

        $this->CreateFolderByPath($path);
    }
}
