<?php

namespace Mota\AdminOfficer\Middlewares;

use Closure;
use Mota\AdminOfficer\Traits\FileManagerHelper;

class MiddlewareMultiUser {

    use FileManagerHelper;

    public function handle($request, Closure $next) {

        if ($this->AllowMultiUser()) {

            $previousDir = $request->input('working_dir');
            $working_dir = $this->RootFolder('user');

            if ($previousDir == null) {

                $request->merge(compact('working_dir'));

            } elseif (! $this->ValidDir($previousDir)) {

                $request->replace(compact('working_dir'));
            }
        }

        return $next($request);
    }

    private function ValidDir($previousDir)
    {
        if (starts_with($previousDir, $this->RootFolder('share'))) {

            return true;
        }

        if (starts_with($previousDir, $this->RootFolder('user'))) {

            return true;
        }

        return false;
    }
}
