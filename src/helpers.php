<?php

if(!function_exists('ao_log')) {

    function ao_log($message) {

        $message = date('Y-m-d H:i:s') . "\t" . $message . "\n";
        error_log($message, 3, base_path('admin-officer.log'));
    }
}

if(!function_exists('fm_base_url')) {

    function fm_base_url($path = '') {

        $prefix = '/'.trim(ao_config('file_manager.url_prefix', ao_config('file_manager.prefix', 'filemanager')), '/');

        $prefix = ($prefix == '/') ? '' : $prefix;

        return $prefix.'/'.trim($path, '/');
    }
}

if(!function_exists('ao_current_route')) {

    function ao_current_route() {

        $url = url()->current();
        $url = str_replace('https://', '', $url);
        $url = str_replace('http://', '', $url);

        $array = explode('/', $url);

        $adminUrl = '/';

        foreach ($array as $key => $item) {

            if($key <= 1) {
                continue;
            }

            $adminUrl .= '/'.$item;
        }

        return '/'. trim($adminUrl, '/');
    }
}

if(!function_exists('ao_package_path')) {

    function ao_package_path($path = '')
    {
        return __DIR__ . '/' . trim($path, '/');
    }
}

if(!function_exists('ao_base_path')) {

    function ao_base_path($path = '')
    {
        return ucfirst(ao_config('directory')).($path ? DIRECTORY_SEPARATOR.$path : $path);
    }
}

if(!function_exists('ao_base_url')) {

    function ao_base_url($path = '') {

        $prefix = '/'.trim(ao_config('route.prefix'), '/');

        $prefix = ($prefix == '/') ? '' : $prefix;

        return $prefix.'/'.trim($path, '/');
    }
}

if(!function_exists('ao_url')) {

    function ao_url($path = '', $parameters = [], $secure = null)
    {
        if (\Illuminate\Support\Facades\URL::isValidUrl($path)) {
            return $path;
        }

        $secure = $secure ?: ao_config('secure');

        return url(ao_base_url($path), $parameters, $secure);
    }
}

if(!function_exists('ao_fm_base_url')) {

    function ao_fm_base_url($path = '') {

        $adminOfficerPrefix = '/'.trim(ao_config('route.prefix'), '/');

        $fileManagerPrefix = '/'.trim(ao_config('file_manager.url_prefix', ao_config('file_manager.prefix', 'filemanager')), '/');

        $prefix = $adminOfficerPrefix . $fileManagerPrefix;

        $prefix = (($prefix == '/') || ($prefix == '//')) ? '' : $prefix;

        return $prefix.'/'.trim($path, '/');
    }
}

if(!function_exists('ao_fm_url')) {

    function ao_fm_url($path = '', $parameters = [], $secure = null)
    {
        if (\Illuminate\Support\Facades\URL::isValidUrl($path)) {
            return $path;
        }

        $secure = $secure ?: ao_config('secure');

        return url(ao_fm_base_url($path), $parameters, $secure);
    }
}

if(!function_exists('ao_asset')) {

    function ao_asset($path) {

        $version = ao_config('version', 'dev');

        return asset( 'vendor/mota/AdminOfficer/' . trim($path, '/'), ao_config('secure')) . '?v=' . $version;
    }
}

if(!function_exists('ao_trans')) {

    function ao_trans(string $key, string $targetTrans = null, $replace = [], $lang = null) {

        $defaultLang = (is_null($lang)) ? ao_lang('iso639') : $lang;
        $defaultLang = ($defaultLang) ? strtolower($defaultLang) : $defaultLang;

        $keyArray = explode('.', $key);

        if(count($keyArray) <= 0)
            return null;

        if(count($keyArray) > 1) {

            $attributesKey = "admin_officer::attributes";
            $attributes = trans($attributesKey, [], $defaultLang);
            $attributes = ($attributes != $attributesKey) ? $attributes : [];

            if(is_null($targetTrans)) {

                $attributeTrans = '';

            } else {

                if(key_exists($targetTrans, $attributes)) {

                    $attributeTrans = $attributes[$targetTrans];

                } else {

                    $attributeTrans = $targetTrans;
                }
            }

            $attributeReplace = [
                'attribute' => $attributeTrans
            ];

            $replace = array_merge($attributeReplace, $replace);
        }

        $resourceLang = resource_path("lang/$defaultLang/adminofficer.php");
        if($keyArray[0] == 'adminofficer' && file_exists($resourceLang)) {

            $trans = trans($key, $replace, $defaultLang);

        } else {

            $trans = trans("admin_officer::$key", $replace, $defaultLang);
        }

        return (is_string($trans)) ? preg_replace('/\s+/', ' ',
            preg_replace('/(?!\b)(:\w+\b)/', '', $trans)) : $trans;
    }
}

if(!function_exists('ao_config')) {

    function ao_config($key, $default = null) {

        return config("admin-officer.$key", $default);
    }
}

if(!function_exists('ao_db_config')) {

    function ao_db_config($require = null, $default = null) {

        $info = [
            'firstname' => null,
            'lastname' => null,
            'fullname' => null,
            'email' => null,
            'setting' => [
                'avatar' => null,
                'lang' => null,
                'filemanager' => [
                    'sort' => null,
                    'list' => null
                ]
            ],
            'theme' => [
                'current' => null,
                'skin' => null,
                'version' => null
            ]
        ];

        $guard = config('auth.defaults.admin_officer');

        $accountId = \Illuminate\Support\Facades\Auth::guard($guard)->id();

        if(!is_null($accountId)) {

            $account = (new \Mota\AdminOfficer\Tables\TableAccount)->SelectFirst([
                ['accountid', '=', $accountId]
            ]);

            if(!is_null($account)) {

                $info['firstname'] = $account->FirstName;
                $info['lastname'] = $account->LastName;
                $info['fullname'] = $account->FirstName . ' ' . $account->LastName;
                $info['email'] = $account->email;
                $info['setting']['avatar'] = '/'.$account->Setting->Avatar;
                $info['setting']['lang'] = strtolower($account->Setting->Language->ISO639);
                $info['setting']['filemanager']['sort'] = $account->Setting->FileManagerSort;
                $info['setting']['filemanager']['list'] = $account->Setting->FileManagerView;
                $info['theme']['current'] = $account->Setting->Theme->ThemeName;
                $info['theme']['skin'] = $account->Setting->Theme->Skin;
                $info['theme']['version'] = $account->Setting->Theme->Version;
            }
        }

        if(!is_null($require)) {

            $requireInArray = explode('.', $require);

            $return = null;

            foreach ($requireInArray as $item) {

                if(is_null($return)) {

                    if(key_exists($item, $info)) {

                        $return = $info[$item];
                    } else {

                        break;
                    }

                } else {

                    if(key_exists($item, $return)) {

                        $return = $return[$item];

                    } else {

                        $return = null;
                        break;
                    }
                }
            }

            return ((!is_null($return)) ? $return : (is_null($default)) ? $return : $default);
        }

        return $info;
    }
}

if(!function_exists('ao_view')) {

    function ao_view($theme, $view) {

        return "admin_officer::$theme.$view";
    }
}

if(!function_exists('ao_base_view')) {

    function ao_base_view($theme, $view) {

        return "admin_officer_base::$theme.$view";
    }
}

if(!function_exists('ao_lang')) {

    function ao_lang($require = null, $default = null) {

        $iso639 = ao_db_config('setting.lang', config('app.locale', 'en'));

        $lang = [
            'languageid' => null,
            'country' => null,
            'language' => null,
            'flag' => null,
            'direction' => null,
            'iso3166' => null,
            'iso639' => null
        ];

        $language = (new \Mota\AdminOfficer\Tables\TableLanguage())->SelectFirst([
            ['iso639', '=', strtoupper($iso639)]
        ]);

        if(!is_null($language)) {

            $lang['languageid'] = $language->LanguageID;
            $lang['country'] = $language->Country;
            $lang['language'] = $language->Language;
            $lang['flag'] = $language->Flag;
            $lang['direction'] = $language->Direction;
            $lang['iso3166'] = $language->ISO3166;
            $lang['iso639'] = $language->ISO639;
        }


        if(!is_null($require)) {

            $requireInArray = explode('.', $require);

            $return = null;

            foreach ($requireInArray as $item) {

                if(is_null($return)) {

                    if(key_exists($item, $lang)) {

                        $return = $lang[$item];

                    } else {

                        break;
                    }

                } else {

                    if(key_exists($item, $return)) {

                        $return = $return[$item];

                    } else {

                        $return = null;
                        break;
                    }
                }
            }

            return ((!is_null($return)) ? $return : (is_null($default)) ? $return : $default);
        }

        return $lang;
    }
}

if(!function_exists('ao_message')) {

    function ao_message($messageType, $messageKey, $targetAction = null, $replace = [], $lang = null) {

        switch ($messageType) {

            case 'error':

                return ao_trans('messages.error.' . $messageKey, $targetAction, $replace, $lang);

                break;
            case 'success':

                return ao_trans('messages.success.' . $messageKey, $targetAction, $replace, $lang);

                break;
            default:

                return ao_trans('messages.error.un_know');

                break;
        }
    }
}

if(!function_exists('ao_make_aoo')) {

    function ao_make_aoo(array $array) {

        $newArray = [];

        foreach ($array as $arrayKey => $arrayValue) {

            if(is_array($arrayValue)) {

                $newArray[$arrayKey] = (object) $arrayValue;

            } else {

                $newArray[$arrayKey] = $arrayValue;
            }
        }

        return $newArray;
    }
}

if(!function_exists('ao_id')) {

    function ao_id() {

        $guard = config('auth.defaults.admin_officer');

        $id = \Illuminate\Support\Facades\Auth::guard($guard)->id();

        return $id;
    }
}

if(!function_exists('ao_user')) {

    function ao_user() {

        $guard = config('auth.defaults.admin_officer');

        $user = \Illuminate\Support\Facades\Auth::guard($guard)->user();

        return $user;
    }
}