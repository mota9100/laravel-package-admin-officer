<?php

namespace Mota\AdminOfficer\Traits;

use Mota\AdminOfficer\Utilities\PersianDigit;
use Illuminate\Http\Request;
use Illuminate\Validation\Validator;
use Illuminate\Translation\Translator;
use Illuminate\Translation\FileLoader;
use Illuminate\Filesystem\Filesystem;

trait ControllerHelper {

    protected $thisRequest;

    private function GetRequestParameters($object, $key = null) {

        if(is_null($key))
            return (array) $object;

        $keyType = gettype($key);

        switch ($keyType) {

            case 'string':

                if(!isset($object))
                    return null;

                if(!property_exists($object, $key))
                    return null;

                return $object->$key;

                break;

            case 'array':

                $resultArray = [];

                foreach ($key as $keyItem) {

                    if(!isset($object)) {

                        $resultArray[$keyItem] = null;
                        continue;
                    }


                    if(!property_exists($object, $keyItem)) {

                        $resultArray[$keyItem] = null;
                        continue;
                    }

                    $resultArray[$keyItem] = $object->$keyItem;
                }

                return $resultArray;

                break;

            default:

                return null;

                break;
        }
    }

    protected function TerminalRequest(Request $request) {

        $this->thisRequest = $request;

        $objPersianDigit = new PersianDigit();

        $this->terminalDefine = null;
        $this->terminalData = (object)[];
        $this->terminalTable = (object)[];

        $header = $request->get('header');
        $body = $request->get('body');

        if(is_array($header)) {

            if(key_exists('define', $header)) {

                $this->terminalDefine = $header['define'];
            }
        }

        if(is_array($body)) {

            if (key_exists('data', $body)) {

                $data = $body['data'];

                if(is_array($data)) {

                    if(is_array($data)) {

                        foreach ($data as $key => $item) {

                            $data[$key] = $objPersianDigit->ReversePersianDigit($item);
                        }
                    }

                    $this->terminalData = (object) $data;
                }
            }

            if (key_exists('table', $body)) {

                $table = $body['table'];

                if(is_array($table)) {

                    if(key_exists('identityvalue', $table)) {

                        if(is_array($table['identityvalue'])) {

                            foreach ($table['identityvalue'] as $key => $item) {

                                $table['identityvalue'][$key] = $objPersianDigit->ReversePersianDigit($item);
                            }
                        }
                    }

                    if(key_exists('fields', $table)) {

                        if(is_array($table['fields'])) {

                            foreach ($table['fields'] as $key => $item) {

                                $table['fields'][$key] = $objPersianDigit->ReversePersianDigit($item);
                            }
                        }
                    }

                    $this->terminalTable = (object) $table;
                }
            }
        }
    }

    protected function GetRequestDefine() {

        if(!isset($this->terminalDefine))
            return null;

        return $this->terminalDefine;
    }

    protected function GetRequestData($key = null) {

        return $this->GetRequestParameters($this->terminalData, $key);
    }

    protected function GetRequestTable($key = null) {

        return $this->GetRequestParameters($this->terminalTable, $key);
    }

    protected function GetRequestTableFields($key) {

        $fields = $this->GetRequestParameters('fields', $this->terminalTable);

        if(is_null($fields) || !is_array($fields)) {

            $keyType = gettype($key);

            switch ($keyType) {

                case 'array':

                    $resultArray = [];

                    foreach ($key as $keyItem) {

                        $resultArray[$keyItem] = null;
                    }

                    return $resultArray;

                    break;
                case 'string':
                default:

                    return null;

                    break;
            }
        }

        $objFields = (object) $fields;

        return $this->GetRequestParameters($key, $objFields);
    }

    protected function TerminalResponse(bool $status) {

        $this->responseMessages = [];
        $this->responseData = [];
        $this->responseTable = [];
        $this->responseStatus = $status;

        return $this;
    }

    protected function SetResponseMessage(string $text, string $type = null, bool $outsideClick = true, $code = null, bool $require = true, $debug = null) {

        $message = [
            'text' => $text,
            'type' => ($type) ? $type : 'info',
            'outsideclick' => $outsideClick
        ];

        if(!is_null($code)) {

            $message['code'] = $code;
        }

        if(!is_null($debug)) {

            $message['debug'] = $debug;
        }

        if($require) {

            $this->responseMessages[] = $message;
        }

        return $this;
    }

    protected function SetResponseData(array $data) {

        foreach ($data as $dataItem => $dataValue) {

            $this->responseData[strtolower($dataItem)] = $dataValue;
        }

        return $this;
    }

    protected function SetResponseTable(int $pageCurrent, int $pageCount, int $pageRow, array $row) {

        $this->responseTable['pagecurrent'] = $pageCurrent;
        $this->responseTable['pagecount'] = $pageCount;
        $this->responseTable['pagerow'] = $pageRow;
        $this->responseTable['row'] = $row;

        return $this;
    }

    protected function SetResponseCookie(string $cookieName, string $cookieValue, int $expire = null) {

        if(!isset($this->responseCookies))
            $this->responseCookies = [];

        $this->responseCookies[] = (object) [
            'Type' => 'set',
            'Name' => $cookieName,
            'Valus' => $cookieValue,
            'Expire' => (!is_null($expire) && is_int($expire)) ? $expire : 30
        ];

        return $this;
    }

    protected function ForgetResponseCookie(string $cookieName) {

        if(!isset($this->responseCookies))
            $this->responseCookies = [];

        $this->responseCookies[] = (object) [
            'Type' => 'forget',
            'Name' => $cookieName
        ];

        return $this;
    }

    protected function CreateResponse() {

        $returnArray = [
            'header' => [
                'result' => $this->responseStatus
            ],
            'body' => []
        ];

        if(count($this->responseMessages) > 0) {

            $returnArray['body']['message'] = $this->responseMessages;
        }

        if(count($this->responseData) > 0) {

            $returnArray['body']['data'] = $this->responseData;
        }

        if(count($this->responseTable) > 0) {

            $returnArray['body']['table'] = $this->responseTable;
        }

        $create = response()->json($returnArray);

        if(isset($this->responseCookies)) {

            foreach ($this->responseCookies as $cookie) {

                switch ($cookie->Type) {

                    case 'set':

                        $create->cookie($cookie->Name, $cookie->Value, $cookie->Expire);

                        break;

                    case 'forget':

                        $create->withCookie(Cookie::forget($cookie->Name));

                        break;
                }
            }
        }

        return $create;
    }

    protected function GenerateWrongRequest(int $line, Validator $validator = null) {

        $debug = null;

        if($validator instanceof Validator) {

            $debug = [];

            $errorMessages = $validator->errors()->getMessages();

            foreach ($errorMessages as $errorMessage) {

                foreach ($errorMessage as $key => $value) {

                    $debug[] = $value;
                }
            }
        }

        return $this->TerminalResponse(false)
            ->SetResponseMessage(ao_message('error', 'wrong_request'), 'warning', true, $line, true, $debug)
            ->CreateResponse();
    }

    public function AdminValidator($inputs, $rules, $lang = null) {

        $defaultLang = (is_null($lang)) ? ao_db_config('setting.lang', 'fa') : $lang;
        $langPath = ao_package_path('Lang');
        $fileSystem = new Filesystem();
        $fileLoader = new FileLoader($fileSystem, $langPath);
        $translator = new Translator($fileLoader, $defaultLang);

        $validator = new Validator($translator, $inputs, $rules);
        $validator->setPresenceVerifier(app()['validation.presence']);
        return $validator;
    }

    public function AdminValidatorMessages(Validator $validator, $validatorLine = null, $customMessages = []) {

        $response = $this->TerminalResponse(false);

        if($validator->errors()->any()){

            $validatorMessages = $validator->errors()->getMessages();

            foreach ($validatorMessages as $thisValidatorMessages) {

                foreach ($thisValidatorMessages as $validatorMessage) {

                    $response->SetResponseMessage($validatorMessage, 'warning', true, $validatorLine);
                }
            }
        }

        foreach ($customMessages as $customMessage) {

            $response->SetResponseMessage($customMessage, 'warning', true, __LINE__);
        }

        return $response->CreateResponse();
    }
}