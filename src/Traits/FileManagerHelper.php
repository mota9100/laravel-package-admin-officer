<?php

namespace Mota\AdminOfficer\Traits;

use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Imagick;
use Intervention\Image\Facades\Image;
use Mota\AdminOfficer\Utilities\FileManager\ConfigHandler;
use Mota\AdminOfficer\Utilities\PersianCalender;

trait FileManagerHelper {

    private $request = [];
    private $permissions = [
        'root' => [
            'root' => '- - - -',
            '*' => '- - - -'
        ],
        'user' => [
            'root' => 'r - - -',
            '*' => 'r w d u'
        ],
        'share' => [
            'root' => 'r w d u',
            '*' => 'r w d u'
        ],
    ];
    protected $systemPath = [
        'Thumbs',
        'thumbs'
    ];

    private $slash = '/';


    //Init Functions

    protected function SetRequest(array $request) {

        $this->request = $request;
    }

    protected function GetRequest() {

        return (object) $this->request;
    }

    protected function CurrentFolderType() {

        $request = $this->GetRequest();

        return $request->folderType;
    }

    protected function CurrentWorkDir() {

        $request = $this->GetRequest();

        return $request->workDirectory;
    }

    protected function GetPreviousPath($currentFolder = null, $currentWorkDir = null) {

        $currentFolder = (is_null($currentFolder)) ? $this->CurrentFolderType() : $currentFolder;
        $currentWorkDir = (is_null($currentWorkDir)) ? $this->CurrentWorkDir() : $currentWorkDir;

        if($currentFolder === 'root') {

            $previousFolder = null;
            $previousWorkDir = null;

        } else {

            if($currentWorkDir === 'root') {

                $previousFolder = null;
                $previousWorkDir = null;

            } else {

                $removeName = $this->RemoveName($currentWorkDir);

                $previousFolder = $currentFolder;
                $previousWorkDir = (is_null($removeName)) ? 'root' : $removeName;
            }
        }

        return [
            'folder_type' => $previousFolder,
            'work_directory' => $previousWorkDir
        ];
    }

    protected function GetCurrentPath($specificPath = null, $currentFolder = null, $currentWorkDir = null) {

        $currentFolder = (is_null($currentFolder)) ? $this->CurrentFolderType() : $currentFolder;
        $currentWorkDir = (is_null($currentWorkDir)) ? $this->CurrentWorkDir() : $currentWorkDir;

        $innerDirectory = ($currentWorkDir === 'root') ? null : $currentWorkDir;

        switch ($currentFolder) {

            case 'root':

                $folder = '';

                break;

            case 'share':

                $folder = 'share';

                break;

            case 'user':
            default:

                $folder = $this->GetUserHome();

                break;
        }

//        (substr($innerDirectory, 0, strlen($currentFolder)) == $currentFolder) ?
//            str_replace("{$currentFolder}{$this->slash}", '', $innerDirectory) :
//            $innerDirectory;

        return $this->RemoveSlash(
            $this->RemoveDuplicateSlash(
                (
                    $folder .
                    $this->slash .
                    $this->RemoveSlash($innerDirectory) .
                    $this->slash .
                    $this->RemoveSlash($specificPath)
                )
            )
        );
    }

    protected function GetUserHome() {

        $objConfigHandler = new ConfigHandler();
        $userDirectory = $objConfigHandler->UserField();

        return 'home' . $this->slash . $userDirectory;
    }

    protected function GetUserTemp() {

        $objConfigHandler = new ConfigHandler();
        $userDirectory = $objConfigHandler->UserField();

        return 'tmp' . $this->slash . $userDirectory;
    }

    protected function SortFilesAndDirectories(array $arrayItems, string $sortType) {

        if ($sortType == 'time') {

            $keyToSort = 'updated';

        } elseif ($sortType == 'alphabetic') {

            $keyToSort = 'name';

        } else {

            $keyToSort = 'updated';
        }

        if(count($arrayItems) > 0) {

            uasort($arrayItems, function ($a, $b) use ($keyToSort) {

                return strcmp($a->{$keyToSort}, $b->{$keyToSort});

            });
        }

        return $arrayItems;
    }

    protected function InitPermissions() {


    }

    protected function CheckPermission($folderType, $workDirectory, $permission) {

        if(!key_exists($folderType, $this->permissions))
            return false;

        $folderTypePermissions = $this->permissions[$folderType];

        if(!key_exists($workDirectory, $folderTypePermissions))
            return false;

        $permissions = $folderTypePermissions[$workDirectory];

        $permissionArray = explode(' ', $permissions);

        return in_array($permission, $permissionArray);
    }

    protected function FileNameIsValid($filename) {

        if (strpbrk($filename, "\\/?%*:|\"<>")) {

            return false;
        }

        return true;
    }



    //Storage

    private function Storage() {

        return Storage::disk(ao_config('file_manager.filesystem_disk', 'public'));
    }

    protected function CheckExist($path) {

        if($path == "" && $this->CurrentFolderType() == 'root')
            return true;

        return $this->Storage()->exists($this->RemoveSlash($path));
    }

    protected function IsFile($path) {

        if($this->CheckExist($path)) {

            return is_file($this->SystemPath($path));
        }

        return false;
    }

    protected function GetUrl($path) {

        if($this->CheckExist($path)) {

            return $this->Storage()->url($this->RemoveSlash($path));
        }

        return null;
    }

    protected function Move($oldPath, $newPath, $makeThumb = true) {

        if($this->CheckExist($oldPath) && !$this->CheckExist($newPath)) {

            try {

                if($this->IsFile($oldPath)) {

                    $extension = File::extension($oldPath);
                    $newName = basename($this->GetName($newPath), ".$extension") . ".$extension";
                    $newPath = $this->ReplaceName($newPath, $newName);
                    $thumb = $this->GetThumbPath($oldPath);

                    $move = File::move($this->SystemPath($oldPath), $this->SystemPath($newPath));

                    if(!is_null($thumb) && $move && $makeThumb) {

                        $newThumbFolder = $this->GetThumbPath($this->RemoveName($newPath));
                        $this->CreateDirectory($newThumbFolder);

                        $newThumb = $this->GetThumbPath($newPath, true);
                        File::move($this->SystemPath($thumb), $this->SystemPath($newThumb));
                    }

                    return $move;
                }

                return File::moveDirectory($this->SystemPath($oldPath), $this->SystemPath($newPath));

            } catch (\Exception $exception) {

                return false;
            }
        }

        return false;
    }

    protected function Copy($path, $copyPath, $makeThumb = true) {

        if($this->CheckExist($path) && !$this->CheckExist($copyPath)) {

            try {

                if($this->IsFile($path)) {

                    $extension = File::extension($path);
                    $newName = basename($this->GetName($copyPath), ".$extension") . ".$extension";
                    $copyPath = $this->ReplaceName($copyPath, $newName);
                    $thumb = $this->GetThumbPath($path);

                    $copy = File::copy($this->SystemPath($path), $this->SystemPath($copyPath));


                    if(!is_null($thumb) && $copy && $makeThumb) {

                        $newThumbFolder = $this->GetThumbPath($this->RemoveName($copyPath));
                        $this->CreateDirectory($newThumbFolder);

                        $newThumb = $this->GetThumbPath($copyPath, true);
                        File::copy($this->SystemPath($thumb), $this->SystemPath($newThumb));
                    }

                    return $copy;
                }

                return File::copyDirectory($this->SystemPath($path), $this->SystemPath($copyPath));

            } catch (\Exception $exception) {

                return false;
            }
        }

        return false;
    }

    private function FileType($file) {

        if($this->IsFile($file)) {

            return File::mimeType($this->SystemPath($file));
        }

        return null;
    }

    private function FileSize($file) {

        if($this->IsFile($file)) {

            return filesize($this->SystemPath($file));
        }

        return 0;
    }

    private function DirectorySize($directory) {

        $size = 0;
        $directory = $this->SystemPath($directory);

        foreach (glob(rtrim($directory, '/').'/*', GLOB_NOSORT) as $each) {

            $size += is_file($each) ? $this->FileSize($this->StoragePath($each)) : $this->DirectorySize($each);
        }

        return $size;
    }

    private function GetThumbPath($path, $force = false) {

        $path = $this->RemoveSlash($path);

        $arrayPath = explode($this->slash, $path);

        $file = end($arrayPath);

        unset($arrayPath[(count($arrayPath) - 1)]);

        $thumbsPath = implode($this->slash, $arrayPath) . $this->slash . 'Thumbs' . $this->slash . $file;

        if($force) {

            return $thumbsPath;
        }

        if($this->CheckExist($path)) {

            if($this->IsFile($path)) {

                if($this->FileIsImage($path) && $this->CheckExist($thumbsPath)) {

                    return $thumbsPath;
                }

                return null;
            }

            return $path . $this->slash . 'Thumbs';
        }

        return null;
    }

    private function GetName($path) {

        $arrayPath = explode($this->slash, $path);
        $name = end($arrayPath);

        return $name;
    }

    private function RemoveName($path) {

        $arrayPath = explode($this->slash, $path);
        unset($arrayPath[(count($arrayPath) - 1)]);

        if(count($arrayPath) <= 0)
            return null;

        return implode($this->slash, $arrayPath);
    }

    private function ReplaceName($path, $newName) {

        $arrayPath = explode($this->slash, $path);
        unset($arrayPath[(count($arrayPath) - 1)]);

        array_push($arrayPath, $newName);

        return implode($this->slash, $arrayPath);
    }

    private function DetectForGUI($path) {

        $systemFolderTypes = [
            'user' => $this->GetUserHome(),
            'share' => 'share'
        ];

        $folderType = 'root';

        if(strlen($path) > 0) {

            $arrayPath = explode($this->slash, $path);

            if($arrayPath[0] == 'home') {

                $arrayPath[1] = $arrayPath[0] . $this->slash . $arrayPath[1];
                unset($arrayPath[0]);
                $arrayPath = array_values($arrayPath);
            }

            $orgCount = count($arrayPath);

            if(in_array($arrayPath[0], $systemFolderTypes) && count($arrayPath) > 1) {

                $folderType = array_search($arrayPath[0], $systemFolderTypes);

                unset($arrayPath[0]);
            }

            if(count($arrayPath) > 0) {

                unset($arrayPath[($orgCount - 1)]);
            }

            $path = (count($arrayPath) > 0) ? implode($this->slash, $arrayPath) : 'root';
        }

        return [
            'folder_type' => $folderType,
            'path' => $path
        ];
    }

    //Storage File Functions

    protected function GetDirectoryFiles(string $directory = null, bool $getInfo = false, $type = null) {

        $directory = $this->RemoveSlash($directory);
        $collectionType = 'files';

        if($getInfo)
            $collectionType = 'file';

        $return = [
            'collection_type' => $collectionType,
            'name' => $this->GetName($directory),
            'path' => $directory,
            $collectionType => ($collectionType === 'files') ? collect([]) : null
        ];

        if($this->CheckExist($directory)) {

            if($getInfo) {

                $return[$collectionType] = $this->FinalGenerator($directory);

            } else {

                $files = $this->Storage()->files($directory);

                $return[$collectionType] = collect(array_map(function ($filePath) {

                    return $this->FinalGenerator($filePath);

                }, array_filter($files, function ($filePath) use ($type) {

                    $typeAllow = true;

                    if(!is_null($type)) {

                        $fileType = $this->GetFileType($filePath);
                        $typeAllow = starts_with($fileType, $type);
                    }

                    $fileAllow = !in_array($this->GetName($filePath), $this->systemPath);

                    return ($typeAllow && $fileAllow);

                })));
            }
        }

        return collect($return);
    }

    protected function GetFile(string $file) {

        if($this->CheckExist($file)) {

            return $this->Storage()->get($file);
        }

        return null;
    }

    protected function GetFileType($file) {

        return $this->FileType($file);
    }

    protected function GetFileSize($file) {

        return $this->ResolveSize($this->FileSize($file));
    }

    protected function GetFileNameInfo($file, bool $checkExist = true) {

        $extension = null;
        $baseName = null;

        if((($checkExist) ? $this->IsFile($file) : true)) {

            if($this->CheckExist($file)) {

                $extension = File::extension($this->SystemPath($file));

            } else {

                $arrayFileName = explode('.', $file);
                $extension = end($arrayFileName);
            }

            $baseName = basename($this->GetName($file), (($extension) ? ".$extension" : null));
        }

        return [
            'extension' => $extension,
            'base_name' => $baseName,
            'file' => $file
        ];
    }

    protected function CreateFile(string $file, $content) {

        if(!$this->CheckExist($file)) {

            return $this->Storage()->put($file, $content);
        }

        return null;
    }

    protected function UploadFile(string $path, UploadedFile $file, string $name){

        if(!$this->CheckExist($file)) {

            return $this->Storage()->putFileAs($path, $file, $name);
        }

        return null;
    }

    protected function DeleteFile(string $file) {

        if($this->CheckExist($file)) {

            try {

                $delete = $this->Storage()->delete($file);

            } catch (\Exception $exception) {

                $delete = false;
            }

            return $delete;
        }

        return false;
    }

    protected function FileIsImage($file) {

        $mimeType = $this->GetFileType($file);

        return starts_with($mimeType, 'image');
    }

    protected function CreateThumbImage($imagePath) {

        $createThumbnails = ao_config('file_manager.should_create_thumbnails', true);

        if ($createThumbnails && $this->FileIsImage($imagePath)) {

            $this->CreateDirectory($this->GetThumbPath(($this->RemoveName($imagePath))));

            $thumbImgWidth = ao_config('file_manager.thumb_img_width', 200);
            $thumbImgHeight = ao_config('file_manager.thumb_img_height', 200);

            $thumbPath = $this->GetThumbPath($imagePath, true);

            if($this->CheckExist($thumbPath)) {

                $this->DeleteFile($thumbPath);
            }

            Image::make($this->SystemPath($imagePath))
                ->fit($thumbImgWidth, $thumbImgHeight)
                ->save($this->SystemPath($thumbPath));
        }
    }

    protected function ImageConvertToJpeg(&$imagePath, int $convertQuality) {

        if($this->FileIsImage($imagePath)) {

            $imageInfo = $this->ImageInfo($imagePath);

            $imageExtension = strtolower($imageInfo['extension']);

            $ignoreExtensions = [
                'jpg',
                'jpeg'
            ];

            if(!in_array($imageExtension, $ignoreExtensions)) {

                $systemPath = $this->SystemPath($imagePath);
                $systemRoot = str_replace($imagePath, '', $systemPath);

                $jpgImage =imagecreatefromstring(file_get_contents($systemPath));

                $newBaseName = $imageInfo['filename'] . ".jpg";
                $jpgPath = str_replace($imageInfo['basename'], $newBaseName, $systemPath);

                if(file_exists($jpgPath)) {

                    $newBaseName = $imageInfo['filename'] . time() . ".jpg";
                    $jpgPath = str_replace($imageInfo['basename'], $newBaseName, $systemPath);
                }

                imagejpeg($jpgImage, $jpgPath, $convertQuality);

                unlink($systemPath);

                $imagePath = str_replace($systemRoot, '', $jpgPath);
            }
        }
    }

    protected function ImageOptimizer($imagePath, int $convertQuality) {

        if($this->FileIsImage($imagePath)) {

            $systemPath = $this->SystemPath($imagePath);

            $optimizer = new Imagick($systemPath);

            $optimizer->setImageFormat('jpg');

            $optimizer->optimizeImageLayers();

            $optimizer->setImageCompression(Imagick::COMPRESSION_JPEG);
            $optimizer->setImageCompressionQuality($convertQuality);

            $optimizer->writeImages($systemPath, true);
        }
    }

    protected function ImageCrop(string $imagePath, array $details, $overWrite = false) {

        if($this->CheckExist($imagePath)) {

            $cropPath = $imagePath;

            if($overWrite) {

                $imageNameInfo = $this->GetFileNameInfo($imagePath);
                $newName = $imageNameInfo['base_name'] . '_cropped_' . time();
                $cropPath = $this->ReplaceName($cropPath, $newName);
            }

            Image::make($this->SystemPath($imagePath))
                ->crop($details['width'], $details['height'], $details['x'], $details['y'])
                ->save($this->SystemPath($cropPath));

            $this->CreateThumbImage($cropPath);

            return true;
        }

        return false;
    }

    protected function ImageResize(string $imagePath, array $details) {

        if($this->CheckExist($imagePath)) {

            Image::make($this->SystemPath($imagePath))
                ->resize($details['width'], $details['height'])
                ->save();

            return true;
        }

        return false;
    }

    protected function ImageInfo(string $imagePath) {

        if($this->CheckExist($imagePath)) {

            $originalImage = Image::make($this->SystemPath($imagePath));

            return [
                'width' => $originalImage->width(),
                'height' => $originalImage->height(),
                'extension' => $originalImage->extension,
                'mime' => $originalImage->mime,
                'dir' => $this->StoragePath($originalImage->dirname),
                'basename' => $originalImage->basename,
                'filename' => $originalImage->filename
            ];
        }

        return null;
    }

    protected function ImageMakeTemp(string $imagePath, $sufix = null) {

        if($this->CheckExist($imagePath)) {

            $userTemp = $this->GetUserTemp();

            $imageInfo = $this->ImageInfo($imagePath);

            $tempName = 'tmpimg' . (is_null($sufix) ? '' : "-$sufix") . '.' . $imageInfo['extension'];
            $tempPath = $userTemp . $this->slash . $tempName;

            $this->DeleteFile($tempPath);

            $this->Copy($imagePath, $tempPath, false);

            return $tempPath;
        }

        return null;
    }

    protected function GetCurrentFiles($specificFile = null, $currentFolder = null, $currentWorkDir = null, $type = null) {

        $path = $this->GetCurrentPath($specificFile, $currentFolder, $currentWorkDir);
        $getInfo = (is_null($specificFile)) ? false : true;

        return $this->GetDirectoryFiles($path, $getInfo, $type);
    }



    //Storage Directory Functions

    protected function GetDirectory(string $directory = null, bool $getInfo = false) {

        $directory = $this->RemoveSlash($directory);
        $collectionType = 'directories';

        if($getInfo)
            $collectionType = 'directory';

        $return = [
            'collectiontype' => $collectionType,
            'name' => $this->GetName($directory),
            'path' => $directory,
            $collectionType => ($collectionType === 'directories') ? collect([]) : null
        ];

        if($this->CheckExist($directory)) {

            if($getInfo) {

                $return[$collectionType] = $this->FinalGenerator($directory);

            } else {

                $directories = $this->Storage()->directories($directory);

                $return[$collectionType] = collect(array_map(function ($directoryPath) {

                    return $this->FinalGenerator($directoryPath);

                }, array_filter($directories, function ($directoryPath) {

                    return in_array($this->GetName($directoryPath), $this->systemPath) === false;
                })));
            }
        }

        return collect($return);
    }

    protected function GetDirectorySize ($directory) {

        return $this->ResolveSize($this->DirectorySize($directory));
    }

    protected function CreateDirectory(string $directory) {

        if(!$this->CheckExist($directory)) {

            return $this->Storage()->makeDirectory($directory);
        }

        return false;
    }

    protected function CreateDirectoryFree($directory) {

        if (! File::exists($directory)) {

            File::makeDirectory($directory, ao_config('file_manager.create_folder_mode', 0755), true, true);
        }
    }

    protected function DeleteDirectory(string $directory) {

        if($this->CheckExist($directory)) {

            try {

                $delete = $this->Storage()->deleteDirectory($directory);

            } catch (\Exception $exception) {

                $delete = false;
            }

            return $delete;
        }

        return false;
    }

    protected function GetCurrentDirectories($specificFile = null, $currentFolder = null, $currentWorkDir = null) {

        $path = $this->GetCurrentPath($specificFile, $currentFolder, $currentWorkDir);
        $getInfo = (is_null($specificFile)) ? false : true;

        return $this->GetDirectory($path, $getInfo);
    }

    protected function InitUserHome() {

        $userHome = $this->GetUserHome();

        $userFolders = array_map(function ($folder) use (&$userHome) {

            return $userHome . $this->slash . $folder;

        }, ao_config('file_manager.folders', []));


        foreach ($userFolders as $userFolder) {

            $this->CreateDirectory($userFolder);
        }
    }

    protected function InitUserTemp() {

        $userTemp = $this->GetUserTemp();

        $this->CreateDirectory($userTemp);
    }

    protected function InitDirectories() {

        $storageDirectory = ao_config('file_manager.storage_directory', '');

        $this->CreateDirectoryFree(storage_path($storageDirectory));

        $fileManagerDirectories = [
            'home',
            'share',
            'tmp'
        ];

        foreach ($fileManagerDirectories as $fileManagerDirectory) {

            $this->CreateDirectory($fileManagerDirectory);
        }

        $this->InitUserHome();
        $this->InitUserTemp();
    }

    //Other

    private function FinalGenerator(string $itemPath) {

        $objPersianCalender = new PersianCalender();

        $isFile = $this->IsFile($itemPath);
        $isImage = $this->FileIsImage($itemPath);
        $systemPath = $this->SystemPath($itemPath);
        $GUIPath = $this->DetectForGUI($itemPath);
        $pathType = ($isFile) ? $this->GetFileType($itemPath) : 'directory/directory';
        $icon = $this->GetIcon($pathType);

        return [
            'name' => $this->GetName($itemPath),
            'url'     => $this->GetUrl($itemPath),
            'path' => $itemPath,
            'system_path' => $systemPath,
            'size' => ($isFile) ? $this->GetFileSize($itemPath) : $this->GetDirectorySize($itemPath),
            'updated' => filemtime($systemPath),
            'modify' => date('Y-m-d - H:i', filemtime($systemPath)),
            'persian_modify'    => $objPersianCalender->jdate('Y/m/d - H:i', filemtime($systemPath), '', 'Asia/Tehran', 'en'),
            'thumb' => ($isImage) ? $this->GetUrl($this->GetThumbPath($itemPath)) : null,
            'type'    => $this->GetStringFileType($pathType),
            'folder_type' => $GUIPath['folder_type'],
            'work_directory' => $GUIPath['path'],
            'icon' => $icon['icon'],
            'icon_type' => $icon['type'],
            'item_type' => ($isFile) ? (($isImage) ? 'image' : 'file') : 'folder',
            'extension' => ($isFile) ? $this->GetFileNameInfo($itemPath)['extension'] : null
        ];
    }

    protected function ResolveSize($bytes, $decimals = 2) {

        $size = ['B', 'kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];

        $factor = floor((strlen($bytes) - 1) / 3);

        return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . ' ' . @$size[$factor];
    }

    protected function GetStringFileType($type) {

        $arrayType = explode('/', $type);

        if($arrayType[0] == 'application') {

            $type = $arrayType[1];

        } else {

            $type = $arrayType[0];
        }

        switch ($type) {

            case 'directory':

                $stringType = 'Folder';

                break;

            case 'image':

                $stringType = ucfirst($arrayType[1]) . ' Image';

                break;

            case 'audio':

                $stringType = ucfirst($arrayType[1]) . ' Audio';

                break;

            case 'video':

                $stringType = ucfirst($arrayType[1]) . ' Video';

                break;

            case 'pdf':

                $stringType = 'Pdf';

                break;

            case 'msword':

                $stringType = 'Office Word';

                break;

            case 'vnd.ms-excel':

                $stringType = 'Office Excel';

                break;

            case 'vnd.openxmlformats-officedocument.presentationml.presentation':
            case 'vnd.ms-powerpoint':

            $stringType = 'Office PowerPoint';

                break;

            case 'zip':

                $stringType = 'Zip';

                break;

            case 'rar':

                $stringType = 'RAR';

                break;

            case 'text':

                $stringType = 'Text';

                break;

            default:

                $stringType = 'UnKnow';

                break;
        }

        return $stringType;
    }

    protected function GetIcon($type) {

        $arrayType = explode('/', $type);

        if($arrayType[0] == 'application') {

            $type = $arrayType[1];

        } else {

            $type = $arrayType[0];
        }

        switch ($type) {

            case 'directory':

                $icon = [
                    'icon' => 'folder',
                    'type' => 'material'
                ];

                break;

            case 'image':

                $icon = [
                    'icon' => 'fa fa-file-photo-o',
                    'type' => 'fontawesome'
                ];

                break;

            case 'audio':

                $icon = [
                    'icon' => 'fa fa-file-audio-o',
                    'type' => 'fontawesome'
                ];

                break;

            case 'video':

                $icon = [
                    'icon' => 'fa fa-file-video-o',
                    'type' => 'fontawesome'
                ];

                break;

            case 'pdf':

                $icon = [
                    'icon' => 'fa fa-file-pdf-o',
                    'type' => 'fontawesome'
                ];

                break;

            case 'msword':

                $icon = [
                    'icon' => 'fa fa-file-word-o',
                    'type' => 'fontawesome'
                ];

                break;

            case 'vnd.ms-excel':

                $icon = [
                    'icon' => 'fa fa-file-excel-o',
                    'type' => 'fontawesome'
                ];

                break;

            case 'vnd.openxmlformats-officedocument.presentationml.presentation':
            case 'vnd.ms-powerpoint':

                $icon = [
                    'icon' => 'fa fa-file-powerpoint-o',
                    'type' => 'fontawesome'
                ];

                break;

            case 'zip':
            case 'rar':

                $icon = [
                    'icon' => 'fa fa-file-archive-o',
                    'type' => 'fontawesome'
                ];

                break;

            default:

                $icon = [
                    'icon' => 'fa fa-file-text-o',
                    'type' => 'fontawesome'
                ];

                break;
        }

        return $icon;
    }

    protected function SystemPath(string $path) {

        $fileSystemDisk = ao_config('file_manager.filesystem_disk', '');
        $diskRoot = config("filesystems.disks.$fileSystemDisk.root");

        return $this->slash . $this->RemoveSlash($diskRoot) . $this->slash . $this->RemoveSlash($path);
    }

    protected function StoragePath(string $path) {

        $fileSystemDisk = ao_config('file_manager.filesystem_disk', '');
        $diskRoot = config("filesystems.disks.$fileSystemDisk.root");

        return str_replace($this->RemoveSlash($diskRoot), '', $this->RemoveSlash($path));
    }

    protected function RemoveSlash($path) {

        return (is_string($path)) ? trim($path, '/') : null;
    }

    protected function RemoveDuplicateSlash($path) {

        return preg_replace('/\\'.$this->slash.'{2,}/', $this->slash, $path);
    }
}