<?php

namespace Mota\AdminOfficer\Traits;


trait ClassHelper {

    private $classStatus = false;

    private $classMessage;

    private $classData = [];

    protected function SetStatus(bool $status) {

        $this->classStatus = (boolean) $status;
        return $this;
    }

    protected function SetMessage(string $text) {

        $this->classMessage = $text;
        return $this;
    }

    protected function SetData(array $data) {

        foreach ($data as $dataKey => $dataValue) {

            $this->classData[$dataKey] = $dataValue;
        }
        return $this;
    }

    public function GetStatus() {

        return $this->classStatus;
    }

    public function GetMessage() {

        return $this->classMessage;
    }

    public function GetData() {

        return (object) $this->classData;
    }

    protected function GenerateReturn() {
        return new static();
    }
}