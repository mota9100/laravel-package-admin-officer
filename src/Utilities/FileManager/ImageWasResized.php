<?php

namespace Mota\AdminOfficer\Utilities\FileManager;

class ImageWasResized {

    private $path;

    public function __construct($path) {

        $this->path = $path;
    }

    public function Path() {

        return $this->path;
    }
}
