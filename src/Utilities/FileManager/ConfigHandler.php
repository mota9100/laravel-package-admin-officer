<?php

namespace Mota\AdminOfficer\Utilities\FileManager;

use Illuminate\Support\Facades\Auth;

class ConfigHandler {
    
    public function UserField() {

        $guard = config('auth.defaults.admin_officer');

        $admin = Auth::guard($guard)->user();

        return 'user' . $admin->AccountID;
    }
}