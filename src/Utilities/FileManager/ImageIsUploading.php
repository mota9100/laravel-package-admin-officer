<?php

namespace Mota\AdminOfficer\Utilities\FileManager;

class ImageIsUploading {
    
    private $path;

    public function __construct($path) {
        $this->path = $path;
    }

    
    public function Path() {
        
        return $this->path;
    }
}
