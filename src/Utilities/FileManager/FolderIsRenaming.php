<?php

namespace Mota\AdminOfficer\Utilities\FileManager;

class FolderIsRenaming {
    
    private $oldPath;
    private $newPath;

    public function __construct($oldPath, $newPath) {
        
        $this->oldPath = $oldPath;
        $this->newPath = $newPath;
    }
    
    public function OldPath() {
        
        return $this->oldPath;
    }

    public function NewPath()
    {
        return $this->newPath;
    }
}
