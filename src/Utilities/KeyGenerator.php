<?php

namespace Mota\AdminOfficer\Utilities;


class KeyGenerator
{
    private $lowercase;
    private $uppercase;
    private $number;
    private $specialChar;

    function __construct($uppercase = true, $lowercase = true, $number = true, $specialChar = false) {
        $this->lowercase = $lowercase;
        $this->uppercase = $uppercase;
        $this->number = $number;
        $this->specialChar = $specialChar;
    }

    function Generate($length=8, $prefix='hon') {

        $chars = [];
        if ($this->uppercase) {
            $chars[] = "A";
            $chars[] = "B";
            $chars[] = "C";
            $chars[] = "D";
            $chars[] = "E";
            $chars[] = "F";
            $chars[] = "G";
            $chars[] = "h";
            $chars[] = "I";
            $chars[] = "J";
            $chars[] = "K";
            $chars[] = "L";
            $chars[] = "M";
            $chars[] = "N";
            $chars[] = "O";
            $chars[] = "P";
            $chars[] = "Q";
            $chars[] = "R";
            $chars[] = "S";
            $chars[] = "T";
            $chars[] = "U";
            $chars[] = "V";
            $chars[] = "W";
            $chars[] = "X";
            $chars[] = "Y";
            $chars[] = "Z";
        }
        if ($this->lowercase) {
            $chars[] = "a";
            $chars[] = "b";
            $chars[] = "c";
            $chars[] = "d";
            $chars[] = "e";
            $chars[] = "f";
            $chars[] = "g";
            $chars[] = "h";
            $chars[] = "i";
            $chars[] = "j";
            $chars[] = "k";
            $chars[] = "l";
            $chars[] = "m";
            $chars[] = "n";
            $chars[] = "o";
            $chars[] = "p";
            $chars[] = "q";
            $chars[] = "r";
            $chars[] = "s";
            $chars[] = "t";
            $chars[] = "u";
            $chars[] = "v";
            $chars[] = "w";
            $chars[] = "x";
            $chars[] = "y";
            $chars[] = "z";
        }
        if ($this->number) {
            $chars[] = "1";
            $chars[] = "2";
            $chars[] = "3";
            $chars[] = "4";
            $chars[] = "5";
            $chars[] = "6";
            $chars[] = "7";
            $chars[] = "8";
            $chars[] = "9";
            $chars[] = "0";
        }
        if ($this->specialChar) {
            $chars[] = '!';
            $chars[] = '@';
            $chars[] = '#';
            $chars[] = "$";
            $chars[] = '%';
            $chars[] = '^';
            $chars[] = '&';
            $chars[] = '*';
            $chars[] = '(';
            $chars[] = ')';
            $chars[] = '{';
            $chars[] = '}';
            $chars[] = '[';
            $chars[] = ']';
            $chars[] = '<';
            $chars[] = '>';
            $chars[] = '?';
            $chars[] = "=";
            $chars[] = '+';
            $chars[] = '-';
            $chars[] = "_";
        }

        $maxElements = count($chars) - 1;
        $newPassword = $prefix;
        $finalLength = $length - strlen($prefix);
        for ($i = 0;$i<$finalLength;$i++)
        {
            srand((double)microtime()*1000000);
            $newPassword .= $chars[rand(0,$maxElements)];
        }
        return  $newPassword;
    }
}