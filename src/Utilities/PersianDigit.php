<?php

namespace Mota\AdminOfficer\Utilities;

class PersianDigit
{

    public function ConvertToPersian($inputString)
    {

        $inputString = str_replace("0", "۰", $inputString);
        $inputString = str_replace("1", "۱", $inputString);
        $inputString = str_replace("2", "۲", $inputString);
        $inputString = str_replace("3", "۳", $inputString);
        $inputString = str_replace("4", "۴", $inputString);
        $inputString = str_replace("5", "۵", $inputString);
        $inputString = str_replace("6", "۶", $inputString);
        $inputString = str_replace("7", "۷", $inputString);
        $inputString = str_replace("8", "۸", $inputString);
        $inputString = str_replace("9", "۹", $inputString);

        return $inputString;
    }

    function ReversePersianDigit($inputString) {

        $inputString = str_replace("۰", "0", $inputString);
        $inputString = str_replace("۱", "1", $inputString);
        $inputString = str_replace("۲", "2", $inputString);
        $inputString = str_replace("۳", "3", $inputString);
        $inputString = str_replace("۴", "4", $inputString);
        $inputString = str_replace("۵", "5", $inputString);
        $inputString = str_replace("۶", "6", $inputString);
        $inputString = str_replace("۷", "7", $inputString);
        $inputString = str_replace("۸", "8", $inputString);
        $inputString = str_replace("۹", "9", $inputString);

        return $inputString;
    }
}