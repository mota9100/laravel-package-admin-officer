<?php


namespace Mota\AdminOfficer\Console;


class CommandUpdate extends AdminOfficerCommand {

    protected $name = 'admin-officer:update';

    protected $description = 'update admin officer package';

    /**
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function handle() {

        $this->call('admin-officer:install');

        $this->vendorPublishForce();
    }

    public function vendorPublishForce() {

        $this->call('vendor:publish', [
            '--provider' => AdminOfficerServiceProvider::class,
            '--force' => true
        ]);
    }
}