<?php

namespace Mota\AdminOfficer\Console;

use Illuminate\Console\Command;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Filesystem\Filesystem;

class AdminOfficerCommand extends Command {

    protected $files;

    /**
     * AdminOfficerCommand constructor.
     * @param Filesystem $files
     */
    public function __construct(Filesystem $files) {

        parent::__construct();

        $this->files = $files;
    }

    /**
     * @param $name
     * @return string
     * @throws FileNotFoundException
     */
    protected function GetStub($name) {

        try {

            return $this->files->get(__DIR__ . "/Stubs/$name.stub");

        } catch (FileNotFoundException $e) {

            throw new FileNotFoundException();
        }
    }

    protected function MakeDir($directory, $path = '') {

        $this->files->makeDirectory("{$directory}/$path", 0755, true, true);
    }

    protected function AlreadyExists($file) {

        return $this->files->exists($file);
    }
}