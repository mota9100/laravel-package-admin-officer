<?php

namespace Mota\AdminOfficer\Console;


use Mota\AdminOfficer\Tables\TableTheme;
use Symfony\Component\Console\Input\InputArgument;

class CommandMakeView extends AdminOfficerCommand {

    protected $name = 'admin-officer:make:view';

    protected $description = 'make admin officer view';

    /**
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function handle() {

        $this->MakeAdminView();
    }

    /**
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    protected function MakeAdminView() {

        $objTableTheme = new TableTheme();
        $viewDirectory = resource_path('views/admin');

        if (!is_dir($viewDirectory)) {

            $this->line("<error>View directory not exists please run admin-officer:install!</error> ");

            return;
        }

        $view = $this->GetViewName();
        $themes = $objTableTheme->SelectGet();

        if(count($themes) <= 0) {

            $this->line("<error>can not find theme!</error> ");

            return;
        }

        foreach ($themes as $theme) {

            $themeDirectory = resource_path("views/admin/{$theme->ThemeName}");

            if (!is_dir($themeDirectory)) {

                $this->MakeDir(resource_path("views/admin"), $theme->ThemeName);
            }

            $file = resource_path("views/admin/{$theme->ThemeName}/{$view}.blade.php");

            if($this->AlreadyExists($file)) {

                $this->line("<error>Admin Officer View {$view} already exists!</error> ");

                continue;
            }

            $stub = $this->getStub('view');

            $this->PutFile($file, $stub, $theme->ThemeName, $view);
        }

        $this->line('<info>Admin Officer View was created:</info> '.str_replace(base_path(), '', $file));
    }

    protected function Replacements($stub, $theme, $view) {

        $stub = str_replace(
            ['DummyTheme', 'DummyViewName'],
            [$theme, $view],
            $stub
        );

        return $stub;
    }

    protected function PutFile($file, $stub, $theme, $view) {

        $stub = $this->Replacements($stub, $theme, $view);

        $this->files->put($file, $stub);
    }

    protected function GetViewName() {

        return trim($this->argument('AdminViewName'));
    }

    protected function getArguments() {

        return [
            ['AdminViewName', InputArgument::REQUIRED, 'The Name Of The Admin Officer View'],
        ];
    }
}