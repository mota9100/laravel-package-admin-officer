<?php

namespace Mota\AdminOfficer\Console;

use Symfony\Component\Console\Input\InputArgument;

class CommandMakeController extends AdminOfficerCommand {

    protected $name = 'admin-officer:make:controller';

    protected $description = 'make admin officer controller';

    /**
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function handle() {

        $this->MakeAdminController();
    }

    /**
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    protected function MakeAdminController() {

        $controllerDirectory = app_path('Http/Controllers/AdminOfficer');

        if (!is_dir($controllerDirectory)) {

            $this->line("<error>Controller directory not exists please run admin-officer:install!</error> ");

            return;
        }

        $controller = $this->GetControllerName();

        $file = app_path("Http/Controllers/AdminOfficer/{$controller}.php");

        if($this->AlreadyExists($file)) {

            $this->line("<error>Admin Officer Controller {$controller} already exists!</error> ");

            return;
        }

        $contents = $this->getStub('controller');

        $this->PutFile($file, $contents, $controller);

        $this->line('<info>Admin Officer Controller Class was created:</info> '.str_replace(base_path(), '', $file));
    }

    protected function GetNamespace() {

        return ao_config('route.namespace');
    }

    protected function Replacements($stub, $controllerName) {

        $stub = str_replace(
            ['DummyNamespace', 'DummyClassName'],
            [$this->GetNamespace(), $controllerName],
            $stub
        );

        return $stub;
    }

    protected function PutFile($file, $stub, $className = '') {

        $stub = $this->Replacements($stub, $className);

        $this->files->put($file, $stub);
    }

    protected function GetControllerName() {

        return trim($this->argument('AdminControllerName'));
    }

    protected function getArguments()
    {
        return [
            ['AdminControllerName', InputArgument::REQUIRED, 'The Name Of The Admin Officer Controller'],
        ];
    }
}