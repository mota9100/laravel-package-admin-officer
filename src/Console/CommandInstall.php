<?php

namespace Mota\AdminOfficer\Console;

use Mota\AdminOfficer\Providers\AdminOfficerServiceProvider;
use Spatie\LaravelImageOptimizer\ImageOptimizerServiceProvider;
use Mota\AdminOfficer\Tables\TableAccount;
use Mota\AdminOfficer\Seeds\SeedAdminOfficer;
use Mota\AdminOfficer\Tables\TableTheme;

class CommandInstall extends AdminOfficerCommand {

    protected $name = 'admin-officer:install';

    protected $description = 'initialize admin officer package';

    /**
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function handle() {

        $this->initDatabase();

        $this->vendorPublish();

        $this->initAdminDirectory();

        $this->initFileManager();
    }

    public function initDatabase() {

        $objTableAccount = new TableAccount();

        $this->call('migrate', ['--path' =>  str_replace(base_path() . '/', '', __DIR__ . '/../Migrations')]);

        if ($objTableAccount->GetCount() == 0) {

            $this->call('db:seed', ['--class' => SeedAdminOfficer::class]);
        }
    }

    public function vendorPublish() {

        $this->call('vendor:publish', ['--provider' => AdminOfficerServiceProvider::class]);
        $this->call('vendor:publish', ['--provider' => ImageOptimizerServiceProvider::class]);
    }

    public function initFileManager() {

        $storageDirectory = ao_config('file_manager.storage_directory', 'AOFileManager');
        $publicDirectory = ao_config('file_manager.public_directory', 'officerstorage');

        $storgePath = storage_path($storageDirectory);
        $publicPath = public_path($publicDirectory);

        $command = "ln -s $storgePath $publicPath";
        @exec($command);

        $this->line("<info>File Manager symlink created!</info> ");
    }

    /**
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     * @return void
     */
    protected function initAdminDirectory() {

        $this->initControllerDirectory();

        $this->initViewDirectory();

        $this->createRoutesFile();

        $this->createIncludeJSFile();

        $this->createIncludeCSSFile();

        $this->createIncludeMenuFile();

        $this->call('admin-officer:make:view', ['AdminViewName' => 'example']);
    }

    /**
     *
     */
    protected function initControllerDirectory() {

        $controllerDirectory = app_path('Http/Controllers/AdminOfficer');

        if (is_dir($controllerDirectory)) {

            $this->line("<info>Controller directory already exists!</info> ");

            return;
        }

        $this->MakeDir($controllerDirectory, '/');

        $this->call('admin-officer:make:controller', ['AdminControllerName' => 'ExampleController']);
    }

    /**
     *
     */
    protected function initViewDirectory() {

        $viewDirectory = resource_path('views/admin');

        if (is_dir($viewDirectory)) {

            $this->line("<info>View directory already exists!</info> ");

            return;
        }

        $this->MakeDir($viewDirectory, '/');
    }

    /**
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     * @return void
     */
    protected function createRoutesFile() {

        $file = base_path('routes/admin.php');

        $stub = $this->GetStub('routes');

        $this->PutFile($file, $stub);

        $this->line('<info>Routes file was created:</info> ');
    }

    /**
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     * @return void
     */
    protected function createIncludeCSSFile() {

        $objTableTheme = new TableTheme();

        $themes = $objTableTheme->SelectGet();

        if(count($themes) <= 0) {

            $this->line("<error>can not find theme!</error> ");

            return;
        }

        foreach ($themes as $theme) {

            $themeDirectory = resource_path("views/admin/{$theme->ThemeName}");

            if (!is_dir($themeDirectory)) {

                $this->MakeDir(resource_path("views/admin"), $theme->ThemeName);
            }

            $includeDirectory = resource_path("views/admin/{$theme->ThemeName}/includes");

            if (!is_dir($includeDirectory)) {

                $this->MakeDir(resource_path("views/admin/{$theme->ThemeName}"), "includes");
            }

            $file = resource_path("views/admin/{$theme->ThemeName}/includes/CSS.blade.php");

            if($this->AlreadyExists($file)) {

                $this->line("<info>Admin Officer {$theme->ThemeName} Include CSS already exists!</info> ");

                continue;
            }

            $stub = $this->GetStub('include-css');

            $this->PutFile($file, $stub);

        }

        $this->line('<info>Include CSS file was created.</info>');
    }

    /**
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     * @return void
     */
    protected function createIncludeJSFile() {

        $objTableTheme = new TableTheme();

        $themes = $objTableTheme->SelectGet();

        if(count($themes) <= 0) {

            $this->line("<error>can not find theme!</error> ");

            return;
        }

        foreach ($themes as $theme) {

            $themeDirectory = resource_path("views/admin/{$theme->ThemeName}");

            if (!is_dir($themeDirectory)) {

                $this->MakeDir(resource_path("views/admin"), $theme->ThemeName);
            }

            $includeDirectory = resource_path("views/admin/{$theme->ThemeName}/includes");

            if (!is_dir($includeDirectory)) {

                $this->MakeDir(resource_path("views/admin/{$theme->ThemeName}"), "includes");
            }

            $file = resource_path("views/admin/{$theme->ThemeName}/includes/JS.blade.php");

            if($this->AlreadyExists($file)) {

                $this->line("<info>Admin Officer {$theme->ThemeName} Include JS already exists!</info> ");

                continue;
            }

            $stub = $this->GetStub('include-js');

            $this->PutFile($file, $stub);

        }

        $this->line('<info>Include JS file was created.</info>');
    }

    /**
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     * @return void
     */
    protected function createIncludeMenuFile() {

        $objTableTheme = new TableTheme();

        $themes = $objTableTheme->SelectGet();

        if(count($themes) <= 0) {

            $this->line("<error>can not find theme!</error> ");

            return;
        }

        foreach ($themes as $theme) {

            $themeDirectory = resource_path("views/admin/{$theme->ThemeName}");

            if (!is_dir($themeDirectory)) {

                $this->MakeDir(resource_path("views/admin"), $theme->ThemeName);
            }

            $includeDirectory = resource_path("views/admin/{$theme->ThemeName}/includes");

            if (!is_dir($includeDirectory)) {

                $this->MakeDir(resource_path("views/admin/{$theme->ThemeName}"), "includes");
            }

            $file = resource_path("views/admin/{$theme->ThemeName}/includes/Menu.blade.php");

            if($this->AlreadyExists($file)) {

                $this->line("<info>Admin Officer {$theme->ThemeName} Include Menu already exists!</info> ");

                continue;
            }

            $stub = $this->GetStub('include-menu');

            $this->PutFile($file, $stub);

        }

        $this->line('<info>Include Menu file was created.</info>');
    }

    /**
     * @param $file
     * @param $stub
     */
    protected function PutFile($file, $stub) {

        $this->files->put($file, $stub);
    }

}