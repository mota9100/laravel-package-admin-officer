<?php

use Illuminate\Support\Facades\Route;

Route::post('/get', 'ControllerFileManager@Index')
    ->name('admin.officer.filemanager.get');

Route::post('/get/file', 'ControllerFileManager@GetFileByUrl')
    ->name('admin.officer.filemanager.get.file');

Route::post('/terminal', 'ControllerFileManager@Terminal')
    ->name('admin.officer.filemanager.terminal');

Route::post('/upload', 'ControllerUpload@Upload')
    ->name('admin.officer.filemanager.upload');

Route::post('/upload/validate', 'ControllerUpload@UploadValidate')
    ->name('admin.officer.filemanager.upload.validate');

Route::get('/download', 'ControllerDownload')
    ->name('admin.officer.filemanager.download');


