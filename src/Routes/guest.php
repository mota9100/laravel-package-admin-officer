<?php

use Illuminate\Support\Facades\Route;

Route::get('/login', 'ControllerGlobalViews@LoginView')
    ->name('admin.officer.login');

Route::post('/global/terminal', 'ControllerGlobal@Terminal')
    ->name('admin.officer.global.terminal');



Route::get('/password/reset','ControllerGlobalViews@RequestResetPasswordLinkView')
    ->name('admin.officer.password.request');

Route::post('/password/email','ControllerGlobalViews@sendResetLinkEmail')
    ->name('admin.officer.password.email');

Route::get('/password/reset/{token}','ControllerGlobalViews@ResetPasswordView')
    ->name('admin.officer.password.reset');

Route::post('/password/reset','ControllerGlobalViews@reset')
    ->name('admin.officer.password.do.reset');