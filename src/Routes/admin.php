<?php


use Illuminate\Support\Facades\Route;

Route::middleware('admin.officer.first.login')->group(function() {

    Route::post('/admin/terminal', 'ControllerAdmin@Terminal')
        ->name('admin.officer.admin.terminal');

    Route::get('/', 'ControllerAdminViews@Dashboard')
        ->name('admin.officer.dashboard');

    Route::get('/filemanager', 'ControllerAdminViews@FileManager')
        ->name('admin.officer.filemanager');

    Route::get('/profile', 'ControllerAdminViews@Profile')
        ->name('admin.officer.profile');
});

Route::post('/admin/register/terminal', 'ControllerAdmin@Terminal')
    ->name('admin.officer.admin.terminal');

Route::get('/register/{email?}', 'ControllerAdminViews@Register')
    ->name('admin.officer.register');

Route::get('/register/activation/{email}/{token}', 'ControllerAdminViews@RegisterActivation')->
    name('admin.officer.account.activate');