<?php

use Illuminate\Support\Facades\Route;

Route::get('/captcha', 'ControllerGlobalViews@Captcha')
    ->name('admin.officer.captcha');