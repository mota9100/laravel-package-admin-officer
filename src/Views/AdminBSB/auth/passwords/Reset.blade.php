@extends(ao_view('AdminBSB', 'auth.Master'))

@section('title', ao_trans('adminofficer.title.password_reset.head_title'))

@section('content')

    <div class="row m-r-0 m-l-0">

        <form role="form" method="POST" action="{{ route('admin.officer.password.request') }}">

            <div class="input-group">
                <span class="input-group-addon">
                    <i class="material-icons">person</i>
                </span>
                <div class="form-line">
                    <input type="text" class="form-control detect-direction" name="email"
                           placeholder="{{ ao_trans('adminofficer.input.text.email') }}" value="{{ old('email') }}" required autofocus>
                </div>
            </div>

            <div class="input-group">
                <span class="input-group-addon">
                    <i class="material-icons">vpn_key</i>
                </span>
                <div class="form-line">
                    <input type="password" class="form-control detect-direction" name="password"
                           placeholder="{{ ao_trans('adminofficer.input.text.password_new') }}" required>
                </div>
            </div>

            <div class="input-group">
                <span class="input-group-addon">
                    <i class="material-icons">vpn_key</i>
                </span>
                <div class="form-line">
                    <input type="password" class="form-control detect-direction" name="password_confirmation"
                           placeholder="{{ ao_trans('adminofficer.input.text.password_confirm') }}" required>
                </div>
            </div>

            <div class="col-xs-12 p-t-5">
                {{ csrf_field() }}
                <input type="hidden" name="token" value="{{ $token }}">
                <button type="submit" class="btn btn-block bg-teal waves-effect form-submit-button">
                    {{ ao_trans('adminofficer.button.update.reset_password') }}
                </button>
            </div>

        </form>

    </div>

@endsection

@section('JS')

    <script type="text/javascript">

        $(function () {

            @if (session('status'))

            AlertNotification('success', "{{ session('status') }}");

            @elseif($errors->any())

            @foreach($errors->all() as $error)

            AlertNotification('error', "{{ $error }}");

            @endforeach

            @endif

        });

    </script>

@endsection