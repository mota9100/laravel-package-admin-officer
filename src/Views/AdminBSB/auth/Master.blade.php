<!doctype html>
<html>

<head>
    @include(ao_view('AdminBSB', 'includes.META'))
    <title>@yield('title')</title>
    @include(ao_view('AdminBSB', 'includes.CSS'), ['pages' => ['global']])
    @yield('CSS')
</head>

<body class="login-page login-body">
<div class="login-box">
    <div class="card">
        <div class="body">
            @yield('content')
        </div>
    </div>
</div>

@include(ao_view('AdminBSB', 'includes.JS'), ['pages' => ['global']])
@yield('JS')
</body>

</html>