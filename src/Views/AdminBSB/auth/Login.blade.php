﻿@extends(ao_view('AdminBSB', 'auth.Master'))

@section('title', ao_trans('adminofficer.title.login.head_title'))

@section('CSS')
    @include(ao_view('AdminBSB', 'includes.CSS'), ['pages' => ['login']])
@endsection

@section('content')
    <div class="form-submit">
        <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">person</i>
                        </span>
            <div class="form-line">
                <input type="text" class="form-control detect-direction" name="email" placeholder="{{ ao_trans('adminofficer.input.text.email') }}" required autofocus>
            </div>
        </div>
        <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">lock</i>
                        </span>
            <div class="form-line">
                <input type="password" class="form-control detect-direction" name="password" placeholder="{{ ao_trans('adminofficer.input.text.password') }}" required>
            </div>
        </div>
        <div class="row">

            <div class="col-sm-7 p-t-5 pull-left">
                <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">security</i>
                        </span>
                    <div class="form-line">
                        <input type="text" class="form-control detect-direction" name="captcha" autocomplete="off" data-title="captcha" placeholder="{{ ao_trans('adminofficer.input.text.captcha') }}" required>
                    </div>
                </div>
            </div>

            <div class="col-sm-5 p-t-5 pull-right">
                <img src="{{ ao_url('/captcha') }}" data-title="captcha">
                <i class="material-icons login-reload-captcha" onclick="ReloadLoginCaptcha();">refresh</i>
            </div>

        </div>

        <div class="row">
            <div class="col-xs-8 p-t-5">
                <input type="checkbox" name="rememberme" id="rememberme" class="filled-in chk-col-teal pull-left">
                <label for="rememberme">{{ ao_trans('adminofficer.input.check_box.remember_me') }}</label>
            </div>
            <div class="col-xs-4 p-t-5 pull-right">
                <button class="btn btn-block bg-teal waves-effect form-submit-button" id="login" type="button">{{ ao_trans('adminofficer.button.login.default') }}</button>
            </div>
        </div>
        <div class="row m-t-15 m-b--20">
            <div class="col-xs-12 align-center col-grey">
                <a class="col-grey" href="{{ route('admin.officer.password.request') }}">
                    {{ ao_trans('global.forget_password') }}
                </a>
            </div>
        </div>
    </div>
@endsection

@section('JS')
    @include(ao_view('AdminBSB', 'includes.JS'), ['pages' => ['login']])
@endsection