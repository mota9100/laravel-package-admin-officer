@extends(ao_view('AdminBSB', 'Master'))

@section('title', ao_trans('adminofficer.title.register.head_title'))

@section('css')
    @include(ao_view('AdminBSB', 'includes.CSS'), ['pages' => ['register']])
@endsection

@section('content')

    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2><i class="material-icons font-20">person_add</i>
                        {{ ao_trans('adminofficer.title.register.title') }}
                    </h2>
                </div>
                <div class="body">

                    <div class="row clearfix">

                        @if(is_null($email))

                            <div class="col-sm-12">

                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input class="form-control detect-direction" type="email" name="email">
                                        <label class="form-label">
                                            {{ ao_trans('adminofficer.input.text.email') }}
                                        </label>
                                    </div>
                                </div>

                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input class="form-control detect-direction" type="text" name="firstname">
                                        <label class="form-label">
                                            {{ ao_trans('adminofficer.input.text.first_name') }}
                                        </label>
                                    </div>
                                </div>

                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input class="form-control detect-direction" type="text" name="lastname">
                                        <label class="form-label">
                                            {{ ao_trans('adminofficer.input.text.last_name') }}
                                        </label>
                                    </div>
                                </div>

                                <div class="form-group form-float m-t-10">
                                    <div class="form-line">
                                        <input class="form-control detect-direction" type="password" name="passwordnew">
                                        <label class="form-label">
                                            {{ ao_trans('adminofficer.input.text.password_new') }}
                                        </label>
                                    </div>
                                </div>

                                <div class="form-group form-float m-t-10">
                                    <div class="form-line">
                                        <input class="form-control detect-direction" type="password" name="passwordconfirm">
                                        <label class="form-label">
                                            {{ ao_trans('adminofficer.input.text.password_confirm') }}
                                        </label>
                                    </div>
                                </div>

                            </div>

                            <div class="col-sm-12">

                                <button type="button"
                                        class="btn changble-bg bg-{{ ao_db_config('theme.skin') }} waves-effect ao-btn"
                                        name="register-button">
                                    {{ ao_trans('adminofficer.button.register.default') }}
                                </button>
                            </div>

                        @else

                            <div class="col-sm-12">

                                <input type="hidden" name="email" value="{{ $email }}">
                                <p>{{ ao_message('success', 'account_sended', $email) }}</p>

                            </div>

                            <div class="col-sm-12">

                                <button type="button"
                                        class="btn changble-bg bg-{{ ao_db_config('theme.skin') }} waves-effect ao-btn"
                                        name="send-activation-button">
                                    {{ ao_trans('adminofficer.button.send.activation_email') }}
                                </button>
                            </div>

                        @endif

                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection

@section('js')
    @include(ao_view('AdminBSB', 'includes.JS'), ['pages' => ['register']])
@endsection