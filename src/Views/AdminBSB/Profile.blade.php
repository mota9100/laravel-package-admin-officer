@extends(ao_view('AdminBSB', 'Master'))

@section('title', ao_trans('adminofficer.title.profile.head_title'))

@section('css')
    @include(ao_view('AdminBSB', 'includes.CSS'), ['pages' => ['profile']])
@endsection

@section('content')

    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2><i class="material-icons font-20">person</i>
                        {{ ao_trans('adminofficer.title.profile.title') }}
                    </h2>
                </div>
                <div class="body">

                    <div class="row clearfix">

                        <div class="col-sm-12 ao-profile-avatar-container">
                            <img class="avatar-image" src="/{{ $profile->Avatar }}">
                            <input type="hidden" class="avatar-image" name="avatar" value="{{ $profile->Avatar }}"/>
                            <button type="button"
                                    class="btn changble-bg bg-{{ ao_db_config('theme.skin') }} waves-effect"
                                    data-fm-open="image"
                                    data-fm-function="SetAvatar">
                                {{ ao_trans('adminofficer.button.update.avatar') }}
                            </button>
                        </div>

                        <div class="col-sm-12">

                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input class="form-control detect-direction" type="text" name="email" value="{{ $profile->Email }}">
                                    <label class="form-label">
                                        {{ ao_trans('adminofficer.input.text.email') }}
                                    </label>
                                </div>
                            </div>

                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input class="form-control detect-direction" type="text" name="firstname" value="{{ $profile->FirstName }}">
                                    <label class="form-label">
                                        {{ ao_trans('adminofficer.input.text.first_name') }}
                                    </label>
                                </div>
                            </div>

                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input class="form-control detect-direction" type="text" name="lastname" value="{{ $profile->LastName }}">
                                    <label class="form-label">
                                        {{ ao_trans('adminofficer.input.text.last_name') }}
                                    </label>
                                </div>
                            </div>

                            @if($multiLanguageStatus == 1)

                                <div class="form-group">
                                    <label for="language">
                                        {{ ao_trans('adminofficer.input.select.language') }}
                                    </label>
                                    <select id="language" name="language" class="form-control show-tick selectpicker">
                                        @foreach($languages as $language)
                                            <option data-content="<img width='25' src='/{{ $language->Flag }}'> <span>{{ $language->Language }}</span>" value="{{ $language->LanguageID }}" @if($profile->LanguageID == $language->LanguageID) selected @endif >
                                                {{ $language->Language }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>

                            @endif


                            <div class="collapse" id="collapseExample">

                                <div class="form-group form-float m-t-10">
                                    <div class="form-line">
                                        <input class="form-control detect-direction" type="password" name="passwordold">
                                        <label class="form-label">
                                            {{ ao_trans('adminofficer.input.text.password_old') }}
                                        </label>
                                    </div>
                                </div>

                                <div class="form-group form-float m-t-10">
                                    <div class="form-line">
                                        <input class="form-control detect-direction" type="password" name="passwordnew">
                                        <label class="form-label">
                                            {{ ao_trans('adminofficer.input.text.password_new') }}
                                        </label>
                                    </div>
                                </div>

                                <div class="form-group form-float m-t-10">
                                    <div class="form-line">
                                        <input class="form-control detect-direction" type="password" name="passwordconfirm">
                                        <label class="form-label">
                                            {{ ao_trans('adminofficer.input.text.password_confirm') }}
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-12">

                            <button type="button" class="btn bg-red waves-effect password-collapse"
                                    data-toggle="collapse" data-target="#collapseExample" aria-expanded="false"
                                    aria-controls="collapseExample">
                                {{ ao_trans('adminofficer.button.update.password') }}
                            </button>

                            <button type="button"
                                    class="btn changble-bg bg-{{ ao_db_config('theme.skin') }} waves-effect ao-btn update-profile">
                                {{ ao_trans('adminofficer.button.save.profile') }}
                            </button>

                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>


@endsection

@section('js')
    <script type="text/javascript">

        var multiLanguage = parseInt({{ $multiLanguageStatus }}) === 1;

    </script>
    @include(ao_view('AdminBSB', 'includes.FileManager'))
    @include(ao_view('AdminBSB', 'includes.JS'), ['pages' => ['profile']])
@endsection