@extends(ao_view('AdminBSB', 'Master'))

@section('title', ao_trans('pages.file_manager.head_title'))

@section('css')

    @include(ao_view('AdminBSB', 'includes.CSS'), ['pages' => ['global', 'dashboard']]);

@endsection

@section('content')

    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card show-context" data-ftype="container">
                <div class="header">

                    <h2 class="card-title">{{ ao_trans('pages.file_manager.title') }}</h2>

                    <button type="button" class="btn btn-default waves-effect m-r-5 hide" id="to-previous">
                        <span>{{ ao_trans('file-manager.nav-back') }}</span>
                        <i class="material-icons">arrow_back_ios</i>
                    </button>

                    <ul class="header-dropdown m-r--5">
                        <li class="dropdown">
                            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                <i class="material-icons">more_vert</i>
                            </a>
                            <ul class="dropdown-menu pull-right">
                                <li>
                                    <a href="javascript:void(0);" class="waves-effect waves-block" id="thumbnail-display">
                                        <i class="material-icons">grid_on</i>
                                        <span>{{ ao_trans('file-manager.nav-thumbnails') }}</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);" class="waves-effect waves-block" id="list-display">
                                        <i class="material-icons">view_list</i>
                                        <span>{{ ao_trans('file-manager.nav-list') }}</span>
                                    </a>
                                </li>
                                <li class="divider"></li>
                                <li>
                                    <a href="javascript:void(0);" class="waves-effect waves-block" id="list-sort-alphabetic">
                                        <i class="material-icons">sort_by_alpha</i>
                                        {{ ao_trans('file-manager.nav-sort-alphabetic') }}
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);" class="waves-effect waves-block" id="list-sort-time">
                                        <i class="material-icons">date_range</i>
                                        {{ ao_trans('file-manager.nav-sort-time') }}
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>

                <div class="body">

                    <div class="row clearfix">

                        <div class="visible-xs" id="current_dir" style="padding: 5px 15px;background-color: #f8f8f8;color: #5e5e5e;"></div>

                        <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 ltr-dir text-align-left" style="min-height: 200px; border-left:.5px #e0e0e0 solid;">
                            <div id="content"></div>
                        </div>

                        <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 ltr-dir">
                            <div id="tree"></div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="contextMenu" class="file-manager-context-menu dropdown clearfix" data-type="container">
        <ul class="dropdown-menu" role="menu" style="display:block;position:static;margin-bottom:5px;">
            <li><a href="javascript:void(0);" id="add-folder"><i class="material-icons">create_new_folder</i> {{ ao_trans('file-manager.menu-new-folder') }}</a></li>
            <li><a href="javascript:void(0);" id="upload"><i class="material-icons">cloud_upload</i> {{ ao_trans('file-manager.menu-upload') }}</a></li>
        </ul>
    </div>

    <div id="contextMenuFile" class="file-manager-context-menu dropdown clearfix" data-type="file">
        <ul class="dropdown-menu" role="menu" style="display:block;position:static;margin-bottom:5px;">
            <li><a href="javascript:download()"><i class="material-icons">cloud_download</i> {{ ao_trans('file-manager.menu-download') }}</a></li>
            <li class="divider"></li>
            <li><a href="javascript:rename()"><i class="material-icons">edit</i> {{ ao_trans('file-manager.menu-rename') }}</a></li>
            <li><a href="javascript:trash()"><i class="material-icons">delete</i> {{ ao_trans('file-manager.menu-delete') }}</a></li>
        </ul>
    </div>

    <div id="contextMenuImage" class="file-manager-context-menu dropdown clearfix" data-type="image">
        <ul class="dropdown-menu" role="menu" style="display:block;position:static;margin-bottom:5px;">
            <li><a href="javascript:download()"><i class="material-icons">cloud_download</i> {{ ao_trans('file-manager.menu-download') }}</a></li>
            <li class="divider"></li>
            <li><a href="javascript:fileView()"><i class="material-icons">photo_size_select_actual</i> {{ ao_trans('file-manager.menu-view') }}</a></li>
            <li><a href="javascript:resizeImage()"><i class="material-icons">crop</i> {{ ao_trans('file-manager.menu-resize') }}</a></li>
            <li><a href="javascript:cropImage()"><i class="material-icons">crop_free</i> {{ ao_trans('file-manager.menu-crop') }}</a></li>
            <li class="divider"></li>
            <li><a href="javascript:rename()"><i class="material-icons">edit</i> {{ ao_trans('file-manager.menu-rename') }}</a></li>
            <li><a href="javascript:trash()"><i class="material-icons">delete</i> {{ ao_trans('file-manager.menu-delete') }}</a></li>
        </ul>
    </div>

    <div id="contextMenuFolder" class="file-manager-context-menu dropdown clearfix" data-type="folder">
        <ul class="dropdown-menu" role="menu" style="display:block;position:static;margin-bottom:5px;">
            <li><a href="javascript:rename()"><i class="material-icons">edit</i> {{ ao_trans('file-manager.menu-rename') }}</a></li>
            <li><a href="javascript:trash()"><i class="material-icons">delete</i> {{ ao_trans('file-manager.menu-delete') }}</a></li>
        </ul>
    </div>

    <div class="modal fade" id="uploadModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aia-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">{{ ao_trans('file-manager.title-upload') }}</h4>
                </div>
                <div class="modal-body">
                    <form action="{{ route('admin-officer.filemanager.upload') }}" role='form' id='uploadForm' name='uploadForm' method='post' enctype='multipart/form-data' class="dropzone">
                        <div class="form-group" id="attachment">

                            <div class="controls text-center">
                                <div class="input-group" style="width: 100%">
                                    <a class="btn btn-primary" id="upload-button">{{ ao_trans('file-manager.message-choose') }}</a>
                                </div>
                            </div>
                        </div>
                        <input type='hidden' name='working_dir' id='working_dir'>
                        <input type='hidden' name='type' id='type' value='{{ request("type") }}'>
                        <input type='hidden' name='_token' value='{{csrf_token()}}'>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ ao_trans('file-manager.btn-close') }}</button>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')
    <script>
        var routePrefix = "{{ url('/') }}";
        var fileManagerRoute = "{{ fm_base_url()  }}";
        var lang = JSON.parse(JSON.stringify({!! json_encode(ao_trans('file-manager')) !!}));
        var maxSize = parseInt({{ lcfirst(str_singular(request('type', 'image'))) == 'image' ? ao_config('file_manager.max_image_size') : ao_config('file_manager.max_file_size') }});
        var acceptFiles = "{{ lcfirst(str_singular(request('type', 'image'))) == 'image' ? implode(',', ao_config('file_manager.valid_image_mimetypes')) : implode(',', ao_config('file_manager.valid_file_mimetypes')) }}";
        var show_list = parseInt({{ ao_db_config('setting.filemanager.list') }});
        var sort_type = '{{ ao_db_config('setting.filemanager.sort') }}';
    </script>
    @include(ao_view('AdminBSB', 'includes.JS'), ['pages' => ['dashboard']]);
@endsection
