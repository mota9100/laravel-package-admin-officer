<ul class="list-unstyled file-tree-list">
  @foreach($root_folders as $root_folder)
    <li>
      <a class="clickable folder-item folder-item-tree waves-effect waves-block list-view-a" data-id="{{ $root_folder->path }}">
        <i class="material-icons">folder</i>
        {{ $root_folder->name }}
      </a>
    </li>
    @foreach($root_folder->children as $directory)
      <li style="margin-left: 20px;">
        <a class="clickable folder-item folder-item-tree list-view-a" data-id="{{ $directory->path }}">
          <i class="material-icons">folder</i>
          {{ $directory->name }}
        </a>
      </li>
    @endforeach
    @if($root_folder->has_next)
      <hr>
    @endif
  @endforeach
</ul>
