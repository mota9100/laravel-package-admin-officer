<div class="row">

    @if($filesCount > 0  || $directoriesCount > 0)

        @for($indexItem = 1; $indexItem <= count($items); $indexItem++)
            @php $item = $items[$indexItem-1] @endphp

            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-6 fmg-box-grid show-context"
                 data-item-box
                 data-item-number="fmg-item-{{ $indexItem }}"
                 data-item-name="{{ $item->name }}"
                 data-item-url="{{ $item->url }}"
                 data-item-type="{{ $item->item_type }}"
                 data-balloon-length="small"
                 data-balloon="{{ $item->name }}"
                 data-balloon-pos="down"
                 data-folder-type="{{ $item->folder_type }}"
                 data-work-directory="{{ $item->work_directory }}"
                 @if($item->item_type == 'image')
                 data-magnify="gallery"
                 data-caption="{{ $item->name }}"
                 data-src="{{ $item->url }}"
                 @endif
                 unselectable="on">


                @if($item->thumb)

                    <div class="fmg-image-preloader">
                        <i class="fas fa-spinner fa-spin"></i>
                        <img src="{{ $item->thumb }}" style="display: none">
                    </div>

                @else

                    @if($item->icon_type == 'material')

                        <i class="material-icons @if($item->icon == 'folder') changble-col col-{{ ao_db_config('theme.skin') }} @endif ">{{ $item->icon }}</i>

                    @else

                        <i class="fmg-font-file">
                                <span class="fmg-font-extension">
                                    {{ substr($item->extension, 0, 4) }}
                                </span>
                        </i>

                    @endif

                @endif

                <span class="fmg-item-title fmg-grid-title" data-name="{{ $item->name }}">{{ $item->name }}</span>


            </div>
        @endfor


    @else

        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fmg-empty-folder">
            <i class="material-icons">folder_open</i><br>
            <p>
                {{ ao_trans('file-manager.message_empty') }}
            </p>
        </div>

    @endif

    <script type="text/javascript">

        SetPreviousDirectory("{{ $previousFolderType }}", "{{ $previousWorkDirectory }}");

        if(typeof UnsetSelected === 'function') {

            UnsetSelected();
        }

    </script>

</div>


