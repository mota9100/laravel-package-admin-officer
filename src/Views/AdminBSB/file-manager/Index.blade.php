@php
    $includePages = [
        'fm'
    ];

    if($selectable)
        $includePages[] = 'fm_selectable';
@endphp

@include(ao_view('AdminBSB', 'includes.CSS'), ['pages' => $includePages])

<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card show-context fmg-card" data-item-type="container">

            <div class="fmg-overlay hide">
                <div class="preloader hide">
                    <div class="spinner-layer pl-white">
                        <div class="circle-clipper left">
                            <div class="circle"></div>
                        </div>
                        <div class="circle-clipper right">
                            <div class="circle"></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="fmg-main-overlay fmg-transparent-overlay hide"></div>

            <div class="header">

                <div class="m-b-10">
                    <ul class="header-dropdown fmg-setting-button">
                        <li class="dropdown">
                            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button"
                               aria-haspopup="true" aria-expanded="false">
                                <i class="material-icons">settings</i>
                            </a>
                            <ul class="dropdown-menu pull-left">
                                <li>
                                    <a href="javascript:ChangeViewType('grid');" class="waves-effect waves-block">
                                        <i class="material-icons">grid_on</i>
                                        <span>{{ ao_trans('file-manager.nav_thumbnails') }}</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:ChangeViewType('list');" class="waves-effect waves-block">
                                        <i class="material-icons">view_list</i>
                                        <span>{{ ao_trans('file-manager.nav_list') }}</span>
                                    </a>
                                </li>
                                <li class="divider"></li>
                                <li>
                                    <a href="javascript:ChangeSortType('alphabetic');" class="waves-effect waves-block">
                                        <i class="material-icons">sort_by_alpha</i>
                                        {{ ao_trans('file-manager.nav_sort_alphabetic') }}
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:ChangeSortType('time');" class="waves-effect waves-block">
                                        <i class="material-icons">date_range</i>
                                        {{ ao_trans('file-manager.nav_sort_time') }}
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>

                    <a href="javascript:PreviousDirectory();" class="fmg-back-button">
                        <i class="material-icons">arrow_back</i>
                    </a>
                </div>

            </div>

            <div class="body content-body fmg-body">

                <div class="row clearfix">

                    <div id="fmContent"
                         class="fmg-content col-lg-12 col-md-12 col-sm-12 col-xs-12 fmg-ltr-dir fmg-text-align-left"></div>
                    {{--<div id="tree" class="tree col-sm-3 ltr-dir text-align-left" data-visible="hide"></div>--}}

                </div>
            </div>

            <div class="card fmg-inner-card fmg-upload-box">

                <div class="header">

                    <div class="m-b-10">
                        <a href="javascript:ToggleUpload();" class="fmg-close-button">
                            <i class="material-icons">close</i>
                        </a>

                        <a href="javascript:OpenFileBrowser();" class="fmg-upload-open-button">
                            <i class="material-icons">folder</i>
                        </a>

                        <a href="javascript:ClearUploadList();" class="fmg-upload-clear-button hide">
                            <i class="material-icons">clear_all</i>
                        </a>
                    </div>

                </div>

                <div class="body">

                    <div class="row clearfix">

                        <form class="fmg-upload-form" enctype="multipart/form-data"><input multiple type="file"/></form>

                        <div class="fmg-upload-area fmd-clickable">
                            <div class="fmg-upload-area-first-message">
                                <span class="fmg-transparent-overlay"></span>
                                <i class="fmg-font-upload fm-dz-message-icon"></i>
                                <p class="fm-dz-message-text">برای آپلود کلیک یا فایل خود را اینجا بیندازید</p>
                            </div>
                            <div class="fmg-upload-area-files"></div>
                        </div>

                    </div>

                </div>

            </div>

            <div class="card fmg-inner-card fmg-new-folder-box with-overlay">

                <div class="fmg-create-folder-overlay fmg-transparent-overlay hide">
                    <div class="preloader pl-size-l">
                        <div class="spinner-layer changble-pl pl-{{ ao_db_config('theme.skin') }}">
                            <div class="circle-clipper left">
                                <div class="circle"></div>
                            </div>
                            <div class="circle-clipper right">
                                <div class="circle"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="header">

                    <div>
                        <a href="javascript:ToggleNewFolder();" class="fmg-close-button">
                            <i class="material-icons">close</i>
                        </a>

                        <p class="m-b--10 m-t--10 fmg-text-with-icon">
                            <i class="material-icons">create_new_folder</i>
                            {{ ao_trans('file-manager.title_new_folder') }}
                        </p>
                    </div>

                </div>

                <div class="body">

                    <div class="row form-submit clearfix">

                        <div class="col-sm-10 m-b--20">
                            <div class="form-group">
                                <div class="form-line">
                                    <input class="form-control fmg-create-folder-name detect-direction" name="text"
                                           placeholder="{{ ao_trans('adminofficer.input.text.folder_name') }}"
                                           required="" autofocus="" type="text">
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-2 m-b--20">
                            <button type="button" onclick="CreateFolder(this);"
                                    class="btn btn-block changble-bg bg-{{ ao_db_config('theme.skin') }} waves-effect form-submit-button">
                                {{ ao_trans('adminofficer.button.create.default') }}
                            </button>
                        </div>
                    </div>
                </div>

            </div>

            <div class="card fmg-inner-card fmg-delete-box with-overlay">

                <div class="fmg-delete-overlay fmg-transparent-overlay hide">
                    <div class="preloader pl-size-l">
                        <div class="spinner-layer changble-pl pl-{{ ao_db_config('theme.skin') }}">
                            <div class="circle-clipper left">
                                <div class="circle"></div>
                            </div>
                            <div class="circle-clipper right">
                                <div class="circle"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="header">

                    <div>
                        <a href="javascript:ToggleDelete();" class="fmg-close-button">
                            <i class="material-icons">close</i>
                        </a>

                        <p class="m-b--10 m-t--10 fmg-text-with-icon">
                            <i class="material-icons">delete</i>
                            {{ ao_trans('file-manager.title_delete') }}
                        </p>
                    </div>

                </div>

                <div class="body m-b-20">

                    <div class="row clearfix form-submit">

                        <div class="col-sm-8 m-b--20">
                            <p class="fmg-delete-text"></p>
                        </div>

                        <div class="col-sm-2 m-b--20">
                            <button type="button" onclick="Delete();"
                                    class="btn btn-block bg-red waves-effect fmg-delete-yes-btn">
                                {{ ao_trans('adminofficer.button.delete.default') }}
                            </button>
                        </div>

                        <div class="col-sm-2 m-b--20">
                            <button type="button" onclick="ToggleDelete();"
                                    class="btn btn-block bg-blue waves-effect fmg-delete-no-btn form-submit-button">
                                {{ ao_trans('adminofficer.button.cancel.default') }}
                            </button>
                        </div>

                    </div>
                </div>

            </div>

        </div>
    </div>
</div>

<div id="contextMenu" class="dropdown clearfix fmg-context-menu" data-type="container">
    <ul class="dropdown-menu" role="menu">
        <li>
            <a href="javascript:ToggleNewFolder();">
                <i class="material-icons">create_new_folder</i>
                {{ ao_trans('file-manager.menu_new_folder') }}
            </a>
        </li>
        <li>
            <a href="javascript:ToggleUpload();" id="upload">
                <i class="material-icons">cloud_upload</i>
                {{ ao_trans('file-manager.menu_upload') }}
            </a>
        </li>
        <li class="fmg-paste hide">
            <a href="javascript:Paste()">
                <i class="fas fa-paste"></i>
                {{ ao_trans('file-manager.menu_paste') }}
            </a>
        </li>
    </ul>
</div>

<div id="contextMenuFile" class="dropdown clearfix fmg-context-menu" data-type="file">
    <ul class="dropdown-menu" role="menu">
        <li>
            <a href="javascript:Download();">
                <i class="material-icons">cloud_download</i>
                {{ ao_trans('file-manager.menu_download') }}
            </a>
        </li>
        <li>
            <a href="javascript:void(0);" class="copy-clipboard">
                <i class="fas fa-clipboard"></i>
                {{ ao_trans('file-manager.menu_copy_url') }}
            </a>
        </li>
        <li class="divider"></li>
        <li>
            <a href="javascript:RenamePopOver();">
                <i class="fas fa-file-signature"></i>
                {{ ao_trans('file-manager.menu_rename') }}
            </a>
        </li>
        <li>
            <a href="javascript:ToggleDelete();">
                <i class="material-icons">delete</i>
                {{ ao_trans('file-manager.menu_delete') }}
            </a>
        </li>
        <li class="divider"></li>
        <li>
            <a href="javascript:Copy();">
                <i class="fas fa-copy"></i>
                {{ ao_trans('file-manager.menu_copy') }}
            </a>
        </li>
        <li>
            <a href="javascript:Cut();">
                <i class="fas fa-cut"></i>
                {{ ao_trans('file-manager.menu_cut') }}
            </a>
        </li>
    </ul>
</div>

<div id="contextMenuImage" class="dropdown clearfix fmg-context-menu" data-type="image">
    <ul class="dropdown-menu" role="menu">
        <li>
            <a href="javascript:PreviewImage();">
                <i class="material-icons">pageview</i>
                {{ ao_trans('file-manager.menu_view') }}
            </a>
        </li>
        <li>
            <a href="javascript:Download();">
                <i class="material-icons">cloud_download</i>
                {{ ao_trans('file-manager.menu_download') }}
            </a>
        </li>
        <li>
            <a href="javascript:void(0);" class="copy-clipboard">
                <i class="fas fa-clipboard"></i>
                {{ ao_trans('file-manager.menu_copy_url') }}
            </a>
        </li>
        <li>
            <a href="javascript:ShowOptimizeModal();">
                <i class="material-icons">compare</i>
                {{ ao_trans('file-manager.menu_optimize') }}
            </a>
        </li>
        {{--<li class="divider"></li>
        <li>
            <a href="javascript:ResizeImage()">
                <i class="material-icons">crop</i>
                {{ ao_trans('file-manager.menu_resize') }}
            </a>
        </li>
        <li>
            <a href="javascript:CropImage()">
                <i class="material-icons">crop_free</i>
                {{ ao_trans('file-manager.menu_crop') }}
            </a>
        </li>--}}
        <li class="divider"></li>
        <li>
            <a href="javascript:Copy();">
                <i class="fas fa-copy"></i>
                {{ ao_trans('file-manager.menu_copy') }}
            </a>
        </li>
        <li>
            <a href="javascript:Cut();">
                <i class="fas fa-cut"></i>
                {{ ao_trans('file-manager.menu_cut') }}
            </a>
        </li>
        <li class="divider"></li>
        <li>
            <a href="javascript:RenamePopOver()">
                <i class="fas fa-file-signature"></i>
                {{ ao_trans('file-manager.menu_rename') }}
            </a>
        </li>
        <li>
            <a href="javascript:ToggleDelete()">
                <i class="material-icons">delete</i>
                {{ ao_trans('file-manager.menu_delete') }}
            </a>
        </li>
    </ul>
</div>

<div id="contextMenuFolder" class="dropdown clearfix fmg-context-menu" data-type="folder">
    <ul class="dropdown-menu" role="menu">
        <li>
            <a href="javascript:RenamePopOver()">
                <i class="fas fa-file-signature"></i>
                {{ ao_trans('file-manager.menu_rename') }}
            </a>
        </li>
        <li>
            <a href="javascript:ToggleDelete()"><i class="material-icons">delete</i>
                {{ ao_trans('file-manager.menu_delete') }}
            </a>
        </li>
        <li class="divider"></li>
        <li>
            <a href="javascript:Copy();">
                <i class="fas fa-copy"></i>
                {{ ao_trans('file-manager.menu_copy') }}
            </a>
        </li>
        <li>
            <a href="javascript:Cut();">
                <i class="fas fa-cut"></i>
                {{ ao_trans('file-manager.menu_cut') }}
            </a>
        </li>
    </ul>
</div>

<div class="modal fade" id="optimizeModal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="smallModalLabel">{{ ao_trans('file-manager.titile_optimize') }}</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12 fmg-optimize-preview m-b-20">
                        <img alt="" class="img-responsive" src="{{ ao_asset('images/preview.png') }}">
                        <a href="{{ ao_asset('images/preview.png') }}"
                           target="_blank"
                           data-balloon="{{ ao_trans('file-manager.optimize_show_preview') }}"
                           data-balloon-pos="left"><i class="far fa-clone"></i></a>
                    </div>
                    <div class="col-md-6">
                        <p><b>{{ ao_trans('file-manager.titile_optimize_quality') }}</b></p>
                        <div id="optimizeChangeQuality"></div>
                        <div class="m-t-20 font-12">
                            <b>{{ ao_trans('adminofficer.sub_titles.quality') }}: </b>
                            <span id="optimizeChangeQualityValue"></span>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <p><b>{{ ao_trans('file-manager.titile_optimize_jpeg') }}</b></p>
                        <div id="optimizeJpegConvert"></div>
                        <div class="m-t-20 font-12">
                            <b>{{ ao_trans('adminofficer.sub_titles.quality') }}: </b>
                            <span id="optimizeJpegConvertValue"></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <span class="pull-left">
                    <input type="checkbox" id="optimizeOverWriteCheck" class="filled-in chk-col-cyan" checked />
                    <label for="optimizeOverWriteCheck">{{ ao_trans('file-manager.optimize_overwrite_message') }}</label>
                </span>
                <button type="button" id="optimizePreview" class="btn btn-link waves-effect">{{ ao_trans('file-manager.btn_preview') }}</button>
                <button type="button" id="optimizeFinal" class="btn btn-link waves-effect">{{ ao_trans('file-manager.btn_optimize') }}</button>
                <button type="button" class="btn btn-link waves-effect" onclick="CloseOptimizeModal();">{{ ao_trans('file-manager.btn_close') }}</button>
            </div>
        </div>
    </div>
</div>

<input type="text" id="clipboardInput" style="display: none; opacity: 0;">

<script>
    var fmType = 'all';
    var fmFunction, fmButton;
    var routePrefix = "{{ url('/') }}";
    var fileManagerRoute = "{{ fm_base_url()  }}";
    var lang = JSON.parse(JSON.stringify({!! json_encode(ao_trans('file-manager')) !!}));
    var viewType = '{{ ao_db_config('setting.filemanager.list') }}';
    var sortType = '{{ ao_db_config('setting.filemanager.sort') }}';
</script>

@include(ao_view('AdminBSB', 'includes.JS'), ['pages' => $includePages])
