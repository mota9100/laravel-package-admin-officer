<div class="row">

    @if(($filesCount > 0) || $directoriesCount > 0)

        <table class="table ltr-dir">

            {{--<thead class="text-align-center">--}}
            {{--<tr>--}}
                {{--<th style="width:40%;" class="text-align-left">{{ ao_trans('file-manager.title_item') }}</th>--}}
                {{--<th class="text-align-center">{{ ao_trans('file-manager.title_size') }}</th>--}}
                {{--<th class="text-align-center">{{ ao_trans('file-manager.title_type') }}</th>--}}
                {{--<th class="text-align-center">{{ ao_trans('file-manager.title_modified') }}</th>--}}
            {{--</tr>--}}
            {{--</thead>--}}

            <tbody>
            @php
                $totalCount = count($items);
            @endphp

            @for($indexItem = 1; $indexItem <= $totalCount; $indexItem++)
                @php
                    $item = $items[$indexItem-1];
                @endphp

                <tr class="fmg-box-list show-context"
                    data-item-box
                    data-item-number="fmg-item-{{ $indexItem }}"
                    data-item-name="{{ $item->name }}"
                    data-item-url="{{ $item->url }}"
                    data-item-type="{{ $item->item_type }}"
                    data-folder-type="{{ $item->folder_type }}"
                    data-work-directory="{{ $item->work_directory }}"
                    @if($item->item_type == 'image')
                    data-magnify="gallery"
                    data-caption="{{ $item->name }}"
                    data-src="{{ $item->url }}"
                    @endif
                    unselectable="on">

                    <td class="p-t-15 text-align-left fmg-list-title @if($indexItem == 1) fmg-border-top-none @elseif($indexItem == $totalCount) fmg-border-bottom-none @endif "
                        @if(strlen($item->name) > 30)
                        data-balloon-length="small"
                        data-balloon="{{ $item->name }}"
                        data-balloon-pos="down"
                        @endif >

                        @if($item->icon_type == 'material')

                            <i class="fmg-list-icon">
                                <i class="material-icons @if($item->icon == 'folder') changble-col col-{{ ao_db_config('theme.skin') }} @endif ">{{ $item->icon }}</i>
                            </i>

                        @else

                            <i class="fmg-list-icon">
                                {{ substr($item->extension, 0, 4) }}
                            </i>

                        @endif

                        <span class="fmg-item-title" data-name="{{ $item->name }}">
                            {{ str_limit($item->name, $limit = 30, $end = '...') }}
                        </span>

                    </td>

                    <td class="p-t-15 fmg-text-align-center @if($indexItem == 1) fmg-border-top-none @elseif($indexItem == $totalCount) fmg-border-bottom-none @endif ">
                        {{ $item->size }}
                    </td>

                    <td class="p-t-15 fmg-text-align-center @if($indexItem == 1) fmg-border-top-none @elseif($indexItem == $totalCount) fmg-border-bottom-none @endif ">
                        {{ ($item->extension != '') ? $item->extension : $item->type }}
                    </td>

                    <td class="p-t-15 fmg-text-align-center @if($indexItem == 1) fmg-border-top-none @elseif($indexItem == $totalCount) fmg-border-bottom-none @endif ">
                        {{ $item->persian_modify }}
                    </td>
                </tr>
            @endfor
            </tbody>
        </table>

    @else

        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fmg-empty-folder">
            <i class="material-icons">folder_open</i><br>
            <p>
                {{ ao_trans('file-manager.message_empty') }}
            </p>
        </div>

    @endif

        <script type="text/javascript">

            SetPreviousDirectory("{{ $previousFolderType }}", "{{ $previousWorkDirectory }}");

            if(typeof UnsetSelected === 'function') {

                UnsetSelected();
            }

        </script>

</div>