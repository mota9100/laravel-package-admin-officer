<aside id="leftsidebar" class="sidebar">
    <!-- User Info -->
    <div class="user-info">
        <div class="image">
            <img src="{{ ao_db_config('setting.avatar') }}" width="48" height="48" alt="User" />
        </div>
        <div class="info-container">
            <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{ ao_db_config('fullname') }}</div>
            <div class="email">{{ ao_db_config('email') }}</div>
            <div class="btn-group user-helper-dropdown">
                <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                <ul class="dropdown-menu pull-right">
                    <li><a href="{{ ao_url('profile') }}"><i class="material-icons">person</i>{{ ao_trans('adminofficer.button.profile.default') }}</a></li>
                    <li role="seperator" class="divider"></li>
                    <li><a id="logout"><i class="fas fa-sign-out-alt"></i>{{ ao_trans('adminofficer.button.logout.default') }}</a></li>
                </ul>
            </div>
        </div>
    </div>
    <!-- #User Info -->
    <!-- Menu -->
    @php

    $menuConfig = ao_config('system_menu', []);

    @endphp

    <div class="menu">
        <ul class="list">

            @if(in_array('dashboard', $menuConfig))

                <li data-route="/officer">
                    <a target="_self" href="{{ ao_url() }}">
                        <i class="material-icons">home</i>
                        <span>{{ ao_trans('adminofficer.title.dashboard.title') }}</span>
                    </a>
                </li>

            @endif

            @if(in_array('file_manager', $menuConfig))

                <li data-route="/officer/filemanager">
                    <a target="_self" href="{{ ao_url('filemanager') }}">
                        <i class="material-icons">folder</i>
                        <span>{{ ao_trans('adminofficer.title.file_manager.title') }}</span>
                    </a>
                </li>

            @endif

            @if(view()->exists(ao_base_view('AdminBSB', 'includes.Menu')))
                @include(ao_base_view('AdminBSB', 'includes.Menu'))
            @endif
        </ul>
    </div>
    <!-- #Menu -->
    <!-- Footer -->
    <div class="legal">
        <div class="copyright">
            {{ ao_config('copyright', '') }} &copy; <a href="javascript:void(0);">{{ ao_config('name') }} By MOTA {{ ao_config('version') }}</a>
        </div>
    </div>
    <!-- #Footer -->
</aside>













{{--
<li class="active">
    <a href="javascript:void(0);" class="menu-toggle">
        <i class="material-icons">assignment</i>
        <span>Forms</span>
    </a>
    <ul class="ml-menu">
        <li>
            <a href="../../pages/forms/basic-form-elements.html">Basic Form Elements</a>
        </li>
        <li>
            <a href="../../pages/forms/advanced-form-elements.html">Advanced Form Elements</a>
        </li>
        <li>
            <a href="../../pages/forms/form-examples.html">Form Examples</a>
        </li>
        <li>
            <a href="../../pages/forms/form-validation.html">Form Validation</a>
        </li>
        <li>
            <a href="../../pages/forms/form-wizard.html">Form Wizard</a>
        </li>
        <li>
            <a href="../../pages/forms/editors.html">Editors</a>
        </li>
    </ul>
</li>--}}
