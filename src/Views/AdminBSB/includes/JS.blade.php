@if(isset($pages))

    @if(in_array('global', $pages))
        <script type="text/javascript" src="{{ ao_asset("packages/jquery-1.12.4/jquery.min.js") }}"></script>
        <script type="text/javascript"
                src="{{ ao_asset("packages/jquery-ui/jquery-ui-1.11.4/jquery-ui.min.js") }}"></script>
        <script type="text/javascript"
                src="{{ ao_asset("packages/dashboard-bootstrap/js/bootstrap.js") }}"></script>
        <script type="text/javascript"
                src="{{ ao_asset("packages/izi-toast/js/iziToast.min.js") }}"></script>
        <script type="text/javascript" src="{{ ao_asset("packages/node-waves/waves.js") }}"></script>
        <script type="text/javascript" src="{{ ao_asset("packages/Nprogress/js/nprogress.js") }}"></script>
        <script type="text/javascript" src="{{ ao_asset("js/script.js") }}"></script>
    @endif

    @if(in_array('fm', $pages))
        <script type="text/javascript" src="{{ ao_asset("packages/magnify/js/jquery.magnify.js") }}"></script>
        <script type="text/javascript" src="{{ ao_asset("packages/sprintf/js/sprintf.min.js") }}"></script>
        <script type="text/javascript" src="{{ ao_asset("packages/webui-popover/js/jquery.webui-popover.js") }}"></script>
        <script type="text/javascript" src="{{ ao_asset("packages/nouislider/js/nouislider.min.js") }}"></script>
        {{--<script type="text/javascript" src="{{ ao_asset("packages/cropper/js/cropper.min.js") }}"></script>--}}
        <script type="text/javascript" src="{{ ao_asset("js/fm.js") }}"></script>
        @if(in_array('fm_selectable', $pages))
            <script type="text/javascript" src="{{ao_asset("packages/selectable/js/jquery.selectable.min.js")}}"></script>
            <script type="text/javascript" src="{{ao_asset("js/fm-selectable.js")}}"></script>
        @endif
    @endif

    @if(in_array('dashboard', $pages))

        <script type="text/javascript" src="{{ ao_asset("js/admin-ajaxes.js") }}"></script>
        <script type="text/javascript"
                src="{{ ao_asset("packages/bootstrap-select/js/bootstrap-select.js") }}"></script>
        <script type="text/javascript"
                src="{{ ao_asset("packages/jquery-slimscroll/jquery.slimscroll.js") }}"></script>
        <script type="text/javascript" src="{{ ao_asset("packages/autosize/autosize.js") }}"></script>
        <script type="text/javascript" src="{{ ao_asset("packages/momentjs/moment.js") }}"></script>
        <script type="text/javascript" src="{{ ao_asset("themes/AdminBSB/js/functions.js") }}"></script>
        <script type="text/javascript" src="{{ ao_asset("themes/AdminBSB/js/admin.js") }}"></script>
        {{--<script type="text/javascript" src="{{ ao_asset("themes/AdminBSB/js/animations.js") }}"></script>--}}
        {{--<script type="text/javascript" src="{{ ao_asset("themes/AdminBSB/js/dialogs.js") }}"></script>--}}
        {{--<script type="text/javascript" src="{{ ao_asset("themes/AdminBSB/js/modals.js") }}"></script>--}}
        {{--<script type="text/javascript" src="{{ ao_asset("themes/AdminBSB/js/notifications.js") }}"></script>--}}
        {{--<script type="text/javascript" src="{{ ao_asset("themes/AdminBSB/js/range-slider.js") }}"></script>--}}
        {{--<script type="text/javascript" src="{{ ao_asset("themes/AdminBSB/js/sortable-nestable.js") }}"></script>--}}
        <script type="text/javascript"
                src="{{ ao_asset("themes/AdminBSB/js/tooltips-popovers.js") }}"></script>

    @endif


    @if(in_array('profile', $pages))

        <script type="text/javascript"
                src="{{ ao_asset("packages/password-strength/password.min.js") }}"></script>
        <script type="text/javascript"
                src="{{ ao_asset("themes/AdminBSB/js/pages/profile.js") }}"></script>

    @endif

    @if(in_array('register', $pages))

        <script type="text/javascript"
                src="{{ ao_asset("packages/password-strength/password.min.js") }}"></script>
        <script type="text/javascript"
                src="{{ ao_asset("themes/AdminBSB/js/pages/register.js") }}"></script>

    @endif

    @if(in_array('file_manager', $pages))

        <script type="text/javascript"
                src="{{ ao_asset("themes/AdminBSB/js/pages/file-manager.js") }}"></script>

    @endif

    @if(in_array('login', $pages))
        <script type="text/javascript"
                src="{{ ao_asset("packages/jquery-validation/jquery.validate.js") }}"></script>
        <script type="text/javascript" src="{{ ao_asset("themes/AdminBSB/js/pages/login.js") }}"></script>
    @endif

@endif
