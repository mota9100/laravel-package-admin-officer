@if(isset($pages))

    @if(in_array('global', $pages))

        <link rel="stylesheet" type="text/css" href="{{ ao_asset("css/font-face.css")}}" />
        <link rel="stylesheet" type="text/css" href="{{ ao_asset("packages/font-awesome-5.1.1/css/all.css")}}" />
        <link rel="stylesheet" type="text/css" href="{{ ao_asset("packages/izi-toast/css/iziToast.min.css")}}" />
        <link rel="stylesheet" type="text/css" href="{{ ao_asset("packages/dashboard-bootstrap/css/bootstrap.min.css")}}" />
        <link rel="stylesheet" type="text/css" href="{{ ao_asset("packages/node-waves/waves.css")}}" />
        <link rel="stylesheet" type="text/css" href="{{ ao_asset("packages/animate-css/animate.css")}}" />
        <link rel="stylesheet" type="text/css" href="{{ ao_asset("packages/balloon-css/balloon-css.css")}}" />
        <link rel="stylesheet" type="text/css" href="{{ ao_asset("packages/jquery-ui/jquery-ui-1.11.4/jquery-ui.min.css")}}">
        <link rel="stylesheet" type="text/css" href="{{ ao_asset("packages/Nprogress/css/nprogress.css")}}" />

        @if(ao_lang('direction') == 'RTL')
            <link rel="stylesheet" type="text/css" href="{{ ao_asset("packages/bootstrap-rtl/bootstrap.rtl.min.css")}}" />
            <link rel="stylesheet" type="text/css" href="{{ ao_asset("themes/AdminBSB/css/style-rtl.css")}}" />
            <link rel="stylesheet" type="text/css" href="{{ ao_asset("themes/AdminBSB/css/custom-style-rtl.css")}}" />
        @else
            <link rel="stylesheet" type="text/css" href="{{ ao_asset("themes/AdminBSB/css/style.css")}}" />
            <link rel="stylesheet" type="text/css" href="{{ ao_asset("themes/AdminBSB/css/custom-style.css")}}" />
        @endif

    @endif

    @if(in_array('dashboard', $pages))

        <link rel="stylesheet" type="text/css" href="{{ ao_asset("packages/waitme/waitMe.css")}}" />

        @if(ao_lang('direction') == 'RTL')
            <link rel="stylesheet" type="text/css" href="{{ ao_asset("packages/bootstrap-select/css/bootstrap-select-rtl.css")}}" />
        @else
            <link rel="stylesheet" type="text/css" href="{{ ao_asset("packages/bootstrap-select/css/bootstrap-select.min.css")}}" />
        @endif

        <link rel="stylesheet" type="text/css" href="{{ ao_asset("themes/AdminBSB/css/themes/all-themes.css")}}" />

    @endif

    @if(in_array('file_manager', $pages))

        <link rel="stylesheet" type="text/css" href="{{ ao_asset("themes/AdminBSB/css/pages/file-manager.css")}}" />

    @endif

    @if(in_array('profile', $pages))

        <link rel="stylesheet" type="text/css" href="{{ ao_asset("packages/password-strength/password.min.css") }}" />
        <link rel="stylesheet" type="text/css" href="{{ ao_asset("themes/AdminBSB/css/pages/profile.css") }}" />

    @endif

    @if(in_array('fm', $pages))
{{--        <link type="text/css" rel="stylesheet" href="{{ ao_asset("packages/cropper/css/cropper.min.css") }}">--}}
        <link type="text/css" rel="stylesheet" href="{{ ao_asset("packages/magnify/css/jquery.magnify.min.css") }}"/>
        <link type="text/css" rel="stylesheet" href="{{ ao_asset("packages/webui-popover/css/jquery.webui-popover.min.css") }}"/>
        <link type="text/css" rel="stylesheet" href="{{ ao_asset("packages/nouislider/css/nouislider.rtl.css") }}"/>
        <link type="text/css" rel="stylesheet" href="{{ ao_asset("css/fm.css") }}">
        @if(in_array('fm_selectable', $pages))
            <link type="text/css" rel="stylesheet" href="{{ ao_asset("css/fm-selectable.css") }}">
        @endif
    @endif

@endif