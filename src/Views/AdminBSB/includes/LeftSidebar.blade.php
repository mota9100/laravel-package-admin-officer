<aside id="rightsidebar" class="right-sidebar">
    <ul class="nav nav-tabs tab-nav-right" role="tablist">
        <li role="presentation" class="active"><a href="#skins" data-toggle="tab">{{ ao_trans('theme.AdminBSB.skin') }}</a></li>
    </ul>
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane fade in active in active" id="skins">
            <ul class="demo-choose-skin">
                <li data-theme="red">
                    <div class="red"></div>
                    <span>{{ ao_trans('global.colors.red') }}</span>
                </li>
                <li data-theme="pink">
                    <div class="pink"></div>
                    <span>{{ ao_trans('global.colors.pink') }}</span>
                </li>
                <li data-theme="purple">
                    <div class="purple"></div>
                    <span>{{ ao_trans('global.colors.purple') }}</span>
                </li>
                <li data-theme="deep-purple">
                    <div class="deep-purple"></div>
                    <span>{{ ao_trans('global.colors.purple_deep') }}</span>
                </li>
                <li data-theme="indigo">
                    <div class="indigo"></div>
                    <span>{{ ao_trans('global.colors.blue_teal') }}</span>
                </li>
                <li data-theme="blue">
                    <div class="blue"></div>
                    <span>{{ ao_trans('global.colors.blue') }}</span>
                </li>
                <li data-theme="light-blue">
                    <div class="light-blue"></div>
                    <span>{{ ao_trans('global.colors.blue_light') }}</span>
                </li>
                <li data-theme="cyan">
                    <div class="cyan"></div>
                    <span>{{ ao_trans('global.colors.cyan') }}</span>
                </li>
                <li data-theme="teal">
                    <div class="teal"></div>
                    <span>{{ ao_trans('global.colors.teal') }}</span>
                </li>
                <li data-theme="green">
                    <div class="green"></div>
                    <span>{{ ao_trans('global.colors.green') }}</span>
                </li>
                <li data-theme="light-green">
                    <div class="light-green"></div>
                    <span>{{ ao_trans('global.colors.green_light') }}</span>
                </li>
                <li data-theme="lime">
                    <div class="lime"></div>
                    <span>{{ ao_trans('global.colors.lime') }}</span>
                </li>
                <li data-theme="yellow">
                    <div class="yellow"></div>
                    <span>{{ ao_trans('global.colors.yellow') }}</span>
                </li>
                <li data-theme="amber">
                    <div class="amber"></div>
                    <span>{{ ao_trans('global.colors.amber') }}</span>
                </li>
                <li data-theme="orange">
                    <div class="orange"></div>
                    <span>{{ ao_trans('global.colors.orange') }}</span>
                </li>
                <li data-theme="deep-orange">
                    <div class="deep-orange"></div>
                    <span>{{ ao_trans('global.colors.orange_deep') }}</span>
                </li>
                <li data-theme="brown">
                    <div class="brown"></div>
                    <span>{{ ao_trans('global.colors.brown') }}</span>
                </li>
                <li data-theme="grey">
                    <div class="grey"></div>
                    <span>{{ ao_trans('global.colors.grey') }}</span>
                </li>
                <li data-theme="blue-grey">
                    <div class="blue-grey"></div>
                    <span>{{ ao_trans('global.colors.blue_grey') }}</span>
                </li>
                <li data-theme="black">
                    <div class="black"></div>
                    <span>{{ ao_trans('global.colors.black') }}</span>
                </li>
            </ul>
        </div>
    </div>
</aside>


{{--
<div role="tabpanel" class="tab-pane fade" id="settings">
    <div class="demo-settings">
        <p>GENERAL SETTINGS</p>
        <ul class="setting-list">
            <li>
                <span>Report Panel Usage</span>
                <div class="switch">
                    <label><input type="checkbox" checked><span class="lever"></span></label>
                </div>
            </li>
            <li>
                <span>Email Redirect</span>
                <div class="switch">
                    <label><input type="checkbox"><span class="lever"></span></label>
                </div>
            </li>
        </ul>
        <p>SYSTEM SETTINGS</p>
        <ul class="setting-list">
            <li>
                <span>Notifications</span>
                <div class="switch">
                    <label><input type="checkbox" checked><span class="lever"></span></label>
                </div>
            </li>
            <li>
                <span>Auto Updates</span>
                <div class="switch">
                    <label><input type="checkbox" checked><span class="lever"></span></label>
                </div>
            </li>
        </ul>
        <p>ACCOUNT SETTINGS</p>
        <ul class="setting-list">
            <li>
                <span>Offline</span>
                <div class="switch">
                    <label><input type="checkbox"><span class="lever"></span></label>
                </div>
            </li>
            <li>
                <span>Location Permission</span>
                <div class="switch">
                    <label><input type="checkbox" checked><span class="lever"></span></label>
                </div>
            </li>
        </ul>
    </div>
</div>--}}
