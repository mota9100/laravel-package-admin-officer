<div class="modal fade" id="fmModal" tabindex="-1" role="dialog" data-open="0">
    <div class="modal-dialog ao-modal-xl" role="document">
        <div class="modal-content">
            <div class="file-manager-container"></div>
            <div class="modal-footer m-t-0 fmg-modal-footer">
                <button type="button" class="btn btn-link waves-effect ao-btn" data-dismiss="modal">
                    {{ ao_trans('adminofficer.button.close.default') }}
                </button>
                <button disabled type="button" class="btn btn-link waves-effect ao-btn fmg-choose-button"
                        onclick="OnSelectFile();">
                    {{ ao_trans('adminofficer.button.choose.default') }}
                </button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function () {
        ajaxes.fileManager.Get({
            selectable: 'true'
        });
    });
</script>