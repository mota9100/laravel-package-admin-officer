<meta charset="UTF-8" />
<meta http-equiv="X-UA-Compatible" content="ie=edge" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
<meta name="csrf-token" content="{{ csrf_token() }}" />
<meta name="admin-officer" content="{{ ao_config('route.prefix') }}"/>
<meta name="file-manager" content="{{ ao_config('file_manager.prefix') }}"/>
<meta name="current-route" content="{{ ao_current_route() }}"/>
<meta name="current-skin" content="{{ ao_db_config('theme.skin') }}"/>
