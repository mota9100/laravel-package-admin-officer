@extends(ao_view('AdminBSB', 'Master'))

@section('title', ao_trans('adminofficer.title.file_manager.head_title'))

@section('css')

    @include(ao_view('AdminBSB', 'includes.CSS'), ['pages' => ['file_manager']])

@endsection

@section('content')

    <div class="col-lg-12 col-md-12 col-sm-6 col-xs-12">
        <div class="file-manager-container"></div>
    </div>

@endsection

@section('js')

    @include(ao_view('AdminBSB', 'includes.JS'), ['pages' => ['file_manager']])

@endsection
