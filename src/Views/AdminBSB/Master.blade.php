<!doctype html>
<html lang="fa">
<head>
    @include(ao_view('AdminBSB', 'includes.META'))
    <title>@yield('title')</title>
    @include(ao_view('AdminBSB', 'includes.CSS'), ['pages' => ['global', 'dashboard']])
    @yield('css')
</head>

<body class="theme-{{ ao_db_config('theme.skin') }}">

<div class="page-loader-wrapper">
    <div class="loader">
        <div class="preloader">
            <div class="spinner-layer pl-{{ ao_db_config('theme.skin') }}">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div>
                <div class="circle-clipper right">
                    <div class="circle"></div>
                </div>
            </div>
        </div>
        <p>{{ ao_trans('global.loading') }}</p>
    </div>
</div>

<div class="overlay"></div>

<div class="search-bar">
    <div class="search-icon">
        <i class="material-icons">search</i>
    </div>
    <input type="text" placeholder="{{ ao_trans('inputs.search') }}">
    <div class="close-search">
        <i class="material-icons">close</i>
    </div>
</div>

@include(ao_view('AdminBSB', 'includes.TopNavbar'))

<section>

    @include(ao_view('AdminBSB', 'includes.LeftSidebar'))

    @include(ao_view('AdminBSB', 'includes.RightSidebar'))

</section>


<section class="content">
    <div class="container-fluid">
        @yield('content')
    </div>
</section>


@include(ao_view('AdminBSB', 'includes.JS'), ['pages' => ['global', 'dashboard']])
@yield('js')
<script type="text/javascript">
    @if($errors->any())

    @foreach($errors->all() as $error)
    AlertNotification("warning", "{{ $error }}");
    @endforeach

    @elseif(session('error'))

    @foreach(session('error') as $sessionError)
    AlertNotification("error", "{{ $sessionError }}");
    @endforeach

    @elseif(session('success'))

    @foreach(session('success') as $sessionSuccess)
    AlertNotification("success", "{{ $sessionSuccess }}");
    @endforeach

    @endif
</script>
</body>
</html>