@extends(ao_view('Emails', 'Master'))

@section('title', 'Admin officer Welcome')

@section('content')

    <span class="preheader">Thanks for trying out AdminOfficer. The link is only valid for {{ $expireTime }} minutes.</span>
    <table class="email-wrapper" width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td align="center">
                <table class="email-content" width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="email-masthead">
                            <a href="https://example.com" class="email-masthead_name">
                                Admin Officer
                            </a>
                        </td>
                    </tr>
                    <!-- Email Body -->
                    <tr>
                        <td class="email-body" width="100%" cellpadding="0" cellspacing="0">
                            <table class="email-body_inner" align="center" width="570" cellpadding="0" cellspacing="0">
                                <!-- Body content -->
                                <tr>
                                    <td class="content-cell">
                                        <h1>Welcome!</h1>
                                        <p>Thanks for trying Admin Officer. We’re thrilled to have you on board.</p>
                                        <p>To get the most out of Admin Officer, active your account:</p>
                                        <!-- Action -->
                                        <table class="body-action" align="center" width="100%" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td align="center">
                                                    <!-- Border based button
                                               https://litmus.com/blog/a-guide-to-bulletproof-buttons-in-email-design -->
                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td align="center">
                                                                <table border="0" cellspacing="0" cellpadding="0">
                                                                    <tr>
                                                                        <td>
                                                                            <a href="{{ $url }}" class="button button--" target="_blank">Verify Account</a>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>

                                        <p>For reference, here's your login information:</p>
                                        <table class="attributes" width="100%" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td class="attributes_content">
                                                    <table width="100%" cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td class="attributes_item"><strong>Login Page:</strong> {{ route('admin.officer.login') }}</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="attributes_item"><strong>Email:</strong> {{ $email }} </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>

                                        <p>Thanks,
                                            <br>[Admin Officer] Team</p>

                                        <!-- Sub copy -->
                                        <table class="body-sub">
                                            <tr>
                                                <td>
                                                    <p class="sub">If you’re having trouble with the button above, copy and paste the URL below into your web browser. This activation link is only valid for the next {{ $expireTime }} minutes..</p>
                                                    <p class="sub">{{ $url }}</p>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table class="email-footer" align="center" width="570" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="content-cell" align="center">
                                        <p class="sub align-center">&copy; 2018 [Admin Officer]. All rights reserved.</p>
                                        <p class="sub align-center">
                                            Admin Officer By MOTA
                                        </p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

@endsection