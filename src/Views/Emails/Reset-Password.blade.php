@extends(ao_view('Emails', 'Master'))

@section('title', 'Admin officer reset password')

@section('content')

    <span class="preheader">Use this link to reset your password. The link is only valid for {{ $expireTime }} minutes.</span>

    <table class="email-wrapper" width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td align="center">
                <table class="email-content" width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="email-masthead">
                            <a href="{{ url('/') }}" class="email-masthead_name">
                                Admin Officer
                            </a>
                        </td>
                    </tr>

                    <tr>
                        <td class="email-body" width="100%" cellpadding="0" cellspacing="0">
                            <table class="email-body_inner" align="center" width="570" cellpadding="0" cellspacing="0">

                                <tr>
                                    <td class="content-cell">
                                        <h1>Hi</h1>
                                        <p>You recently requested to reset your password for your Admin Officer admin officer account. Use the button below to reset it. <strong>This password reset is only valid for the next {{ $expireTime }} minutes.</strong></p>

                                        <table class="body-action" align="center" width="100%" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td align="center">

                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td align="center">
                                                                <table border="0" cellspacing="0" cellpadding="0">
                                                                    <tr>
                                                                        <td>
                                                                            <a href="{{ $url }}" class="button button--green" target="_blank">
                                                                                Reset your password
                                                                            </a>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                        <p>If you did not request a password reset, please ignore this email.</p>
                                        <p>Thanks,<br>MOTA</p>
                                        <!-- Sub copy -->
                                        <table class="body-sub">
                                            <tr>
                                                <td>
                                                    <p class="sub">If you’re having trouble with the button above, copy and paste the URL below into your web browser.</p>
                                                    <p class="sub">{{ $url }}</p>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table class="email-footer" align="center" width="570" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="content-cell" align="center">
                                        <p class="sub align-center">&copy; 2018 [Admin Officer]. All rights reserved.</p>
                                        <p class="sub align-center">
                                            Admin Officer By MOTA
                                        </p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

@endsection