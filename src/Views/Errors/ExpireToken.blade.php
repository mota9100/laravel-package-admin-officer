@extends(ao_view('Errors', 'Master'))

@section('title', 'Page Expired')

@section('content')
    <div class="title">
        Your token has expired or not exist.
        <br/><br/>
        Please try new activation email.
    </div>
@endsection
