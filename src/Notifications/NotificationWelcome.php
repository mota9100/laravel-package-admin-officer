<?php

namespace Mota\AdminOfficer\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class NotificationWelcome extends Notification {

    use Queueable;

    public $token;
    public $email;

    /**
     * NotificationResetPassword constructor.
     * Create a new notification instance.
     * @param $admin
     * @param $token
     * @param $browser
     * @param $operationSystem
     */

    public function __construct($email, $token) {

        $this->token = $token;
        $this->email = $email;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable) {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable) {

        $view = ao_view('Emails', 'Welcome');
        $expireTokenTime = (int) config('auth.activation.admin_officer.expire', 30);
        return (new MailMessage)
            ->subject('Welcome to Admin Officer')
            ->from('support@adminofficer.co')
            ->view($view, [
                'expireTime' => $expireTokenTime,
                'url' => route('admin.officer.account.activate', [
                    'email' => $this->email,
                    'token' => $this->token
                ]),
                'email' => $this->email
            ]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
