<?php

namespace Mota\AdminOfficer\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class NotificationResetPassword extends Notification{

    use Queueable;

    public $token;

    /**
     * NotificationResetPassword constructor.
     * Create a new notification instance.
     * @param $admin
     * @param $token
     * @param $browser
     * @param $operationSystem
     */

    public function __construct($token) {

        $this->token = $token;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable) {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable) {

        $view = ao_view('Emails', 'Reset-Password');
        $expireTokenTime = (int) config('auth.passwords.admin_officer.expire', 15);
        return (new MailMessage)
            ->subject('Admin Officer Reset Password')
            ->from('support@adminofficer.co')
            ->view($view, [
                'expireTime' => $expireTokenTime,
                'url' => route('admin.officer.password.reset', [
                    'token' => $this->token
                ]),
            ]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
